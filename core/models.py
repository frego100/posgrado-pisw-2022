from django.db import models


class PostgraduateTemplate(models.Model):
    """
    Postgraduate template model
    """

    template_file = models.FileField(upload_to='template_files/')
    template_code = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
