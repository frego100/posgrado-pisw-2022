from academics.models import StudyPlan
from account.models import User, Professor
from organization.models import StudyField, StudyProgram, GraduateUnit
from tests.base_graphql_test_case import BaseGraphQLTestCase


class ApiBaseTestCase(BaseGraphQLTestCase):
    """
    Base test case for graphql queries and mutations
    """
    def setUp(self) -> None:
        # Study program
        study_field = StudyField.objects.create(name="Ingenieria")
        self.graduate_unit = GraduateUnit.objects.create(
            name="Ingenieria de Sistemas",
            study_field=study_field
        )
        self.study_program = StudyProgram.objects.create(
            program_type=1,
            name="Maestria en algoritmos y estructura de datos",
            graduate_unit=self.graduate_unit
        )
        # Professor
        self.user = User.objects.create(
            email="apollo@gmail.com", password="123",
            gender=1, first_name="Pedro", last_name="Torres"
        )
        self.professor = Professor.objects.create(
            user=self.user, professor_type=1, curriculum=""
        )
        # Study plan
        self.study_plan = StudyPlan.objects.create(
            study_program=self.study_program, year=2021, code="1111"
        )
