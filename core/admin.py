from django.contrib import admin

from core.models import PostgraduateTemplate


@admin.register(PostgraduateTemplate)
class PostgraduateTemplateAdmin(admin.ModelAdmin):
    """
    Postgraduate Template admin
    """

    list_display = ('__str__', 'template_code', )
