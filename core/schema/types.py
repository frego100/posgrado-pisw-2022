"""
Core types
"""
from graphene.types import ObjectType, ID
from graphene_django.types import DjangoObjectType
from graphene import (
    String,
    Enum,
    Field,
)

from django.utils.translation import ugettext_lazy as _

from core.models import PostgraduateTemplate


class EnumStatus(Enum):
    """
    EnumStatus
    """

    SUCCESS = 1
    ERROR = 2


class FeedbackType(ObjectType):
    """
    Feedback type
    """

    message = String(description=_('message'))
    status = Field(EnumStatus, description=_('request status'))


class CountryType(ObjectType):
    """
    Country type
    """

    id = ID()
    name = String()


class PostgraduateTemplateType(DjangoObjectType):
    """
    Postgraduate Template type
    """

    class Meta:
        model = PostgraduateTemplate

    def resolve_template_file(self: PostgraduateTemplate, info):
        return self.template_file.url
