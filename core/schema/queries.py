"""
Core queries
"""
from django_countries import countries
from graphene import ObjectType, List, Field, String

from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required

from core.schema.types import CountryType, PostgraduateTemplateType
from core.models import PostgraduateTemplate


class Query(ObjectType):
    """
    Core Query Object
    """

    get_countries = List(
        CountryType,
        description=_('Get all countries')
    )
    get_all_templates = List(
        PostgraduateTemplateType,
        description=_('Get all templates')
    )
    get_template_by_code = Field(
        PostgraduateTemplateType,
        template_code=String(required=True),
        description=_('Get template by code')
    )

    @login_required
    def resolve_get_countries(self, info):

        return [{'id': code, 'name': name} for code, name in countries]

    @login_required
    def resolve_get_all_templates(self, info):
        return PostgraduateTemplate.objects.all()

    @login_required
    def resolve_get_template_by_code(self, info, template_code):
        try:

            return PostgraduateTemplate.objects.get(template_code=template_code)
        except PostgraduateTemplate.DoesNotExist:

            return None
