"""
Core mutations
"""
from graphene import (
    Mutation,
    List,
    Field,
)
from graphene_django.types import ErrorType

from django.utils.translation import ugettext_lazy as _

from core.schema.types import FeedbackType


class BaseMutation(Mutation):
    feedback = Field(FeedbackType, description=_('Feedback'))
    errors = List(ErrorType, description=_('Errors'))

    def mutate(self, info, **kwargs):
        pass
