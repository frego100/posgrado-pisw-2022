"""
Core utils
"""
from typing import TypeVar, Union, Type
from rest_framework.exceptions import ValidationError

from django.db import models

from account.models import User


ModelType = TypeVar('ModelType', bound=Union[models.Model, User])


def get_object_by_id(model: Type[ModelType], object_id) -> Union[ModelType, None]:
    try:

        return model.objects.get(pk=object_id)
    except model.DoesNotExist:

        return None


def update_model_instance(instance, update_fields):
    for key in update_fields:
        setattr(instance, key, update_fields[key])


def pop_sub_dict(dictionary: dict, keys: list) -> dict:

    return {key: dictionary.pop(key) for key in keys if key in dictionary}


def validate_existing_instance(model: Type[ModelType], object_id, subject):
    try:

        return model.objects.get(pk=object_id)
    except model.DoesNotExist:

        raise ValidationError(f'{subject} con ese id no existe')
