"""
Payments urls
"""
from django.urls import path

from payments.views import (
    DownloadStudentsPaymentsExcelTemplateView,
    DownloadStudentsPaymentsView,
)

urlpatterns = [
    path(
        'payments_template_file/',
        DownloadStudentsPaymentsExcelTemplateView.as_view(),
        name='payments_view',
    ),
    path(
        'payments_file/',
        DownloadStudentsPaymentsView.as_view(),
        name='payments_template_view',
    ),
]
