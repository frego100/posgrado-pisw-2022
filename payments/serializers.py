from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from payments.messages.error_messages import PAYMENT_SCHEDULE_ALREADY_CREATED, FEE_PAYMENT_SEMESTER_WRONG
from payments.models import PaymentSchedule, FeePayment


class PaymentScheduleSerializer(serializers.ModelSerializer):
    """
    Payment schedule model serializer
    """

    class Meta:
        model = PaymentSchedule
        fields = (
            'study_program',
            'admission_year',
            'num_of_payments',
            'interest_rate'
        )

    def validate(self, attrs):
        if PaymentSchedule.objects.filter(
                study_program=attrs['study_program'],
                admission_year=attrs['admission_year']
        ).exists():
            raise serializers.ValidationError(PAYMENT_SCHEDULE_ALREADY_CREATED)

        return attrs

    def create(self, validated_data):
        payment_schedule = PaymentSchedule.objects.create(**validated_data)

        # Create fee payments for the payment schedule
        for i in range(1, payment_schedule.num_of_payments + 1):
            FeePayment.objects.create(
                payment_schedule=payment_schedule,
                num_of_payment=i
            )

        return payment_schedule


class FeePaymentSerializer(serializers.ModelSerializer):
    """
    Payment schedule model serializer
    """

    class Meta:
        model = FeePayment
        fields = (
            'amount',
            'semester',
            'deadline'
        )

    def validate_semester(self, semester):
        if not semester:
            return semester
        fee_payment: FeePayment = self.instance
        if FeePayment.objects.filter(
            payment_schedule=fee_payment.payment_schedule,
            num_of_payment__lt=fee_payment.num_of_payment,
            semester__gt=semester
        ).exists():
            raise ValidationError(
                "El semestre debe ser mayor o igual al semestre de las cuotas de pago previas"
            )

        if FeePayment.objects.filter(
            payment_schedule=fee_payment.payment_schedule,
            num_of_payment__gt=fee_payment.num_of_payment,
            semester__lt=semester
        ).exists():
            raise ValidationError(
                "El semestre debe ser menor o igual al semestre de las cuotas de pago siguientes"
            )

        return semester
