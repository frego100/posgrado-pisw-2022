import os

import openpyxl
from openpyxl.reader.excel import load_workbook
from openpyxl.styles import Border, Side, PatternFill, Font

from account.models import Student
from enrollment.utils import get_admitted_students
from payments.constants import (
    PAYMENTS_FIRST_ROW,
    PAYMENT_SHEET_PROGRAM_CELL,
    PAYMENT_SHEET_YEAR, PAYMENTS_HEADER_ROW, FIRST_PAYMENT_COL,
    PAYMENTS_FIRST_STUDENT_ROW
)
from payments.models import PaymentSchedule, StudentPayment
from postgraduate import settings


def validate_format_of_student_payments_sheet(
        payments_book, payment_schedule: PaymentSchedule
):
    """
    Validate the format of the student payments sheet
    """
    errors = []

    workbook = openpyxl.load_workbook(payments_book)
    sheet = workbook.active

    sheet_program: str = sheet[PAYMENT_SHEET_PROGRAM_CELL].value
    if sheet_program.upper() != payment_schedule.study_program.name.upper():
        errors.append('El programa de estudio no corresponde')
    sheet_year = sheet[PAYMENT_SHEET_YEAR].value
    if str(sheet_year) != str(payment_schedule.admission_year):
        errors.append('El año de ingreso no corresponde')

    for row in sheet.iter_rows(
            min_row=PAYMENTS_HEADER_ROW,
            max_row=PAYMENTS_HEADER_ROW,
            max_col=payment_schedule.num_of_payments + 2
    ):
        cui_header = row[0].value
        student_header = row[1].value

        if cui_header.upper() != "CUI" or student_header.upper() != "ALUMNO":
            errors.append('Formato del archivo invalido')

    return errors

def convert_to_float(value):
    try:
        return float(value)
    except (ValueError, TypeError):
        return 0

def save_program_student_payments(payments_book, payment_schedule: PaymentSchedule):
    """
    Process payments file to save the payments of the students of a program
    """
    workbook = openpyxl.load_workbook(payments_book)
    sheet = workbook.active

    missing_students = []  # To store the cui of the unknown students in the file
    # Get the fee payments of the payment schedule
    fee_payments = payment_schedule.feepayment_set.all().order_by('num_of_payment')

    # Arrays to bulk create and bulk update at the end of the method
    existing_payments = []  # To store student payments that already exists
    new_payments = []  # To store new student payments

    # There must be two columns of cui and name, plus the num of payments
    max_num_of_columns = payment_schedule.num_of_payments + 2
    # Iterate rows from the first student until the num of payments
    for row in sheet.iter_rows(
            min_row=PAYMENTS_FIRST_ROW,
            max_col=max_num_of_columns
    ):
        if row[0].value is None or row[1].value is None:
            break  # Stop the loop if either of the first two cells is empty
        cui = row[0].value
        payments = row[2:]

        try:
            # Check the student exists and it's admitted on the study program
            student = Student.objects.get(
                cui=cui,
                studyprogramadmission__study_program=payment_schedule.study_program,
                studyprogramadmission__admission_process__admission_year=payment_schedule.admission_year
            )
            # Iterate over the payments row of the student
            for num, amount in enumerate(payments):
                amount = convert_to_float(amount.value)
                # Check if the object of the StudentPayment already exists
                try:
                    student_payment = StudentPayment.objects.get(
                        student=student,
                        fee_payment=fee_payments[num]
                    )
                    student_payment.amount_paid = amount
                    existing_payments.append(student_payment)

                except StudentPayment.DoesNotExist:
                    student_payment = StudentPayment(
                        student=student,
                        fee_payment=fee_payments[num],
                        amount_paid=amount
                    )
                    new_payments.append(student_payment)

        except Student.DoesNotExist:
            missing_students.append(cui)


    StudentPayment.objects.bulk_create(new_payments)
    StudentPayment.objects.bulk_update(
        existing_payments, fields=['amount_paid']
    )

    return missing_students


def create_base_payments_file(payment_schedule: PaymentSchedule):
    """
    Create the base content of the payments file
    """
    template_path = os.path.join(
        settings.BASE_DIR, 'spreadsheets/Formato de Reporte de Pagos.xlsx'
    )
    payments_file = load_workbook(template_path)
    sheet = payments_file.active

    # Set program name and admission year
    sheet[
        PAYMENT_SHEET_PROGRAM_CELL].value = payment_schedule.study_program.name
    sheet[PAYMENT_SHEET_YEAR].value = payment_schedule.admission_year

    # Define header styles
    border_style = Border(
        left=Side(style='thin'), right=Side(style='thin'),
        top=Side(style='thin')
    )
    fill = PatternFill(fgColor='3C78D8', fill_type='solid')
    font_style = Font(color='FFFFFF')

    num_of_fee_payments = payment_schedule.num_of_payments

    # Fill header
    for col_num in range(
            FIRST_PAYMENT_COL, FIRST_PAYMENT_COL + num_of_fee_payments
    ):
        cell = sheet.cell(row=PAYMENTS_HEADER_ROW, column=col_num)
        cell.value = f'P{col_num - FIRST_PAYMENT_COL + 1}'
        cell.border = border_style
        cell.fill = fill
        cell.font = font_style

    return payments_file


def fill_payments_file_content(
        payment_schedule: PaymentSchedule,
        payments_file,
        include_payments=False,
):
    """
    Fill payments file content (students and payments if required)
    """
    sheet = payments_file.active
    # Fetch and fill student data
    students = get_admitted_students(
        study_program=payment_schedule.study_program,
        admission_year=payment_schedule.admission_year
    ).order_by('user__last_name')

    for index, student in enumerate(students):
        row = sheet[PAYMENTS_FIRST_STUDENT_ROW + index]
        row[0].value = student.cui
        row[1].value = student.user.get_full_name().upper()
        if include_payments:
            student_payments = student.studentpayment_set.filter(
                fee_payment__payment_schedule=payment_schedule
            )
            for row_index, payment in enumerate(student_payments, start=2):
                row[row_index].value = payment.amount_paid

    return payments_file

def create_students_payments_template(payment_schedule: PaymentSchedule):
    """
    Create students payments Excel template
    """
    payments_file = create_base_payments_file(payment_schedule)

    return fill_payments_file_content(
        payment_schedule=payment_schedule, payments_file=payments_file
    )


def create_student_payments_file(payment_schedule: PaymentSchedule):
    """
    Create student payments Excel file
    """
    payments_file = create_base_payments_file(payment_schedule)

    return fill_payments_file_content(
        payment_schedule=payment_schedule,
        include_payments=True,
        payments_file=payments_file
    )
