CANNOT_CREATE_PAYMENT_SCHEDULE = 'No se pudo crear el cronograma de pagos'
PAYMENT_SCHEDULE_ALREADY_CREATED = 'Ya existe un cronograma de pagos para este año de ingreso'
COULD_NOT_CHANGE_FEE_PAYMENT = 'No se pudo editar la cuota de pago'
FEE_PAYMENT_SEMESTER_WRONG = 'El número de semestre incorrecto'

CONTACT_SUPPORT_MESSAGE = "Vuelva a intentarlo o contacte al servicio de soporte"
INVALID_PAYMENT_SCHEDULE = "El cronograma de pago no existe"
INVALID_PAYMENTS_FILE_FORMAT = f"Verifique el formato del archivo. {CONTACT_SUPPORT_MESSAGE}"
COULD_NOT_SAVE_STUDENT_PAYMENTS = f"No se pudo registrar información de los pagos. {CONTACT_SUPPORT_MESSAGE}"