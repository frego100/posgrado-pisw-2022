from graphene import InputObjectType, ID, Int, Decimal, Date


class EditFeePaymentInput(InputObjectType):
    """
    Edit Fee Payment input
    """
    fee_payment_id = ID(required=True)
    amount = Decimal(required=False)
    semester = Int(required=False)
    deadline = Date(required=False)
