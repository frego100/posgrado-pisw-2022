"""
GQL types of payment models
"""
import graphene
from graphene import ObjectType
from graphene_django import DjangoObjectType

from enrollment.utils import get_admitted_students
from payments.models import (
    PaymentSchedule, FeePayment, StudentPayment,
)


class PaymentScheduleType(DjangoObjectType):
    """
    Payment schedule GQL type
    """

    class Meta:
        model = PaymentSchedule

    def resolve_feepayment_set(self: PaymentSchedule, info):
        return self.feepayment_set.order_by('num_of_payment')



class FeePaymentType(DjangoObjectType):
    """
    Fee Payment GQL type
    """

    current_total_collection = graphene.Float()
    expected_gross_collection = graphene.Float()
    expected_net_collection = graphene.Float()
    collection_percentage = graphene.Float()

    class Meta:
        model = FeePayment

    def resolve_current_total_collection(self: FeePayment, info):
        return self.get_current_collection()

    def resolve_expected_gross_collection(self: FeePayment, info):
        study_program_students = get_admitted_students(
            study_program=self.payment_schedule.study_program,
            admission_year=self.payment_schedule.admission_year
        )

        return self.amount * study_program_students.count() if self.amount else None

    def resolve_expected_net_collection(self: FeePayment, info):
        return self.get_expected_net_collection()

    def resolve_collection_percentage(self: FeePayment, info):
        expected_collection = self.get_expected_net_collection()
        current_collection = self.get_current_collection()

        if expected_collection and current_collection:
            return round(
                (current_collection * 100) / float(expected_collection), 2
            )

        return None


class StudentPaymentType(DjangoObjectType):
    """
    Student Payment GQL type
    """
    discount = graphene.Int()

    class Meta:
        model = StudentPayment

    def resolve_discount(self: StudentPayment, info):
        return self.discount_percentage
