import graphene
from graphene import ObjectType

from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required

from payments.models import PaymentSchedule
from payments.schema.types import PaymentScheduleType


class Query(ObjectType):
    get_payment_schedules = graphene.List(
        PaymentScheduleType,
        study_program_id=graphene.ID(required=True),
        description=_('Get payment schedules of study program')
    )
    get_payment_schedule = graphene.Field(
        PaymentScheduleType,
        study_program_id=graphene.ID(required=True),
        year=graphene.Int(required=True),
        description=_('Get payment schedule by program and year')
    )

    @login_required
    def resolve_get_payment_schedules(self, info, study_program_id):
        return PaymentSchedule.objects.filter(
            study_program_id=study_program_id
        ).order_by('admission_year')

    @login_required
    def resolve_get_payment_schedule(self, info, study_program_id, year):
        payment_schedule = PaymentSchedule.objects.filter(
            study_program_id=study_program_id, admission_year=year
        )
        return payment_schedule.first() if payment_schedule.exists() else None
