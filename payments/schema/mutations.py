import graphene
from django.utils import timezone
from graphene import ObjectType, Field
from graphene_django.types import ErrorType
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required

from core.messages.error_messages import DONT_HAVE_PERMISSION
from core.schema.mutations import BaseMutation
from core.schema.types import FeedbackType, EnumStatus
from core.utils import get_object_by_id
from organization.models import StudyProgram
from payments.messages.error_messages import (
    CANNOT_CREATE_PAYMENT_SCHEDULE,
    COULD_NOT_CHANGE_FEE_PAYMENT,
    INVALID_PAYMENT_SCHEDULE,
    INVALID_PAYMENTS_FILE_FORMAT,
    COULD_NOT_SAVE_STUDENT_PAYMENTS,
)
from payments.messages.success_messages import (
    PAYMENT_SCHEDULE_CREATED,
    FEE_PAYMENT_CHANGED,
    STUDENT_PAYMENTS_SAVED,
)
from payments.models import FeePayment, PaymentSchedule
from payments.schema.inputs import EditFeePaymentInput
from payments.schema.types import FeePaymentType
from payments.serializers import PaymentScheduleSerializer, FeePaymentSerializer
from payments.utils import (
    validate_format_of_student_payments_sheet,
    save_program_student_payments,
)


class CreatePaymentSchedule(BaseMutation):
    """
    Mutation to create payment schedule
    """
    class Arguments:
        study_program_id = graphene.ID(required=True)
        admission_year = graphene.Int(required=True)
        num_of_payments = graphene.Int(required=True)
        interest_rate = graphene.Decimal(required=False)

    @login_required
    def mutate(self, info, **kwargs):
        study_program_id = kwargs.pop('study_program_id')
        study_program = get_object_by_id(StudyProgram, study_program_id)
        user = info.context.user
        if user.unitmanager.graduate_unit != study_program.graduate_unit:
            return CreatePaymentSchedule(
                feedback=FeedbackType(
                    message=CANNOT_CREATE_PAYMENT_SCHEDULE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({"user": [DONT_HAVE_PERMISSION]})
            )

        kwargs['study_program'] = study_program_id
        serializer = PaymentScheduleSerializer(data=kwargs)

        if serializer.is_valid():
            serializer.save()
            return CreatePaymentSchedule(
                feedback=FeedbackType(
                    message=PAYMENT_SCHEDULE_CREATED,
                    status=EnumStatus.SUCCESS
                )
            )

        return CreatePaymentSchedule(
            feedback=FeedbackType(
                message=CANNOT_CREATE_PAYMENT_SCHEDULE,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class EditFeePaymentMutation(BaseMutation):

    fee_payment = Field(FeePaymentType)

    class Arguments:
        input = EditFeePaymentInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        fee_payment_id = inputs.pop('fee_payment_id')
        fee_payment_model = get_object_by_id(FeePayment, fee_payment_id)
        serializer = FeePaymentSerializer(
            instance=fee_payment_model, data=inputs, partial=True
        )

        if serializer.is_valid():
            fee_payment = serializer.save()
            return EditFeePaymentMutation(
                feedback=FeedbackType(
                    message=FEE_PAYMENT_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                fee_payment=fee_payment
            )

        return EditFeePaymentMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_FEE_PAYMENT,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class SaveStudentsPaymentsMutation(BaseMutation):

    class Arguments:
        payment_schedule_id = graphene.ID(required=True)
        payments_sheet = Upload(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        payment_schedule_id = kwargs.pop('payment_schedule_id')
        payments_sheet = kwargs.pop('payments_sheet')

        payment_schedule = get_object_by_id(PaymentSchedule, payment_schedule_id)
        if not payment_schedule:
            return SaveStudentsPaymentsMutation(
                feedback=FeedbackType(
                    message=INVALID_PAYMENT_SCHEDULE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({'id': [INVALID_PAYMENT_SCHEDULE]})
            )

        format_errors = validate_format_of_student_payments_sheet(
            payments_book=payments_sheet,
            payment_schedule=payment_schedule
        )

        if format_errors:
            return SaveStudentsPaymentsMutation(
                feedback=FeedbackType(
                    message=INVALID_PAYMENTS_FILE_FORMAT,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({'format': format_errors})
            )

        try:
            missing_students = save_program_student_payments(
                payments_book=payments_sheet, payment_schedule=payment_schedule
            )
            if missing_students:
                message = f"{STUDENT_PAYMENTS_SAVED} Solo se consideró los estudiantes registrados como ingresantes."
            else:
                message = STUDENT_PAYMENTS_SAVED

            payment_schedule.last_update = timezone.now()
            payment_schedule.save()

            return SaveStudentsPaymentsMutation(
                feedback=FeedbackType(
                    message=message,
                    status=EnumStatus.SUCCESS
                )
            )

        # TODO: Catch specific exceptions
        except Exception as e:
            return SaveStudentsPaymentsMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_SAVE_STUDENT_PAYMENTS,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(
                    {'error': [COULD_NOT_SAVE_STUDENT_PAYMENTS, str(e)]}
                )
            )


class Mutation(ObjectType):
    create_payment_schedule = CreatePaymentSchedule.Field()
    edit_fee_payment = EditFeePaymentMutation.Field()
    save_students_payments = SaveStudentsPaymentsMutation.Field()
