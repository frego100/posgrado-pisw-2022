import json

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from core.utils import get_object_by_id
from payments.models import PaymentSchedule
from payments.utils import (
    create_students_payments_template,
    create_student_payments_file,
)


@method_decorator(csrf_exempt, name='dispatch')
class DownloadStudentsPaymentsExcelTemplateView(View):
    """
    Download students payments Excel file template
    """

    def post(self, request, *args, **kwargs):
        try:
            body = json.loads(request.body.decode('utf-8'))
            payment_schedule_id = body['payment_schedule_id']
            payment_schedule = get_object_by_id(
                PaymentSchedule, payment_schedule_id
            )

            workbook = create_students_payments_template(payment_schedule)

            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            )
            filename = (
                f"Plantilla Pagos {payment_schedule.admission_year} "
                f'{payment_schedule.study_program.name}.xlsx'
            )
            response[
                'Content-Disposition'] = f'attachment; filename={filename}'
            workbook.save(response)

            return response

        except (KeyError, json.JSONDecodeError) as error:

            return HttpResponse('Wrong request format')


@method_decorator(csrf_exempt, name='dispatch')
class DownloadStudentsPaymentsView(View):
    """
    Download students payments Excel file
    """

    def post(self, request, *args, **kwargs):
        try:
            body = json.loads(request.body.decode('utf-8'))
            payment_schedule_id = body['payment_schedule_id']
            payment_schedule = get_object_by_id(
                PaymentSchedule, payment_schedule_id
            )
            workbook = create_student_payments_file(payment_schedule)

            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            )
            filename = (
                f"Pagos {payment_schedule.admission_year} "
                f'{payment_schedule.last_update.strftime("%d-%b-%y") if payment_schedule.last_update else ""} '
                f'{payment_schedule.study_program.name}.xlsx'
            )
            response['Content-Disposition'] = f'attachment; filename={filename}'
            workbook.save(response)

            return response

        except (KeyError, json.JSONDecodeError) as error:

            return HttpResponse('Wrong request format')
