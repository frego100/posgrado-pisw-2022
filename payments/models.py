from django.db import models

from account.models import Student
from organization.models import StudyProgram


class PaymentSchedule(models.Model):
    """
    Payment schedule
    """
    study_program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    admission_year = models.PositiveSmallIntegerField()
    num_of_payments = models.PositiveSmallIntegerField()
    interest_rate = models.DecimalField(
        null=True, blank=True, max_digits=6, decimal_places=2
    )
    last_update = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{self.study_program.name} - {self.admission_year}'


class FeePayment(models.Model):
    """
    Fee payment
    """
    payment_schedule = models.ForeignKey(
        PaymentSchedule, on_delete=models.CASCADE
    )
    num_of_payment = models.PositiveSmallIntegerField()
    amount = models.DecimalField(
        max_digits=6, decimal_places=2, null=True, blank=True
    )
    semester = models.PositiveSmallIntegerField(null=True, blank=True)
    deadline = models.DateField(null=True, blank=True)

    def __str__(self):
        return f'P{self.num_of_payment} - {self.payment_schedule.study_program}'


    def get_current_collection(self) -> float:
        current_collection = sum(
            student_payment.amount_paid
            for student_payment in self.studentpayment_set.all()
        )
        return current_collection if current_collection > 0 else None
    def get_expected_net_collection(self) -> float:
        student_payments = self.studentpayment_set.all()
        return sum(
            student_payment.amount_paid
            if student_payment.discount_percentage
            else float(self.amount)
            for student_payment in student_payments
        ) if self.amount else None

class StudentPayment(models.Model):
    """
    Student Payment model
    """
    fee_payment = models.ForeignKey(FeePayment, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    amount_paid = models.FloatField(default=0)
    date_paid = models.DateField(null=True, blank=True)
    voucher_code = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f"{self.student} - {self.amount_paid}"

    @property
    def discount_percentage(self) -> int:
        if (
            self.fee_payment.amount
            and self.amount_paid != 0
            and self.amount_paid < self.fee_payment.amount
        ):
            return int(
                100 - ((self.amount_paid * 100) / float(self.fee_payment.amount))
            )
