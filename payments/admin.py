from django.contrib import admin

from payments.models import PaymentSchedule, FeePayment, StudentPayment


class FeePaymentInline(admin.StackedInline):
    model = FeePayment
    readonly_fields = ('num_of_payment', )
    extra = 0


@admin.register(PaymentSchedule)
class PaymentScheduleAdmin(admin.ModelAdmin):
    """
    PaymentSchedule admin
    """

    list_display = ('admission_year', 'study_program', 'num_of_payments')
    list_filter = ('study_program__graduate_unit',)
    inlines = ([FeePaymentInline])


@admin.register(StudentPayment)
class StudentPaymentAdmin(admin.ModelAdmin):
    """
    StudentPayment admin
    """

    list_display = ('student', 'fee_payment', 'amount_paid',)
    list_filter = (
        'fee_payment__payment_schedule__study_program__graduate_unit',
        'fee_payment__payment_schedule__admission_year'
    )
    search_fields = (
        'fee_payment__payment_schedule__study_program__name',
        'student__user__email'
    )
