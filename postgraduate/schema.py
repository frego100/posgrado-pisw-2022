"""
General GraphQL schema
"""
import graphene
import graphql_jwt
from account.schema.queries import Query as AccountQuery
from account.schema.mutations import Mutation as AccountMutation
from academics.schema.queries import Query as AcademicQuery
from academics.schema.mutations import Mutation as AcademicMutation
from enrollment.schema.queries import Query as EnrollmentQuery
from enrollment.schema.mutations import Mutation as EnrollmentMutation
from core.schema.queries import Query as CoreQuery
from operating_plan.schema.queries import Query as OperatingPlanQuery
from operating_plan.schema.mutations import Mutation as OperatingPlanMutation
from organization.schema.queries import Query as OrganizationQuery
from payments.schema.mutations import Mutation as PaymentsMutation
from payments.schema.queries import Query as PaymentsQuery


class Query(
    AcademicQuery,
    AccountQuery,
    CoreQuery,
    OperatingPlanQuery,
    OrganizationQuery,
    EnrollmentQuery,
    PaymentsQuery,
    graphene.ObjectType
):
    hello = graphene.String(default_value="Hi!")


class Mutation(
    AcademicMutation,
    AccountMutation,
    OperatingPlanMutation,
    EnrollmentMutation,
    PaymentsMutation,
    graphene.ObjectType
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    verify_token = graphql_jwt.Verify.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
