from django.db import models

from academics.constants import WeekdayChoices, RequestStatusChoices
from account.models import Professor, Student
from operating_plan.models import OperatingPlanProgram, ProfessorRegistration
from operating_plan.constants import OperatingPlanStatusChoices
from organization.models import StudyProgram


class StudyPlan(models.Model):
    """
    Study model plan
    """

    study_program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    year = models.PositiveIntegerField()
    code = models.CharField(max_length=10, null=True, blank=True)
    study_plan_request = models.OneToOneField(
        'StudyPlanRequest',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.study_program.__str__()} {self.year}"

    @property
    def num_of_semesters(self):
        if self.studyplansubject_set.count() > 0:
            return self.studyplansubject_set.order_by('-semester').first().semester

        return 0

    class Meta:
        ordering = ('-year',)


class StudyPlanSubject(models.Model):
    """
    Study plan subject
    """
    study_plan = models.ForeignKey(StudyPlan, on_delete=models.CASCADE)
    subject = models.CharField(max_length=100)
    credits = models.PositiveSmallIntegerField()
    semester = models.PositiveSmallIntegerField()
    hours = models.PositiveSmallIntegerField()
    code = models.CharField(max_length=16)

    def __str__(self):
        return self.subject


class Course(models.Model):
    """
    Course model
    """
    study_plan_subject = models.ForeignKey(
        StudyPlanSubject, on_delete=models.CASCADE
    )
    operating_plan_program = models.ForeignKey(
        OperatingPlanProgram, on_delete=models.CASCADE
    )
    syllabus = models.FileField(null=True, blank=True)
    enrollment_file = models.FileField(
        null=True,
        blank=True,
        upload_to='course_enrollments/'
    )

    def __str__(self):

        return (
            f"{self.study_plan_subject.subject} - "
            f"{self.operating_plan_program.operating_plan.op_plan_process.year}"
        )


class CourseGroup(models.Model):
    """
    CourseGroup model
    """
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    professor = models.ForeignKey(
        ProfessorRegistration, on_delete=models.CASCADE, null=True, blank=True
    )
    group = models.CharField(max_length=1, null=True, blank=True)
    capacity = models.PositiveSmallIntegerField(null=True, blank=True)
    classroom = models.CharField(max_length=10, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    is_in_operating_plan = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.course.study_plan_subject.subject} - {self.group}"


    @property
    def is_filled(self) -> bool:
        return (
                self.professor
                and self.start_date
                and self.end_date
                and self.schedule_set.count() > 0
                and not self.is_deleted
        )

    def save(self, *args, **kwargs):
        if not self.pk:
            is_approved = self.course.operating_plan_program.operating_plan.status == OperatingPlanStatusChoices.APPROVED
            self.is_in_operating_plan = not is_approved

        super(CourseGroup, self).save(*args, **kwargs)


class Schedule(models.Model):
    """
    Schedule model
    """
    course = models.ForeignKey(CourseGroup, on_delete=models.CASCADE)
    day = models.PositiveSmallIntegerField(choices=WeekdayChoices.choices)
    start_time = models.TimeField()
    end_time = models.TimeField()

    def __str__(self):
        return self.course.__str__()


class CourseProgress(models.Model):
    """
    CourseProgress model
    """

    tittle = models.CharField(max_length=30, blank=True)
    description = models.CharField(max_length=150, blank=True)
    percentage = models.PositiveSmallIntegerField(null=True, blank=True)
    academic_session = models.ForeignKey(
        'enrollment.AcademicSession', on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return f"{self.academic_session.schedule.course} - Percentage {self.percentage} % - {self.academic_session.date}"


class StudyPlanRequest(models.Model):
    """
    Study Plan Request model
    """

    program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    status = models.IntegerField(
        choices=RequestStatusChoices.choices, default=RequestStatusChoices.SENT
    )
    observation = models.TextField(blank=True)
    summary = models.TextField()
    study_plan_file = models.FileField(
        upload_to='study_plan_requests/', max_length=200
    )
    resolution_file = models.FileField(
        upload_to='plan_resolution/', null=True, blank=True
    )
    request_date = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ('-request_date', )

    def __str__(self):
        return self.program.__str__()
