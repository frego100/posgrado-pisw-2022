from django.dispatch import receiver
from django.db.models.signals import post_save

from academics.models import Course
from operating_plan.models import (
    OperatingPlanProgram,
)


@receiver(post_save, sender=OperatingPlanProgram)
def create_courses(sender, instance: OperatingPlanProgram, created, **kwargs):
    """
    Create courses for each subject
    :param sender: sender
    :param instance: operating plan program instance
    :parm created: if instance was created
    :param kwargs: kwargs
    :return: None
    """

    if created:
        study_plan = instance.study_program.studyplan_set.first()
        if study_plan:
            subjects = study_plan.studyplansubject_set.all()
            for subject in subjects:
                Course.objects.create(
                    study_plan_subject=subject, operating_plan_program=instance
                )
