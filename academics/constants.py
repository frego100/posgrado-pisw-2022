from django.db import models


class WeekdayChoices(models.IntegerChoices):
    """
    Weekdays choices
    """
    MONDAY = 1, 'Lunes'
    TUESDAY = 2, 'Martes'
    WEDNESDAY = 3, 'Miercoles'
    THURSDAY = 4, 'Jueves'
    FRIDAY = 5, 'Viernes'
    SATURDAY = 6, 'Sábado'
    SUNDAY = 7, 'Domingo'


class RequestStatusChoices(models.IntegerChoices):
    """
    Request Status choices
    """

    SENT = 1, 'Enviado'
    ACADEMIC_REVIEW = 2, 'En Revision: Subdireccion Academica'
    BOARD_REVIEW = 3, 'En Revision: Consejo Directivo'
    OBSERVED = 4, 'Observado'
    REJECTED = 5, 'Rechazado'
    APPROVED = 6, 'Aprobado'


STUDENTS_FILE_START_INDEX = 5
STUDY_PLAN_REQUEST_YEAR_INDEX = 'B2'

ALLOWED_REQUEST_STATUS_CHANGES = {
    RequestStatusChoices.SENT: [
        RequestStatusChoices.ACADEMIC_REVIEW,
        RequestStatusChoices.OBSERVED
    ],
    RequestStatusChoices.ACADEMIC_REVIEW: [
        RequestStatusChoices.BOARD_REVIEW,
        RequestStatusChoices.OBSERVED
    ],
    RequestStatusChoices.BOARD_REVIEW: [
        RequestStatusChoices.OBSERVED,
        RequestStatusChoices.REJECTED,
        RequestStatusChoices.APPROVED
    ],
    RequestStatusChoices.OBSERVED: [
        RequestStatusChoices.SENT,
    ],
}

REQUEST_REQUIRED_FIELDS = {
    RequestStatusChoices.OBSERVED: 'observation',
    RequestStatusChoices.REJECTED: 'resolution_file',
    RequestStatusChoices.APPROVED: 'resolution_file'
}
