"""
Academics utils
"""
import openpyxl
from typing import Optional

from academics.constants import (
    STUDENTS_FILE_START_INDEX,
    STUDY_PLAN_REQUEST_YEAR_INDEX,
)
from academics.models import (
    StudyPlan,
    StudyPlanSubject,
    StudyPlanRequest,
)


def create_study_plan_from_request(
        study_plan_request: Optional[StudyPlanRequest],
        commit: bool = True,
        study_plan_file=None,
        program_id=None,
):
    """
    Create students from request
    """

    if study_plan_request:
        study_plan_file = study_plan_request.study_plan_file
        program_id = study_plan_request.program_id

    workbook = openpyxl.load_workbook(study_plan_file)
    sheet = workbook.active
    subjects = []
    current_year = sheet[STUDY_PLAN_REQUEST_YEAR_INDEX].value

    study_plan = StudyPlan(
        study_program_id=program_id,
        year=current_year,
        study_plan_request=study_plan_request
    )

    for row in sheet.iter_rows(min_row=STUDENTS_FILE_START_INDEX):
        code, subject, total_credits, semester, hours = row

        if code.value:
            subject = StudyPlanSubject(
                study_plan=study_plan,
                subject=subject.value,
                credits=total_credits.value,
                semester=semester.value,
                hours=hours.value,
                code=code.value,
            )
            subject.clean_fields(exclude=['study_plan'])
            subjects.append(subject)

    if commit:
        study_plan.save()
        StudyPlanSubject.objects.bulk_create(subjects)
