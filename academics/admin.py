from django.contrib import admin

from academics.models import (
    StudyPlan,
    StudyPlanSubject,
    Course,
    Schedule, CourseGroup,
    CourseProgress,
    StudyPlanRequest,
)


@admin.register(StudyPlan)
class StudyPlanAdmin(admin.ModelAdmin):
    list_display = ('study_program', 'year')
    list_filter = ('year', )


@admin.register(StudyPlanSubject)
class StudyPlanSubjectAdmin(admin.ModelAdmin):
    list_display = ('code', 'subject', 'study_plan', 'credits', 'semester')
    list_filter = ('study_plan__study_program__graduate_unit', 'study_plan__year')


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('study_plan_subject', 'operating_plan_program')
    search_fields = (
        'study_plan_subject__subject',
        'operating_plan_program__study_program__name',
    )


class ScheduleInline(admin.TabularInline):
    model = Schedule
    extra = 0


@admin.register(CourseGroup)
class CourseGroupAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'professor', 'group', )
    list_filter = ('group', )
    inlines = (ScheduleInline, )


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'day', 'start_time', 'end_time', )
    list_filter = ('day', )


@admin.register(CourseProgress)
class CourseProgress(admin.ModelAdmin):
    pass


@admin.register(StudyPlanRequest)
class StudyPlanRequestAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'status', 'request_date',)
    list_filter = ('program__graduate_unit',)
