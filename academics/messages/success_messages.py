"""
Academics success messages
"""

COURSE_ADDED = 'El curso fue agregado'
COURSE_CHANGED = 'El curso fue editado'
GROUP_ADDED = 'El grupo fue agregado'
GROUP_CHANGED = 'El grupo fue editado'
GROUP_DELETED = 'El grupo fue eliminado'
SCHEDULE_ADDED = 'El horario fue agregado'
SCHEDULE_CHANGED = 'El horario fue editado'
SCHEDULE_DELETED = 'El horario fue eliminado'
COURSE_PROGRESS_ADDED = 'El avance del curso  fue agregado'
COURSE_PROGRESS_CHANGED = 'El avance del curso  fue editado'
COURSE_PROGRESS_DELETED = 'El avance del curso  fue eliminado'
STUDY_PLAN_REQUEST_SENT = 'La solicitud del plan de estudios fue enviada'
STUDY_PLAN_REQUEST_UPDATED = 'La solicitud del plan de estudios fue actualizada'
