"""
Academics error messages
"""

COULD_NOT_ADD_COURSE = 'No se pudo agregar el curso'
COULD_NOT_CHANGE_COURSE = 'No se pudo editar el curso'
COULD_NOT_ADD_GROUP = 'No se pudo agregar el grupo'
COULD_NOT_CHANGE_GROUP = 'No se pudo editar el grupo'
COULD_NOT_DELETE_GROUP = 'No se pudo eliminar el grupo'
COULD_NOT_ADD_SCHEDULE = 'No se pudo agregar el horario'
COULD_NOT_CHANGE_SCHEDULE = 'No se pudo editar el horario'
COULD_NOT_DELETE_SCHEDULE = 'No se pudo eliminar el horario'
COULD_NOT_ADD_COURSE_PROGRESS = 'No se pudo agregar el avance en el curso'
COULD_NOT_DELETE_COURSE_PROGRESS = 'No se pudo eliminar el avance en el curso'
COULD_NOT_CHANGE_COURSE_PROGRESS = 'No se pudo editar el avance en el curso'
COULD_NOT_SEND_STUDY_PLAN_REQUEST = 'No se pudo enviar la solicitud de plan ' \
                                    'de estudios '
COULD_NOT_SEND_STUDY_PLAN_REQUEST_ON_COURSE = 'No se pudo enviar la solicitud. Existen solicitudes en curso'
COULD_NOT_UPDATE_STUDY_PLAN_REQUEST = 'No se pudo actualizar la solicitud de ' \
                                       'plan de estudios '
COULD_NOT_APPROVE_STUDY_PLAN_REQUEST = 'No se pudo aprobar la solicitud de ' \
                                       'plan de estudios '
