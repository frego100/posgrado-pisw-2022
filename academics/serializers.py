"""
Academics serializers
"""
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from zipfile import BadZipFile

from academics.models import (
    Course,
    CourseGroup,
    Schedule,
    CourseProgress,
    StudyPlanRequest,
)
from academics.constants import REQUEST_REQUIRED_FIELDS, ALLOWED_REQUEST_STATUS_CHANGES
from academics.utils import create_study_plan_from_request


class CourseSerializer(serializers.ModelSerializer):
    """
    Course serializer
    """

    class Meta:
        model = Course
        fields = ['study_plan_subject', 'operating_plan_program', 'syllabus']


class CourseGroupSerializer(serializers.ModelSerializer):
    """
    Course group serializer
    """

    class Meta:
        model = CourseGroup
        fields = ['course', 'professor', 'group', 'capacity', 'classroom', 'start_date', 'end_date', ]


class ScheduleSerializer(serializers.ModelSerializer):
    """
    Schedule serializer
    """

    class Meta:
        model = Schedule
        fields = ['course', 'day', 'start_time', 'end_time', ]

    def validate(self, attrs):
        super().validate(attrs)
        # Validate the start time is before the end time
        if attrs['start_time'] > attrs['end_time']:
            raise ValidationError(
                {'start_time': 'La hora de inicio debe ser mayor a la hora de fin'}
            )

        return attrs

class CourseProgressSerializer(serializers.ModelSerializer):
    """
    CourseProgress serializer
    """

    class Meta:
        model = CourseProgress
        fields = ['tittle', 'description', 'percentage', ]


class StudyPlanRequestSerializer(serializers.ModelSerializer):
    """
    StudyPlanRequest serializer
    """

    class Meta:
        model = StudyPlanRequest
        fields = [
            'status',
            'program',
            'observation',
            'summary',
            'study_plan_file',
            'resolution_file',
        ]

    def validate(self, attrs):
        if self.partial:
            allowed_status = ALLOWED_REQUEST_STATUS_CHANGES[
                self.instance.status
            ]
            status = attrs['status']

            if status in allowed_status:
                required_field = REQUEST_REQUIRED_FIELDS.get(status, None)
                if required_field and required_field not in attrs:
                    raise ValidationError(
                        {required_field: f'{required_field} es obligatorio'}
                    )
            else:
                raise ValidationError('Operacion no valida')

        if not self.instance:
            try:
                create_study_plan_from_request(
                    None,
                    False,
                    attrs['study_plan_file'],
                    attrs['program']
                )

            except BadZipFile:
                raise ValidationError(
                    {'study_plan_file': 'Formato de archivo incorrecto'}
                )

        return attrs
