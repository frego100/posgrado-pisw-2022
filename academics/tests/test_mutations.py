import json
import os

from core.tests import ApiBaseTestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
)
from academics.models import (
    StudyPlanSubject,
    Course,
    CourseGroup,
    Schedule,
)
from academics.tests.mutations import (
    add_course,
    edit_course,
    add_group,
    edit_group,
    delete_group,
    add_schedule,
    edit_schedule,
    delete_schedule,
)


class BaseTestAcademicMutations(ApiBaseTestCase):
    """
    Base test class for tests cases of academic mutations
    """
    def setUp(self) -> None:
        super(BaseTestAcademicMutations, self).setUp()
        self.study_plan_subject = StudyPlanSubject.objects.create(
            subject="Tesis",
            study_plan=self.study_plan,
            credits=6,
            hours=16,
            semester=1
        )
        # Operating plan
        # (Its signal creates operating plan program and observation)
        self.operating_plan = OperatingPlan.objects.create(
            graduate_unit=self.graduate_unit, year='2022',
        )
        self.op_plan_program = OperatingPlanProgram.objects.first()
        self.course = Course.objects.first()
        # Create Group
        self.course_group = CourseGroup.objects.create(
            course=self.course,
            professor=self.professor,
            group="B",
            capacity=30,
            start_date="2021-11-12",
            end_date="2022-03-15",
            classroom="500"
        )


class TestAddCourseMutation(BaseTestAcademicMutations):
    """
    Test cases for AddCourse mutation
    """

    def setUp(self) -> None:
        super().setUp()
        file_route = f"{os.getcwd()}/tests/files/Documento.pdf"
        syllabus_doc = SimpleUploadedFile(
            'syllabus.pdf', content=open(file_route, 'rb').read(),
        )

    # def test_mutation_to_add_course_without_errors(self):
    #     """
    #     Test mutation to add a course correctly
    #     """

    #     response = self.query(
    #         add_course,
    #         variables={
    #             "input": {
    #                 "studyPlanSubject": self.study_plan_subject.id,
    #                 "operatingPlanProgram": self.op_plan_program.id,
    #                 "syllabus": "doc.pdf"
    #             }
    #         }
    #     )
    #     result = json.loads(response.content)
    #     result = result['data']['addCourse']

    #     course_registration = Course.objects.filter(
    #         study_plan_subject=self.study_plan_subject,
    #         operating_plan_program=self.op_plan_program,
    #         syllabus="doc.pdf"
    #     )

    #     self.assertEqual(result['feedback']['status'], 'SUCCESS')
    #     self.assertIsNone(result['errors'])
    #     self.assertTrue(course_registration.exists())
    #     self.assertTrue(course_registration.count() == 1)

    def test_mutation_to_add_course_with_invalid_data(self):
        """
        Test mutation to add course with invalid data as parameters
        """
        response = self.query(
            add_course,
            variables={
                "input": {
                    "studyPlanSubject": -1,
                    "operatingPlanProgram": -1,
                    "syllabus": "doc.pdf"
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addCourse']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo agregar el curso')
        self.assertIsNotNone(result['errors'])


class TestEditCourseMutation(BaseTestAcademicMutations):
    """
    Test cases for EditCourse mutation
    """

    def setUp(self) -> None:
        super().setUp()
        file_route = f"{os.getcwd()}/tests/files/Documento.pdf"
        syllabus_doc = SimpleUploadedFile(
            'syllabus.pdf', content=open(file_route, 'rb').read(),
        )

    # def test_mutation_to_edit_course_without_errors(self):
    #     """
    #     Test mutation to edit a course correctly
    #     """
    #     response = self.query(
    #         edit_course,
    #         variables={
    #             "input": {
    #                 "courseId": self.course.id,
    # 		        "syllabus": 'test.pdf'
    #             }
    #         }
    #     )
    #     result = json.loads(response.content)
    #     result = result['data']['editCourse']

    #     self.assertEqual(result['feedback']['status'], 'SUCCESS')
    #     self.assertIsNone(result['errors'])

    def test_mutation_to_edit_course_with_invalid_data(self):
        """
        Test mutation to edit course with
        invalid data as parameters
        """
        response = self.query(
            edit_course,
            variables={
                "input": {
                    "courseId": -1,
                    "syllabus": 'test.pdf'
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editCourse']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo editar el curso')
        self.assertIsNotNone(result['errors'])


class TestAddGroupMutation(BaseTestAcademicMutations):
    """
    Test cases for AddGroup mutation
    """

    def test_mutation_to_add_group_without_errors(self):
        """
        Test mutation to add a group correctly
        """
        response = self.query(
            add_group,
            variables={
                "input": {
                    "course": self.course.id
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addGroup']

        course_registration = CourseGroup.objects.filter(
            course=self.course,
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertTrue(course_registration.exists())
        self.assertEqual(course_registration.count(), 2)

    def test_mutation_to_add_group_with_invalid_data(self):
        """
        Test mutation to add group with
        invalid data as parameters
        """
        response = self.query(
            add_group,
            variables={
                "input": {
                    "course": -1
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addGroup']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo agregar el grupo')
        self.assertIsNotNone(result['errors'])


class TestEditGroupMutation(BaseTestAcademicMutations):
    """
    Test cases for EditGroup mutation
    """

    def test_mutation_to_edit_group_without_errors(self):
        """
        Test mutation to edit a group correctly
        """
        response = self.query(
            edit_group,
            variables={
                "input": {
                    "courseGroupId": self.course_group.id,
                    "professor": self.professor.id,
                    "group": "A",
                    "capacity": 25,
                    "startDate": "2022-01-01",
                    "endDate": "2022-01-13"
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editGroup']

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        # self.assertEqual(result['group']['group'], self.course_group.group)
        # self.assertEqual(result['group']['capacity'], self.course_group.capacity)

    def test_mutation_to_edit_group_with_invalid_data(self):
        """
        Test mutation to edit group with
        invalid data as parameters
        """
        response = self.query(
            edit_group,
            variables={
                "input": {
                    "courseGroupId": -1,
                    "professor": -1,
                    "group": "A",
                    "capacity": 25,
                    "startDate": "2022-01-01",
                    "endDate": "2022-01-13"
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editGroup']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo editar el grupo')
        self.assertIsNotNone(result['errors'])


class TestDeleteGroupMutation(BaseTestAcademicMutations):
    """
    Test cases for DeleteGroup mutation
    """

    def test_mutation_to_delete_group_without_errors(self):
        """
        Test mutation to delete a group correctly
        """
        response = self.query(
            delete_group,
            variables={
                "groupId": self.course_group.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['deleteGroup']
        
        course_group_registration = CourseGroup.objects.filter(
            id=self.course_group.id
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertFalse(course_group_registration.exists())
        self.assertTrue(course_group_registration.count() == 0)

    def test_mutation_to_delete_group_with_invalid_data(self):
        """
        Test mutation to delete group with
        invalid data as parameters
        """
        response = self.query(
            delete_group,
            variables={
                "groupId": -1
            }
        )
        result = json.loads(response.content)
        result = result['data']['deleteGroup']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo eliminar el grupo')


class TestAddScheduleMutation(BaseTestAcademicMutations):
    """
    Test cases for AddSchedule mutation
    """

    def test_mutation_to_add_schedule_without_errors(self):
        """
        Test mutation to add a schedule correctly
        """
        response = self.query(
            add_schedule,
            variables={
                "input": [
                    {
                        "course": self.course_group.id,
                        "day": "MONDAY",
                        "startTime": "10:00:00",
                        "endTime": "15:00:00"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['addSchedule']

        schedule_registration = Schedule.objects.filter(
            course=self.course_group.id,
            day=1,
            start_time="10:00:00",
            end_time="15:00:00"
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertTrue(schedule_registration.exists())
        self.assertTrue(schedule_registration.count() == 1)

    def test_mutation_to_add_schedule_with_invalid_data(self):
        """
        Test mutation to add a schedule with
        invalid data as parameters
        """
        response = self.query(
            add_schedule,
            variables={
                "input": [
                    {
                        "course": -1,
                        "day": "MONDAY",
                        "startTime": "10:00:00",
                        "endTime": "15:00:00"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['addSchedule']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo agregar el horario')
        self.assertIsNotNone(result['errors'])


class TestEditScheduleMutation(BaseTestAcademicMutations):
    """
    Test cases for EditSchedule mutation
    """

    def setUp(self) -> None:
        super().setUp()
        self.schedule = Schedule.objects.create(
            course=self.course_group, day=1,
            start_time="07:00:00", end_time="09:30:00"
        )

    def test_mutation_to_edit_schedule_without_errors(self):
        """
        Test mutation to edit a schedule correctly
        """
        response = self.query(
            edit_schedule,
            variables={
                "input": [
                    {
                        "day": "FRIDAY",
                        "scheduleId": self.schedule.id,
                        "startTime": "07:00:00",
                        "endTime": "10:00:00"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['editSchedule']
        self.schedule.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertEqual(str(self.schedule.start_time),
                         result['schedules'][0]['startTime'])
        self.assertEqual(str(self.schedule.end_time),
                         result['schedules'][0]['endTime'])

    # def test_mutation_to_edit_schedule_with_invalid_data(self):
    #     """
    #     Test mutation to edit schedule with
    #     invalid data as parameters
    #     """
    #     response = self.query(
    #         edit_schedule,
    #         variables={
    #             "input": [
    #                 {
    #                     "scheduleId": self.schedule.id,
    #                     "startTime": "-10",
    #                     "endTime": "15:00:00"
    #                 }
    #             ]
    #         }
    #     )
    #     result = json.loads(response.content)
    #     print(result)
    #     result = result['data']['editSchedule']

    #     self.assertEqual(result['feedback']['status'], 'ERROR')
    #     self.assertEqual(result['feedback']['message'], 'No se pudo editar el horario')
    #     self.assertIsNotNone(result['errors'])


class TestDeleteScheduleMutation(BaseTestAcademicMutations):
    """
    Test cases for DeleteSchedule mutation
    """

    def setUp(self) -> None:
        super().setUp()
        self.schedule = Schedule.objects.create(
            course=self.course_group, day=1,
            start_time="07:00:00", end_time="09:30:00"
        )

    def test_mutation_to_delete_schedule_without_errors(self):
        """
        Test mutation to delete a schedule correctly
        """
        response = self.query(
            delete_schedule,
            variables={"scheduleId": self.schedule.id}
        )
        result = json.loads(response.content)
        result = result['data']['deleteSchedule']

        schedule_registration = Schedule.objects.filter(
            course=self.course_group, day=1,
            start_time="07:00:00", end_time="09:30:00"
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertFalse(schedule_registration.exists())
        self.assertTrue(schedule_registration.count() == 0)

    def test_mutation_to_delete_schedule_with_invalid_data(self):
        """
        Test mutation to delete schedule with
        invalid data as parameters
        """
        response = self.query(
            delete_schedule,
            variables={"scheduleId": -1}
        )
        result = json.loads(response.content)
        result = result['data']['deleteSchedule']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo eliminar el horario')
