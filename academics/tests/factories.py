import factory
from academics.models import Course, StudyPlan, StudyPlanSubject
from operating_plan.tests.factories import OperatingPlanProgramFactory
from organization.tests.factories import StudyProgramFactory


class StudyPlanFactory(factory.Factory):
    class Meta:
        model = StudyPlan

    # study_program = factory.SubFactory(StudyProgram)
    year = 2022
    code = "123"


class StudyPlanSubjectFactory(factory.Factory):
    class Meta:
        model = StudyPlanSubject

    study_plan = factory.SubFactory(StudyPlanFactory)
    subject = "TestSubject"
    credits = 4
    semester = 1
    hours = 32


class CourseFactory(factory.Factory):
    class Meta:
        model = Course

    study_plan_subject = factory.SubFactory(StudyPlanSubjectFactory)
    operating_plan_program = factory.SubFactory(OperatingPlanProgramFactory)
