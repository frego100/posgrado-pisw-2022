"""
Mutations of academic app
"""
add_course = '''
    mutation AddCourse($input: AddCourseInput!) {
        addCourse(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            course{
                id
            }
        }
    }  
'''
edit_course = '''
    mutation EditCourse($input: EditCourseInput!) {
        editCourse(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            course{
                id
            }
        }
    }  
'''
add_group = '''
    mutation AddGroup($input: AddGroupInput!) {
        addGroup(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            group{
                id
                group
                capacity
                startDate
                endDate
            }
        }
    }  
'''
edit_group = '''
    mutation EditGroup($input: EditGroupInput!) {
        editGroup(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            group{
                id
                group
                capacity
                startDate
                endDate
            }
        }
    }   
'''
delete_group = '''
    mutation DeleteGroup($groupId: ID!) {
        deleteGroup(groupId: $groupId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }   
'''
add_schedule = '''
    mutation AddSchedule($input: [AddScheduleInput]!) {
        addSchedule(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            schedules{
                id
                startTime
                endTime
            }
        }
    } 
'''
edit_schedule = '''
    mutation EditSchedule($input: [EditScheduleInput]!) {
        editSchedule(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            schedules{
                id
                day
                startTime
                endTime
            }
        }
    }  
'''
delete_schedule = '''
    mutation DeleteSchedule($scheduleId: ID!) {
        deleteSchedule(scheduleId: $scheduleId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
add_course_progress = '''
    mutation AddCourseProgress($input: AddCourseProgressInput!) {
        addCourseProgress(inputs: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            courseProgress{
                id
                coursegroup{
                    id
                }
                tittle
                description
                percentage
                date
            }
        }
    }  
'''
edit_course_progress = '''
    mutation EditCourseProgress($input: EditCourseProgressInput!) {
        editCourseProgress(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            courseProgress{
                id
                coursegroup{
                    id
                }
                tittle
                description
                percentage
                date
            }
        }
    } 
'''
delete_course_progress = '''
    mutation DeleteCourseProgress($courseProgressId: ID!) {
        deleteCourseProgress(courseProgressId: $courseProgressId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
