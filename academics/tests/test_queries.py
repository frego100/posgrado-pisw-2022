import json

from academics.models import (
    Course,
    StudyPlanSubject,
    CourseGroup,
    Schedule
)
from core.tests import ApiBaseTestCase
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
)
from academics.tests.queries import (
    get_all_courses,
    get_course,
    get_schedules)


class TestAcademicsQueries(ApiBaseTestCase):
    """
    Test graphql queries of Academic app
    """

    def setUp(self) -> None:
        super().setUp()
        # Subject
        self.study_plan_subject = StudyPlanSubject.objects.create(
            subject="Tesis",
            study_plan=self.study_plan,
            credits=6,
            hours=16,
            semester=1
        )
        # Operating plan (Its signal creates courses for existing subjects)
        OperatingPlan.objects.create(
            graduate_unit=self.graduate_unit, year=2022, status=1
        )
        # Course, group and schedule
        self.course = Course.objects.first()
        self.course_group = CourseGroup.objects.create(
            course=self.course,
            professor=self.professor,
            group="a",
            capacity=30,
            start_date="2021-11-12",
            end_date="2022-03-15",
            classroom="500"
        )
        self.schedule = Schedule.objects.create(
            course=self.course_group, day=1,
            start_time="07:00:00", end_time="09:30:00"
        )

    def test_get_all_courses_query_without_errors(self):
        """
        Test query to get all courses without errors
        """
        op_plan_program = OperatingPlanProgram.objects.first()
        Course.objects.create(
            operating_plan_program=op_plan_program,
            study_plan_subject=self.study_plan_subject
        )

        self.course.syllabus = "silabo.pdf"
        self.course.save()

        response = self.query(
            get_all_courses,
            variables={
                "opPlanProgramId": op_plan_program.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['getAllCourses']

        self.assertEqual(len(result), 2)
        self.assertEqual(
            self.study_plan_subject.credits,
            result[0]['studyPlanSubject']['credits']
        )
        self.assertEqual(
            self.study_plan_subject.subject,
            result[0]['studyPlanSubject']['subject']
        )
        self.assertEqual(
            self.study_plan_subject.semester,
            result[0]['studyPlanSubject']['semester']
        )
        self.assertEqual(
            self.study_plan_subject.hours,
            result[0]['studyPlanSubject']['hours']
        )

    def test_get_all_courses_query_with_invalid_op_plan_program_id(self):
        """
        Test query to get all courses with invalid operating plan_program id
        """
        response = self.query(
            get_all_courses,
            variables={
                "opPlanProgramId": '-1'
            }
        )
        result = json.loads(response.content)

        self.assertEqual(len(result['data']['getAllCourses']), 0)

    def test_get_course_query_without_errors(self):
        """
        Test query to get a course by id without errors
        """
        self.course_group2 = CourseGroup.objects.create(
            course=Course.objects.first(),
            professor=self.professor,
            group="b",
            start_date="2021-11-12",
            end_date="2022-03-15",
            classroom="501"
        )
        self.course.syllabus = "silabo.pdf"
        self.course.save()

        response = self.query(
            get_course,
            variables={
                "courseId": self.course.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['getCourse']

        self.assertEqual(self.course.syllabus, result['syllabus'])
        self.assertEqual(
            self.study_plan_subject.subject,
            result['studyPlanSubject']['subject']
        )
        self.assertEqual(
            self.study_program.name,
            result['studyPlanSubject']['studyPlan']['studyProgram']['name']
        )
        self.assertEqual(len(result['coursegroupSet']), 2)
        self.assertEqual(
            self.course_group.classroom,
            result['coursegroupSet'][0]['classroom']
        )
        self.assertEqual(
            self.course_group.group, result['coursegroupSet'][0]['group']
        )
        self.assertEqual(
            self.course_group.start_date,
            result['coursegroupSet'][0]['startDate']
        )

    def test_get_course_query_with_invalid_course_id(self):
        """
        Test query to get a course with invalid course id
        """
        response = self.query(
            get_course,
            variables={
                "courseId": '-1'
            }
        )
        result = json.loads(response.content)

        self.assertIsNone(result['data']['getCourse'])

    def test_get_schedules_query_without_errors(self):
        """
        Test query to get schedules of a Course Group without errors
        """
        response = self.query(
            get_schedules,
            variables={
                "courseGroupId": self.course.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['getSchedules']

        self.assertEqual(self.schedule.get_day_display(), result[0]['day'])
        self.assertEqual(self.schedule.start_time, result[0]['startTime'])
        self.assertEqual(self.schedule.end_time, result[0]['endTime'])

    def test_get_schedules_query_with_invalid_course_group_id(self):
        """
        Test query to get schedules of a Course Group with invalid
        course group id
        """
        response = self.query(
            get_schedules,
            variables={
                "courseGroupId": '-1'
            }
        )
        result = json.loads(response.content)
        self.assertEqual(len(result['data']['getSchedules']), 0)

    def tearDown(self) -> None:
        pass
