"""
Queries of academic app
"""

get_all_courses = '''
    query($opPlanProgramId: ID){
        getAllCourses(opPlanProgramId: $opPlanProgramId){
            id
            status
            studyPlanSubject{
                subject
                credits
                semester
                hours
            }
            syllabus
            syllabusUrl
        }
    }
'''
get_course = '''
    query($courseId: ID){
        getCourse(courseId: $courseId){
            id
            status
            studyPlanSubject{
                studyPlan{
                    studyProgram{
                        name
                    }
                }
                subject
            }
            syllabus
            syllabusUrl
            coursegroupSet{
                id
                professor{
                    user{
                        firstName
                        lastName
                    }
                }
                group
                capacity
                startDate
                endDate
                classroom
            }
        }
    }
'''
get_schedules = '''
    query($courseGroupId: ID){
        getSchedules(courseGroupId: $courseGroupId){
            id
            day
            startTime
            endTime
        }
    }
'''
