from datetime import date

from graphene import ObjectType, List, ID, Field

from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required

from academics.models import (
    Course,
    Schedule,
    CourseProgress,
    CourseGroup,
    StudyPlan,
    StudyPlanSubject,
    StudyPlanRequest,
)
from academics.schema.types import (
    CourseType,
    ScheduleType,
    CourseProgressType,
    CourseGroupType,
    StudyPlanType,
    StudyPlanSubjectType,
    StudyPlanRequestType,
    ProfessorGroupsType,
)
from account.models import Student
from account.schema.types import UserType
from core.utils import get_object_by_id
from enrollment.schema.types import EnrollmentType
from operating_plan.constants import OperatingPlanStatusChoices
from operating_plan.models import ProfessorRegistration


class Query(ObjectType):
    """
    Academics Query Object
    """

    get_all_courses = List(
        CourseType,
        op_plan_program_id=ID(),
        description='Get all courses of operating plan'
    )
    get_course = Field(
        CourseType,
        course_id=ID(),
        description='Get course by id'
    )
    get_schedules = List(
        ScheduleType,
        course_group_id=ID(),
        description='Get schedules of course group'
    )
    get_all_courses_progress = List(
        CourseProgressType,
        course_group_id=ID(),
        description='Get all courses progress of a group'
    )
    get_course_progress = Field(
        CourseProgressType,
        course_progress_id=ID(),
        description='Get course progress by id'
    )
    get_all_professor_courses = List(
        CourseGroupType,
        process_id=ID(),
        description='Get all courses group of a professor'
    )
    get_study_plans = List(
        StudyPlanType,
        study_program_id=ID(),
        description='Get study plans by study program'
    )
    get_study_plan_subjects = List(
        StudyPlanSubjectType,
        study_plan_id=ID(),
        description='Get study plan subject by study plan'
    )
    get_professor_groups = Field(
        ProfessorGroupsType,
        professor_registration_id=ID(),
        description='Get professor groups'
    )
    get_professor_course_groups = List(
        CourseGroupType,
        operating_plan_program_id=ID(required=True),
        professor_id=ID(required=True),
        description=_('Get courses assigned to professor')
    )

    # Study plan request

    get_study_plan_request = Field(
        StudyPlanRequestType,
        program_id=ID(),
        description=_('Get latest study plan request')
    )
    get_course_group = Field(
        CourseGroupType,
        course_group_id=ID(),
        description=_('Get course group object with a given id')
    )
    get_course_groups_by_study_program = List(
        CourseGroupType,
        study_program_id=ID(),
        description=_('Get course groups by study program')
    )

    @login_required
    def resolve_get_all_courses(self, info, op_plan_program_id):
        return Course.objects.filter(
            operating_plan_program_id=op_plan_program_id
        ).order_by('study_plan_subject__code')

    @login_required
    def resolve_get_course(self, info, course_id):
        return get_object_by_id(Course, course_id)

    @login_required
    def resolve_get_schedules(self, info, course_group_id):
        return Schedule.objects.filter(course_id=course_group_id)

    @login_required
    def resolve_get_all_courses_progress(self, info, course_group_id):
        return CourseProgress.objects.filter(
            academic_session__schedule__course_id=course_group_id
        ).order_by('academic_session__date')

    @login_required
    def resolve_get_course_progress(self, info, course_progress_id):
        return get_object_by_id(CourseProgress, course_progress_id)

    @login_required
    def resolve_get_all_professor_courses(self, info, process_id):
        professor_id = info.context.user.professor.id
        return CourseGroup.objects.filter(
            professor__professor_id=professor_id,
            course__operating_plan_program__operating_plan__op_plan_process=process_id
        )

    @login_required
    def resolve_get_study_plans(self, info, study_program_id):
        return StudyPlan.objects.filter(
            study_program=study_program_id
        )

    @login_required
    def resolve_get_study_plan_subjects(self, info, study_plan_id):
        return StudyPlanSubject.objects.filter(
            study_plan=study_plan_id
        ).order_by('code')

    @login_required
    def resolve_get_study_plan_request(self, info, program_id):
        return StudyPlanRequest.objects.filter(program_id=program_id).first()

    @login_required
    def resolve_get_professor_groups(self, info, professor_registration_id):
        try:
            professor_registration = ProfessorRegistration.objects.select_related(
                'operating_plan_program__study_program'
            ).get(
                pk=professor_registration_id
            )
        except ProfessorRegistration.DoesNotExist:
            return None

        professor_id = professor_registration.professor_id
        unit_id = professor_registration.operating_plan_program.study_program.graduate_unit_id
        program_groups = CourseGroup.objects.filter(
            professor=professor_registration_id
        )
        external_groups = CourseGroup.objects.filter(
            professor__professor_id=professor_id,
            professor__operating_plan_program__study_program__graduate_unit=unit_id
        ).exclude(professor_id=professor_registration_id)
        professor_groups = {
            'program_groups': program_groups,
            'external_groups': external_groups
        }

        return professor_groups

    @login_required
    def resolve_get_professor_course_groups(self, info, **kwargs):
        operating_plan_program_id = kwargs.get('operating_plan_program_id')
        professor_id = kwargs.get('professor_id')

        return CourseGroup.objects.filter(
            professor__professor_id=professor_id,
            course__operating_plan_program_id=operating_plan_program_id,
            course__operating_plan_program__operating_plan__status=OperatingPlanStatusChoices.APPROVED
        )

    @login_required
    def resolve_get_course_group(self, info, course_group_id):
        return get_object_by_id(CourseGroup, course_group_id)

    @login_required
    def resolve_get_course_groups_by_study_program(
            self, info, study_program_id
    ):
        return CourseGroup.objects.filter(
            course__operating_plan_program__study_program_id=study_program_id
        ).distinct('course')
