"""
Academics inputs
"""
from graphene import (
    InputObjectType,
    ID,
    String,
    Int,
    Date,
    Time,
    Enum,
    Field,
)
from graphene_file_upload.scalars import Upload


class AddCourseInput(InputObjectType):
    """
    Add course input
    """

    study_plan_subject = ID()
    operating_plan_program = ID()
    syllabus = Upload()


class EditCourseInput(InputObjectType):
    """
    Edit course input
    """

    course_id = ID()
    syllabus = Upload()


class AddGroupInput(InputObjectType):
    """
    Add group input
    """

    course = ID()


class EditGroupInput(InputObjectType):
    """
    Edit group input
    """

    course_group_id = ID()
    professor = ID()
    group = String()
    capacity = Int()
    classroom = String()
    start_date = Date()
    end_date = Date()


class DayEnum(Enum):
    """
    Day enum
    """

    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7


class AddScheduleInput(InputObjectType):
    """
    Add schedule input
    """

    course = ID()
    day = Field(DayEnum)
    start_time = Time()
    end_time = Time()


class EditScheduleInput(InputObjectType):
    """
    Edit schedule input
    """

    schedule_id = ID()
    day = Field(DayEnum)
    start_time = Time()
    end_time = Time()


class AddCourseProgressInput(InputObjectType):
    """
    Add course progress input
    """

    coursegroup = ID()
    tittle = String()
    description = String()
    percentage = Int()
    date = Date()


class EditCourseProgressInput(InputObjectType):
    """
    Edit course progress input
    """

    course_progress_id = ID(required=True)
    tittle = String()
    description = String()
    percentage = Int()


class SendStudyPlanRequestInput(InputObjectType):
    """
    Send study plan request input
    """

    program = ID(required=True)
    study_plan_file = Upload(required=True)
    summary = String(required=True)
