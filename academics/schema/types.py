"""
Academics types
"""
import graphene
from django.utils import timezone
from graphene import Int, ObjectType, List, Boolean, Date
from graphene.types import String
from graphene_django.types import DjangoObjectType

from django.db.models import Max
from django.contrib.auth.models import Group

from academics.models import (
    StudyPlan,
    StudyPlanSubject,
    Course,
    Schedule,
    CourseGroup,
    CourseProgress,
    StudyPlanRequest,
)
from academics.constants import RequestStatusChoices
from enrollment.constants import ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES
from enrollment.models import StudentGrade
from enrollment.utils import verify_grade_register_period


class GroupType(DjangoObjectType):
    """
    Group type
    """

    class Meta:
        model = Group


class StudyPlanType(DjangoObjectType):
    """
    StudyPlan type
    """

    class Meta:
        model = StudyPlan


class StudyPlanSubjectType(DjangoObjectType):
    """
    StudyPlanSubject type
    """

    class Meta:
        model = StudyPlanSubject


class CourseGroupType(DjangoObjectType):
    """
    CourseGroup type
    """

    class Meta:
        model = CourseGroup

    percentage = Int()
    allow_grades_register = Boolean()
    end_grades_register_period = Date()
    grades_assigned = Boolean()

    def resolve_percentage(self, info):
        max_percentage = CourseProgress.objects.filter(
            academic_session__schedule__course=self.id
        ).aggregate(Max('percentage'))

        return max_percentage['percentage__max']

    def resolve_allow_grades_register(self, info):
        return verify_grade_register_period(self)

    def resolve_end_grades_register_period(self: CourseGroup, info):
        return self.end_date + timezone.timedelta(
            days=ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES
        )

    def resolve_grades_assigned(self: CourseGroup, info):
        return StudentGrade.objects.filter(
            student_enrollment__group_id=self.id
        ).exists()


class CourseType(DjangoObjectType):
    """
    Course type
    """

    class Meta:
        model = Course

    status = graphene.Boolean()
    syllabus_url = graphene.String()
    number_of_groups = Int()
    number_of_students = Int()
    coursegroup_set = graphene.List(
        CourseGroupType, enabled_groups=graphene.Boolean(required=False)
    )
    has_groups = graphene.Boolean()

    def resolve_status(self, info):
        if not self.syllabus or not self.coursegroup_set.exists():
            return False

        for group in self.coursegroup_set.all():
            if (
                    group.professor_id is None
                    or group.group is None
                    or group.capacity is None
                    or group.start_date is None
                    or group.end_date is None
            ):
                return False

        return True

    def resolve_syllabus_url(self, info):
        if self.syllabus:
            return self.syllabus.url

        return None

    def resolve_number_of_groups(self: Course, info):
        return self.coursegroup_set.filter(is_deleted=False).count()

    def resolve_number_of_students(self: Course, info):
        return sum(
            group.groupenrollment_set.count()
            for group in self.coursegroup_set.all()
        )

    def resolve_coursegroup_set(self: Course, info, enabled_groups=False):
        if enabled_groups:
            return (x for x in self.coursegroup_set.all() if x.is_filled)

        return self.coursegroup_set.all()

    def resolve_has_groups(self: Course, info):
        course_groups = self.coursegroup_set.filter(is_deleted=False).count()
        return course_groups > 0


class ScheduleType(DjangoObjectType):
    """
    Schedule type
    """

    day = String()

    class Meta:
        model = Schedule

    def resolve_day(self: Schedule, info):
        return self.get_day_display()


class CourseProgressType(DjangoObjectType):
    """
    CourseProgress type
    """

    class Meta:
        model = CourseProgress


class StudyPlanRequestType(DjangoObjectType):
    """
    StudyPlanRequest type
    """

    status = String()

    class Meta:
        model = StudyPlanRequest

    def resolve_status(self: StudyPlanRequest, info):
        return self.get_status_display()

    def resolve_study_plan_file(self: StudyPlanRequest, info):
        return self.study_plan_file.url

    def resolve_approval_file(self: StudyPlanRequest, info):
        if self.approval_file:
            return self.approval_file.url

        return None


RequestStatusEnum = graphene.Enum.from_enum(RequestStatusChoices)


class ProfessorGroupsType(ObjectType):
    """
    Professor groups type
    """

    program_groups = List(CourseGroupType)
    external_groups = List(CourseGroupType)
