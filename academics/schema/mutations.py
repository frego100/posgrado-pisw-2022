"""
Academics mutations
"""
from graphene import (
    ObjectType,
    Field,
    List,
    ID,
    String,
)
from graphene_django.types import ErrorType
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required

from academics.constants import RequestStatusChoices
from academics.messages.success_messages import (
    COURSE_ADDED,
    COURSE_CHANGED,
    GROUP_ADDED,
    GROUP_CHANGED,
    GROUP_DELETED,
    SCHEDULE_ADDED,
    SCHEDULE_CHANGED,
    SCHEDULE_DELETED,
    COURSE_PROGRESS_ADDED,
    COURSE_PROGRESS_DELETED,
    COURSE_PROGRESS_CHANGED,
    STUDY_PLAN_REQUEST_SENT,
    STUDY_PLAN_REQUEST_UPDATED,
)
from academics.messages.error_messages import (
    COULD_NOT_ADD_COURSE,
    COULD_NOT_CHANGE_COURSE,
    COULD_NOT_ADD_GROUP,
    COULD_NOT_CHANGE_GROUP,
    COULD_NOT_DELETE_GROUP,
    COULD_NOT_ADD_SCHEDULE,
    COULD_NOT_CHANGE_SCHEDULE,
    COULD_NOT_DELETE_SCHEDULE,
    COULD_NOT_ADD_COURSE_PROGRESS,
    COULD_NOT_DELETE_COURSE_PROGRESS,
    COULD_NOT_CHANGE_COURSE_PROGRESS,
    COULD_NOT_SEND_STUDY_PLAN_REQUEST,
    COULD_NOT_UPDATE_STUDY_PLAN_REQUEST, COULD_NOT_SEND_STUDY_PLAN_REQUEST_ON_COURSE,
)
from academics.models import (
    Course,
    CourseGroup,
    Schedule,
    CourseProgress,
    StudyPlanRequest,
)
from academics.schema.inputs import (
    AddCourseInput,
    EditCourseInput,
    AddGroupInput,
    EditGroupInput,
    AddScheduleInput,
    EditScheduleInput,
    AddCourseProgressInput,
    EditCourseProgressInput,
    SendStudyPlanRequestInput,
)
from academics.schema.types import (
    CourseType,
    CourseGroupType,
    ScheduleType,
    CourseProgressType,
    StudyPlanRequestType,
    RequestStatusEnum,
)
from academics.serializers import (
    CourseSerializer,
    CourseGroupSerializer,
    ScheduleSerializer, CourseProgressSerializer,
    StudyPlanRequestSerializer,
)
from academics.utils import create_study_plan_from_request
from core.schema.mutations import BaseMutation
from core.schema.types import FeedbackType, EnumStatus
from core.utils import get_object_by_id
from operating_plan.constants import OperatingPlanStatusChoices


class AddCourseMutation(BaseMutation):
    """
    Add course mutation
    """
    course = Field(CourseType)

    class Arguments:
        input = AddCourseInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        serializer = CourseSerializer(data=inputs)

        if serializer.is_valid():
            course = serializer.save()

            return AddCourseMutation(
                feedback=FeedbackType(
                    message=COURSE_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                course=course
            )

        return AddCourseMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_COURSE,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class EditCourseMutation(BaseMutation):
    """
    Edit course mutation
    """
    course = Field(CourseType)

    class Arguments:
        input = EditCourseInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        course_id = inputs.pop('course_id', None)
        course = get_object_by_id(Course, course_id)
        serializer = CourseSerializer(instance=course, data=inputs, partial=True)

        if serializer.is_valid():
            course = serializer.save()

            return EditCourseMutation(
                feedback=FeedbackType(
                    message=COURSE_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                course=course
            )

        return EditCourseMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_COURSE,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class AddGroupMutation(BaseMutation):
    """
    Add group mutation
    """
    group = Field(CourseGroupType)

    class Arguments:
        input = AddGroupInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        serializer = CourseGroupSerializer(data=inputs)

        if serializer.is_valid():
            group = serializer.save()

            return AddGroupMutation(
                feedback=FeedbackType(
                    message=GROUP_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                group=group
            )

        return AddGroupMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_GROUP,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class EditGroupMutation(BaseMutation):
    """
    Edit group mutation
    """
    group = Field(CourseGroupType)

    class Arguments:
        input = EditGroupInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        course_group_id = inputs.pop('course_group_id')
        course_group = get_object_by_id(CourseGroup, course_group_id)
        serializer = CourseGroupSerializer(instance=course_group, data=inputs, partial=True)

        if serializer.is_valid():
            group = serializer.save()

            return EditGroupMutation(
                feedback=FeedbackType(
                    message=GROUP_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                group=group
            )

        return EditGroupMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_GROUP,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class AddScheduleMutation(BaseMutation):
    """
    Add schedule mutation
    """

    schedules = List(ScheduleType)

    class Arguments:
        input = List(AddScheduleInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, schedule in enumerate(inputs):
            serializer = ScheduleSerializer(data=schedule)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update({f'{key}_{index}': serializer.errors[key] for key in serializer.errors})

        if errors:

            return AddScheduleMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_SCHEDULE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        else:
            schedules = [serializer.save() for serializer in serializers]

            return AddScheduleMutation(
                feedback=FeedbackType(
                    message=SCHEDULE_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                schedules=schedules
            )


class EditScheduleMutation(BaseMutation):
    """
    Edit schedule mutation
    """

    schedules = List(ScheduleType)

    class Arguments:
        input = List(EditScheduleInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, schedule in enumerate(inputs):
            schedule_id = schedule.pop('schedule_id')
            schedule_model = get_object_by_id(Schedule, schedule_id)
            serializer = ScheduleSerializer(instance=schedule_model, data=schedule, partial=True)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update({f'{key}_{index}': serializer.errors[key] for key in serializer.errors})

        if errors:

            return EditScheduleMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_CHANGE_SCHEDULE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        else:
            schedules = [serializer.save() for serializer in serializers]

            return EditScheduleMutation(
                feedback=FeedbackType(
                    message=SCHEDULE_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                schedules=schedules
            )


class DeleteGroupMutation(BaseMutation):
    """
    Delete group mutation
    """

    class Arguments:
        group_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        group_id = kwargs.pop('group_id')
        group = get_object_by_id(CourseGroup, group_id)

        if group:
            if (
                    group.course.operating_plan_program.operating_plan.status == OperatingPlanStatusChoices.APPROVED
                    and group.is_in_operating_plan
            ):
                group.is_deleted = True
                group.save()

            else:
                group.delete()

            return DeleteGroupMutation(
                feedback=FeedbackType(
                    message=GROUP_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteGroupMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_GROUP,
                status=EnumStatus.ERROR
            )
        )


class DeleteScheduleMutation(BaseMutation):
    """
    Delete schedule mutation
    """

    class Arguments:
        schedule_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        schedule_id = kwargs.pop('schedule_id')
        schedule = get_object_by_id(Schedule, schedule_id)

        if schedule:
            schedule.delete()

            return DeleteScheduleMutation(
                feedback=FeedbackType(
                    message=SCHEDULE_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteScheduleMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_SCHEDULE,
                status=EnumStatus.ERROR
            )
        )


class EditCourseProgressMutation(BaseMutation):
    """
    Edit course progress mutation
    """
    course_progress = Field(CourseProgressType)

    class Arguments:
        input = EditCourseProgressInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        course_progress_id = inputs.pop('course_progress_id', None)
        course_progress = get_object_by_id(CourseProgress, course_progress_id)
        serializer = CourseProgressSerializer(instance=course_progress, data=inputs, partial=True)

        if serializer.is_valid():
            course_progress = serializer.save()

            return EditCourseProgressMutation(
                feedback=FeedbackType(
                    message=COURSE_PROGRESS_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                course_progress=course_progress
            )

        return EditCourseProgressMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_COURSE_PROGRESS,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class DeleteCourseProgressMutation(BaseMutation):
    """
    Delete course progress mutation
    """

    class Arguments:
        course_progress_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        course_progress_id = kwargs.pop('course_progress_id')
        course_progress = get_object_by_id(CourseProgress, course_progress_id)

        if course_progress:
            course_progress.delete()

            return DeleteCourseProgressMutation(
                feedback=FeedbackType(
                    message=COURSE_PROGRESS_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteCourseProgressMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_COURSE_PROGRESS,
                status=EnumStatus.ERROR
            )
        )


class SendStudyPlanRequestMutation(BaseMutation):
    """
    Send study plan request mutation
    """

    study_plan_request = Field(StudyPlanRequestType)

    class Arguments:
        input = SendStudyPlanRequestInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        program = inputs['program']

        # Only one study plan request can be active at the same time
        study_plan_requests = StudyPlanRequest.objects.filter(
            program=program,
        ).exclude(
            status__in=[
                RequestStatusChoices.APPROVED, RequestStatusChoices.REJECTED
            ]
        )
        if study_plan_requests.exists():
            return SendStudyPlanRequestMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_SEND_STUDY_PLAN_REQUEST_ON_COURSE,
                    status=EnumStatus.ERROR
                )
            )

        serializer = StudyPlanRequestSerializer(data=inputs)
        if serializer.is_valid():
            study_plan_request = serializer.save()
            return SendStudyPlanRequestMutation(
                feedback=FeedbackType(
                    message=STUDY_PLAN_REQUEST_SENT,
                    status=EnumStatus.SUCCESS
                ),
                study_plan_request=study_plan_request
            )

        return SendStudyPlanRequestMutation(
            feedback=FeedbackType(
                message=COULD_NOT_SEND_STUDY_PLAN_REQUEST,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class ChangeStudyPlanRequestStatusMutation(BaseMutation):
    """
    Observe study plan request mutation
    """

    class Arguments:
        study_plan_request_id = ID(required=True)
        observation = String()
        resolution_file = Upload()
        status = RequestStatusEnum(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        study_plan_request_id = kwargs.pop('study_plan_request_id')
        study_plan_request = get_object_by_id(
            StudyPlanRequest,
            study_plan_request_id
        )
        serializer = StudyPlanRequestSerializer(
            instance=study_plan_request,
            data=kwargs,
            partial=True
        )

        if serializer.is_valid():
            study_plan_request = serializer.save()
            if study_plan_request.status == RequestStatusChoices.APPROVED:
                create_study_plan_from_request(study_plan_request)

            return ChangeStudyPlanRequestStatusMutation(
                feedback=FeedbackType(
                    message=STUDY_PLAN_REQUEST_UPDATED,
                    status=EnumStatus.SUCCESS
                )
            )

        return ChangeStudyPlanRequestStatusMutation(
            feedback=FeedbackType(
                message=COULD_NOT_UPDATE_STUDY_PLAN_REQUEST,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class Mutation(ObjectType):
    add_course = AddCourseMutation.Field()
    edit_course = EditCourseMutation.Field()
    add_group = AddGroupMutation.Field()
    edit_group = EditGroupMutation.Field()
    delete_group = DeleteGroupMutation.Field()
    add_schedule = AddScheduleMutation.Field()
    edit_schedule = EditScheduleMutation.Field()
    delete_schedule = DeleteScheduleMutation.Field()
    edit_course_progress = EditCourseProgressMutation.Field()
    delete_course_progress = DeleteCourseProgressMutation.Field()
    send_study_plan_request = SendStudyPlanRequestMutation.Field()
    change_study_plan_request_status = ChangeStudyPlanRequestStatusMutation.Field()
