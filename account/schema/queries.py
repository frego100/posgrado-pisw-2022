"""
Account queries
"""
from google_auth_oauthlib.flow import Flow
from graphene import (
    ObjectType,
    List,
    Field,
    ID, Int, String,
)
from graphql_jwt.decorators import login_required

from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from account.constants import SCHOOL_MANAGER_GROUP_NAME
from account.schema.types import (
    UserType, UnitManagerType, ProfessorType, DegreeType, StudentType
)
from account.models import User, UnitManager, Professor, Degree, Student
from core.utils import get_object_by_id
from operating_plan.models import ProfessorRegistration
from operating_plan.schema.types import ProfessorRegistrationType


class Query(ObjectType):
    """
    Account Query Object
    """

    get_users = List(UserType, description=_('Get all users'))
    get_unit_managers = List(UnitManagerType, description=_('Get all unit managers'))
    get_professor = Field(
        ProfessorRegistrationType,
        professor_registration_id=ID(),
        description=_('Get professor of operating plan')
    )
    get_degrees = List(
        DegreeType,
        professor_id=ID(),
        description=_('Get professor degrees')
    )
    get_google_oauth_link = Field(String)
    me = Field(UserType)
    search_professor = Field(
        ProfessorType,
        id_type=Int(),
        id_number=String(),
        description=_('Search professor by DNI or Foreign Card')
    )
    get_student_by_cui = Field(
        StudentType,
        cui=String()
    )
    search_user = Field(
        UserType,
        id_type=Int(),
        id_number=String(),
        description=_('Search user by DNI or Foreign Card')
    )
    get_school_managers = List(
        UserType,
        description=_('Get school managers')
    )
    get_unit_managers_by_unit = List(
        UnitManagerType,
        unit_id=ID(),
        description=_('Get unit managers by unit')
    )
    get_unit_manager = Field(
        UnitManagerType,
        unit_manager_id=ID(),
        description=_('Get unit manager by id')
    )
    get_school_manager = Field(
        UserType,
        user_id=ID(),
        description=_('Get school manager by id')
    )

    @login_required
    def resolve_get_users(self, info, **kwargs):
        return User.objects.all()

    @login_required
    def resolve_get_unit_managers(self, info, **kwargs):
        return UnitManager.objects.all()

    @login_required
    def resolve_get_professor(self, info, **kwargs):
        professor_registration_id = kwargs.get('professor_registration_id')
        return get_object_by_id(
            ProfessorRegistration, professor_registration_id
        )

    @login_required
    def resolve_get_degrees(self, info, professor_id):
        return Degree.objects.filter(professor_id=professor_id)

    def resolve_get_google_oauth_link(self, info):
        flow = Flow.from_client_config(
            client_config=settings.CLIENT_CONFIG,
            scopes=settings.SCOPES,
            redirect_uri=settings.GOOGLE_REDIRECT_URI
        )
        authorization_url, state = flow.authorization_url(access_type='offline')

        return authorization_url

    @login_required
    def resolve_me(self, info):
        return info.context.user

    @login_required
    # TODO: Delete query and use search_user query instead
    def resolve_search_professor(self, info, id_type, id_number):
        try:
            return Professor.objects.get(
                user__useridentification__identification_type=id_type,
                user__useridentification__number=id_number,
            )

        except ObjectDoesNotExist:
            return None

    @login_required
    def resolve_get_student_by_cui(self, info, cui):
        try:

            return Student.objects.get(cui=cui)
        except Student.DoesNotExist:

            return None

    @login_required
    def resolve_search_user(self, info, id_type, id_number):
        try:
            return User.objects.get(
                useridentification__identification_type=id_type,
                useridentification__number=id_number,
            )

        except User.DoesNotExist:
            return None

    @login_required
    def resolve_get_school_managers(self, info):
        return User.objects.filter(groups__name=SCHOOL_MANAGER_GROUP_NAME)

    @login_required
    def resolve_get_unit_managers_by_unit(self, info, unit_id):
        return UnitManager.objects.filter(graduate_unit_id=unit_id)

    @login_required
    def resolve_get_unit_manager(self, info, unit_manager_id):
        try:
            return UnitManager.objects.get(pk=unit_manager_id)

        except UnitManager.DoesNotExist:
            return None

    @login_required
    def resolve_get_school_manager(self, info, user_id):
        try:
            school_manager = User.objects.filter(
                groups__name=SCHOOL_MANAGER_GROUP_NAME
            ).get(pk=user_id)

        except User.DoesNotExist:
            school_manager = None

        return school_manager
