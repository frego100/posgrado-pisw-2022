"""
Account mutations
"""
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import Flow
from graphene import (
    ObjectType,
    Field,
    Mutation,
    String,
    List,
    ID,
)
from graphene_django.types import ErrorType
from graphql_jwt.decorators import login_required
from graphql_jwt.mixins import JSONWebTokenMixin
from graphql_jwt.settings import jwt_settings

from django.conf import settings

from account.constants import SCHOOL_MANAGER_GROUP_NAME
from account.messages.error_messages import (
    COULD_NOT_ADD_DEGREES,
    COULD_NOT_CHANGE_DEGREES,
    COULD_NOT_DELETE_DEGREE,
    COULD_NOT_ADD_PROFESSOR,
    COULD_NOT_CHANGE_PROFESSOR,
    COULD_NOT_ADD_UNIT_MANAGER,
    COULD_NOT_CHANGE_UNIT_MANAGER,
    COULD_NOT_DELETE_UNIT_MANAGER,
    COULD_NOT_ADD_SCHOOL_MANAGER,
    COULD_NOT_CHANGE_SCHOOL_MANAGER,
    COULD_NOT_DELETE_SCHOOL_MANAGER,
)
from account.messages.success_messages import (
    DEGREES_ADDED,
    DEGREES_CHANGED,
    DEGREE_DELETED,
    PROFESSOR_ADDED,
    PROFESSOR_CHANGED,
    UNIT_MANAGER_ADDED,
    UNIT_MANAGER_CHANGED,
    UNIT_MANAGER_DELETED,
    SCHOOL_MANAGER_ADDED,
    SCHOOL_MANAGER_CHANGED,
    SCHOOL_MANAGER_DELETED,
)
from account.models import (
    Degree,
    Professor,
    User, UnitManager,
)
from account.schema.inputs import (
    AddProfessorInput,
    EditProfessorInput,
    AddDegreeInput,
    EditDegreeInput,
    AddGraduateUnitManagerInput,
    EditGraduateUnitManagerInput,
    AddSchoolManagerInput,
    EditSchoolManagerInput,
)
from account.schema.types import (
    ProfessorType,
    UserType,
    DegreeType,
)
from account.serializers import (
    ProfessorSerializer,
    DegreeSerializer,
    UnitManagerSerializer,
    SchoolManagerSerializer,
)
from core.schema.mutations import BaseMutation
from core.schema.types import FeedbackType, EnumStatus
from core.utils import get_object_by_id, pop_sub_dict


class AddSchoolManagerMutation(BaseMutation):
    """
    Mutation to add school manager
    """

    class Arguments:
        input = AddSchoolManagerInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        manager_serializer = SchoolManagerSerializer(data=inputs)

        if manager_serializer.is_valid():
            manager_serializer.save()
            return AddSchoolManagerMutation(
                feedback=FeedbackType(
                    message=SCHOOL_MANAGER_ADDED,
                    status=EnumStatus.SUCCESS
                ),
            )

        return AddSchoolManagerMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_SCHOOL_MANAGER,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(manager_serializer.errors)
        )


class EditSchoolManagerMutation(BaseMutation):
    """
    Mutation to edit school manager
    """

    class Arguments:
        input = EditSchoolManagerInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        user_id = inputs.pop('user_id')
        school_manager = get_object_by_id(User, user_id)

        manager_serializer = SchoolManagerSerializer(
            data=inputs, instance=school_manager, partial=True
        )

        if manager_serializer.is_valid():
            manager_serializer.save()
            return EditSchoolManagerMutation(
                feedback=FeedbackType(
                    message=SCHOOL_MANAGER_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
            )

        return EditSchoolManagerMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_SCHOOL_MANAGER,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(manager_serializer.errors)
        )


class DeleteSchoolManagerMutation(BaseMutation):
    """
    Mutation to delete school manager
    """

    class Arguments:
        user_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        user_id = kwargs.pop('user_id')
        try:
            school_manager = User.objects.filter(
                groups__name=SCHOOL_MANAGER_GROUP_NAME
            ).get(pk=user_id)
            school_manager.delete()

            return DeleteSchoolManagerMutation(
                feedback=FeedbackType(
                    message=SCHOOL_MANAGER_DELETED,
                    status=EnumStatus.SUCCESS
                ),
            )

        except User.DoesNotExist:
            return DeleteSchoolManagerMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_DELETE_SCHOOL_MANAGER,
                    status=EnumStatus.ERROR
                ),
            )


class DeleteUnitManagerMutation(BaseMutation):
    """
    Mutation to delete unit manager
    """

    class Arguments:
        unit_manager_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        unit_manager_id = kwargs.pop('unit_manager_id')
        unit_manager = get_object_by_id(UnitManager, unit_manager_id)

        if unit_manager:
            try:
                unit_manager.user.delete()
            except AttributeError:
                unit_manager.delete()

            return DeleteUnitManagerMutation(
                feedback=FeedbackType(
                    message=UNIT_MANAGER_DELETED,
                    status=EnumStatus.SUCCESS
                ),
            )

        return DeleteUnitManagerMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_UNIT_MANAGER,
                status=EnumStatus.ERROR
            ),
        )


class AddUnitManagerMutation(BaseMutation):
    """
    Mutation to add graduate unit manager
    """
    class Arguments:
        input = AddGraduateUnitManagerInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        serializer_data = pop_sub_dict(
            inputs,
            ['graduate_unit',]
        )
        manager_serializer = UnitManagerSerializer(
            data={
                'user': inputs,
                **serializer_data
            }
        )

        if manager_serializer.is_valid():
            manager_serializer.save()
            return AddUnitManagerMutation(
                feedback=FeedbackType(
                    message=UNIT_MANAGER_ADDED,
                    status=EnumStatus.SUCCESS
                ),
            )

        errors = manager_serializer.errors

        return AddUnitManagerMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_UNIT_MANAGER,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class EditUnitManagerMutation(BaseMutation):
    """
    Mutation to edit graduate unit manager
    """

    class Arguments:
        input = EditGraduateUnitManagerInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        serializer_data = pop_sub_dict(
            inputs,
            [
                'unit_manager_id',
                'graduate_unit',
            ]
        )

        unit_manager = get_object_by_id(
            UnitManager, serializer_data.get('unit_manager_id', None)
        )
        serializer_data = {'user': inputs, **serializer_data}

        manager_serializer = UnitManagerSerializer(
            instance=unit_manager, data=serializer_data, partial=True
        )

        if manager_serializer.is_valid():
            manager_serializer.save()
            return EditUnitManagerMutation(
                feedback=FeedbackType(
                    message=UNIT_MANAGER_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
            )
        errors = manager_serializer.errors

        if 'user' in errors:
            user_errors = errors.pop('user')
            errors.update(user_errors)

        return EditUnitManagerMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_UNIT_MANAGER,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class AddProfessorMutation(BaseMutation):
    """
    Add professor mutation
    """

    professor = Field(ProfessorType)

    class Arguments:
        input = AddProfessorInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        serializer_data = pop_sub_dict(
            inputs,
            [
                'curriculum',
                'operating_plan_program',
                'professor_type',
                'degree_set',
                'investigations',
                'sunedu_file',
                'orcid_id',
                'is_director',
                'is_coordinator',
            ]
        )
        professor_serializer = ProfessorSerializer(
            data={
                'user': inputs,
                **serializer_data
            }
        )
        professor_is_valid = professor_serializer.is_valid()

        if professor_is_valid:
            professor = professor_serializer.save()
            return AddProfessorMutation(
                feedback=FeedbackType(
                    message=PROFESSOR_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                professor=professor
            )

        errors = professor_serializer.errors

        return AddProfessorMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_PROFESSOR,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class EditProfessorMutation(BaseMutation):
    """
    Edit professor mutation
    """

    professor = Field(ProfessorType)

    class Arguments:
        input = EditProfessorInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        serializer_data = pop_sub_dict(
            inputs,
            [
                'curriculum',
                'professor_id',
                'professor_type',
                'investigations',
                'sunedu_file',
                'orcid_id',
                'operating_plan_program',
                'is_director',
                'is_coordinator',
            ]
        )

        professor = get_object_by_id(Professor, serializer_data.get('professor_id', None))
        serializer_data = {'user': inputs, **serializer_data}

        professor_serializer = ProfessorSerializer(
            instance=professor, data=serializer_data, partial=True
        )

        if professor_serializer.is_valid():
            professor = professor_serializer.save()

            return EditProfessorMutation(
                feedback=FeedbackType(
                    message=PROFESSOR_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                professor=professor
            )
        errors = professor_serializer.errors

        if 'user' in errors:
            user_errors = errors.pop('user')
            errors.update(user_errors)

        return EditProfessorMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_PROFESSOR,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class LoginMutation(JSONWebTokenMixin, Mutation):
    """
    Login mutation
    """

    user = Field(UserType)

    class Arguments:
        code = String(required=True)

    def mutate(self, info, **kwargs):
        code = kwargs.pop('code')
        context = info.context
        flow = Flow.from_client_config(
            client_config=settings.CLIENT_CONFIG,
            scopes=settings.SCOPES,
            redirect_uri=settings.GOOGLE_REDIRECT_URI,
        )
        flow.fetch_token(code=code)
        user_info_service = build('oauth2', 'v2', credentials=flow.credentials)
        user_info = user_info_service.userinfo().get().execute()
        user = User.objects.get(email=user_info['email'])
        payload = jwt_settings.JWT_PAYLOAD_HANDLER(user, context)
        orig_iat = payload.get("origIat")
        payload["origIat"] = orig_iat
        refresh_expires_in = (
            orig_iat + jwt_settings.JWT_REFRESH_EXPIRATION_DELTA.total_seconds()
        )
        token = jwt_settings.JWT_ENCODE_HANDLER(payload, context)

        return LoginMutation(
            payload=payload,
            token=token,
            refresh_expires_in=refresh_expires_in
        )


class AddDegreeMutation(BaseMutation):
    """
    Add degree mutation
    """

    degrees = List(DegreeType)

    class Arguments:
        input = List(AddDegreeInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, degree in enumerate(inputs):
            serializer = DegreeSerializer(data=degree)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update({f'{key}_{index}': serializer.errors[key] for key in serializer.errors})

        if errors:

            return AddDegreeMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_DEGREES,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        else:
            degrees = [serializer.save() for serializer in serializers]

            return AddDegreeMutation(
                feedback=FeedbackType(
                    message=DEGREES_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                degrees=degrees
            )


class EditDegreeMutation(BaseMutation):
    """
    Edit degree mutation
    """

    degrees = List(DegreeType)

    class Arguments:
        input = List(EditDegreeInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, degree in enumerate(inputs):
            degree_id = degree.pop('degree_id')
            degree_model = get_object_by_id(Degree, degree_id)
            serializer = DegreeSerializer(instance=degree_model, data=degree, partial=True)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update({f'{key}_{index}': serializer.errors[key] for key in serializer.errors})

        if errors:

            return EditDegreeMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_CHANGE_DEGREES,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        else:
            degrees = [serializer.save() for serializer in serializers]

            return EditDegreeMutation(
                feedback=FeedbackType(
                    message=DEGREES_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                degrees=degrees
            )


class DeleteDegreeMutation(BaseMutation):
    """
    Delete degree mutation
    """

    class Arguments:
        degree_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        degree_id = kwargs.pop('degree_id')
        degree = get_object_by_id(Degree, degree_id)

        if degree:
            degree.delete()

            return DeleteDegreeMutation(
                feedback=FeedbackType(
                    message=DEGREE_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteDegreeMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_DEGREE,
                status=EnumStatus.ERROR
            )
        )


class Mutation(ObjectType):
    add_professor = AddProfessorMutation.Field()
    edit_professor = EditProfessorMutation.Field()
    login = LoginMutation.Field()
    add_degrees = AddDegreeMutation.Field()
    edit_degrees = EditDegreeMutation.Field()
    delete_degree = DeleteDegreeMutation.Field()
    create_unit_manager = AddUnitManagerMutation.Field()
    edit_unit_manager = EditUnitManagerMutation.Field()
    delete_unit_manager = DeleteUnitManagerMutation.Field()
    add_school_manager = AddSchoolManagerMutation.Field()
    edit_school_manager = EditSchoolManagerMutation.Field()
    delete_school_manager = DeleteSchoolManagerMutation.Field()
