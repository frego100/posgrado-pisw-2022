"""
Account types
"""
import graphene
from graphene_django.types import DjangoObjectType
from graphene import Enum
from django_countries.graphql.types import Country

from account.models import (
    User,
    UnitManager,
    Professor,
    UserIdentification,
    Degree,
    Student,
)
from account.constants import DegreeTypes
from operating_plan.schema.types import ProfessorRegistrationType
from payments.schema.types import StudentPaymentType


class UserType(DjangoObjectType):
    """
    User type
    """

    country = graphene.Field(Country)

    class Meta:
        model = User


class UserIdentificationType(DjangoObjectType):
    """
    UserIdentification type
    """

    class Meta:
        model = UserIdentification


class UnitManagerType(DjangoObjectType):
    """
    Unit manager type
    """

    class Meta:
        model = UnitManager


class ProfessorType(DjangoObjectType):
    """
    Professor type
    """

    class Meta:
        model = Professor

    university_title = graphene.String()
    top_degree = graphene.String()
    curriculum_url = graphene.String()
    investigations_url = graphene.String()
    sunedu_file_url = graphene.String()
    professorregistration_set = graphene.List(
        ProfessorRegistrationType, op_plan_program_id=graphene.ID(required=False)
    )

    def resolve_university_title(self: Professor, info):
        professor_titles = self.degree_set.filter(
            degree_type=DegreeTypes.TITLE
        )
        if professor_titles.exists():
            return professor_titles.first().name

        return "No tiene titulo universitario"

    def resolve_top_degree(self: Professor, info):
        if self.degree_set.exists():
            return self.degree_set.all().order_by('-degree_type').first().name

        return "No posee algún grado"

    def resolve_curriculum_url(self: Professor, info):
        return self.curriculum.url if self.curriculum else None

    def resolve_investigations_url(self: Professor, info):
        return self.investigations.url if self.investigations else None

    def resolve_sunedu_file_url(self: Professor, info):
        return self.sunedu_file.url if self.sunedu_file else None

    def resolve_professorregistration_set(
            self: Professor, info, op_plan_program_id=None
    ):
        if op_plan_program_id:
            return self.professorregistration_set.filter(
                operating_plan_program_id=op_plan_program_id
            )
        return self.professorregistration_set.all()

class GenderEnum(Enum):
    """
    Gender enum
    """

    MALE = 1
    FEMALE = 2


class DegreeType(DjangoObjectType):
    """
    Degree type
    """

    class Meta:
        model = Degree

    certificate_url = graphene.String()
    sunedu_certificate_url = graphene.String()

    def resolve_certificate_url(self, info):
        if self.certificate:
            return self.certificate.url

        return None

    def resolve_sunedu_certificate_url(self, info):
        if self.sunedu_certificate:
            return self.sunedu_certificate.url

        return None


class StudentType(DjangoObjectType):
    """
    Student type
    """

    class Meta:
        model = Student

    last_enrollment_year = graphene.String()
    payments = graphene.List(
        StudentPaymentType, payment_schedule_id=graphene.ID(required=True)
    )

    def resolve_last_enrollment_year(self: Student, info):
        year = self.enrollment_set.all().values_list(
            'enrollment_period__process__year', flat=True
        ).order_by('-enrollment_period__process__year').first()

        return str(year) if year is not None else None

    def resolve_payments(self: Student, info, payment_schedule_id):
        return self.studentpayment_set.filter(
            fee_payment__payment_schedule_id=payment_schedule_id
        ).order_by('fee_payment__num_of_payment')
