"""
Account inputs
"""
from graphene import (
    InputObjectType,
    String,
    Date,
    ID,
    Enum,
    List,
    Int, Boolean,
)
from graphene_file_upload.scalars import Upload

from account.schema.types import GenderEnum


class UserIdentificationEnum(Enum):
    """
    UserIdentification enum
    """
    DNI = 1
    FOREIGN_CARD = 2


class ProfessorTypesEnum(Enum):
    """
    Professor types enum
    """
    INVITED = 1
    FROM_UNIVERSITY = 2


class DegreeTypeEnum(Enum):
    """
    Degree type enum
    """
    TITLE = 0
    BACHELOR = 1
    MASTER = 2
    DOCTORATE = 3


class AddUserIdentificationInput(InputObjectType):
    """
    User identification input
    """
    identification_type = UserIdentificationEnum()
    number = String()


class DegreeInput(InputObjectType):
    """
    Degree input
    """

    name = String(required=True)
    degree_type = DegreeTypeEnum(required=True)
    certificate = Upload(required=True)
    year = Int(required=True)
    country = ID(required=True)
    sunedu_certificate = Upload(required=True)
    institution = String(required=True)


class AddProfessorInput(InputObjectType):
    """
    Add professor input
    """

    first_name = String(required=True)
    last_name = String(required=True)
    maternal_last_name = String(required=False)
    birthday = Date(required=True)
    gender = GenderEnum(required=True)
    email = String(required=True)
    phone = String(required=True)
    professor_type = ProfessorTypesEnum(required=True)
    operating_plan_program = ID(required=True)
    country = ID(required=True)
    curriculum = Upload()
    user_identifications = List(AddUserIdentificationInput, required=True)
    degree_set = List(DegreeInput)
    profile_picture = Upload()
    investigations = Upload()
    sunedu_file = Upload()
    orcid_id = String()
    is_director = Boolean()
    is_coordinator = Boolean()


class EditProfessorInput(InputObjectType):
    """
    Edit professor input
    """
    first_name = String()
    last_name = String()
    maternal_last_name = String()
    birthday = Date()
    gender = GenderEnum()
    email = String()
    phone = String()
    professor_type = ProfessorTypesEnum()
    country = ID()
    curriculum = Upload()
    professor_id = ID(required=True)
    profile_picture = Upload()
    investigations = Upload()
    sunedu_file = Upload()
    orcid_id = String()
    operating_plan_program = ID(required=False)
    is_director = Boolean(required=False)
    is_coordinator = Boolean(required=False)


class AddDegreeInput(DegreeInput):
    """
    Add degree input
    """
    professor = ID(required=True)


class EditDegreeInput(InputObjectType):
    """
    Edit degree input
    """
    name = String()
    degree_type = DegreeTypeEnum()
    certificate = Upload()
    year = Int()
    country = ID()
    sunedu_certificate = Upload()
    institution = String()
    degree_id = ID()


class StudentInput(InputObjectType):
    """
    Student input
    """
    email = String(required=True)
    first_name = String(required=True)
    last_name = String(required=True)
    maternal_last_name = String(required=False)
    birthday = Date(required=False)
    gender = GenderEnum(required=True)
    phone = String(required=False)
    user_identifications = List(AddUserIdentificationInput, required=True)
    code = String(required=True)
    cui = String(required=True)


class AddGraduateUnitManagerInput(InputObjectType):
    """
    Add graduate unit manager input
    """
    email = String(required=True)
    first_name = String(required=True)
    last_name = String(required=True)
    maternal_last_name = String(required=False)
    birthday = Date(required=False)
    gender = GenderEnum(required=False)
    phone = String(required=False)
    user_identifications = List(AddUserIdentificationInput, required=False)
    graduate_unit = ID(required=True)


class EditGraduateUnitManagerInput(InputObjectType):
    """
    Edit graduate unit manager input
    """
    unit_manager_id = ID(required=True)
    email = String(required=True)
    first_name = String(required=True)
    last_name = String(required=True)
    maternal_last_name = String(required=False)
    birthday = Date(required=False)
    gender = GenderEnum(required=False)
    phone = String(required=False)
    user_identifications = List(AddUserIdentificationInput, required=False)
    graduate_unit = ID(required=True)


class AddSchoolManagerInput(InputObjectType):
    """
    Add school manager input
    """

    email = String(required=True)
    first_name = String(required=True)
    last_name = String(required=True)
    maternal_last_name = String()
    birthday = Date(required=False)
    gender = GenderEnum(required=False)
    phone = String(required=False)


class EditSchoolManagerInput(InputObjectType):
    """
    Edit school manager input
    """

    user_id = ID(required=True)
    email = String()
    first_name = String()
    last_name = String()
    maternal_last_name = String()
    birthday = Date(required=False)
    gender = GenderEnum(required=False)
    phone = String(required=False)
