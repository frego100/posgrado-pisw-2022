"""
Account urls
"""
from django.urls import path
from account.views import ProfessorsExcelFileView

urlpatterns = [
    path(
        'professors_file/',
        ProfessorsExcelFileView.as_view(),
        name='excel_view',
    ),
]