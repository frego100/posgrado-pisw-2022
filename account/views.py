import json

from django.views import View
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from account.utils import create_professors_file


@method_decorator(csrf_exempt, name='dispatch')
class ProfessorsExcelFileView(View):
    """
    Professors excel file view
    """

    def post(self, request, *args, **kwargs):
        try:
            body = json.loads(request.body.decode('utf-8'))
            unit_id = body['unit_id']
            plan_process_id = body['plan_process_id']

            workbook = create_professors_file(unit_id, plan_process_id)

            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            )
            response[
                'Content-Disposition'
            ] = 'attachment; filename=professors.xlsx'
            workbook.save(response)

            return response
        except (KeyError, json.JSONDecodeError) as error:

            return HttpResponse('Wrong request format')
