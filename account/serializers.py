"""
Account serializers
"""
from django.core.validators import EmailValidator
from rest_framework import serializers

from django.contrib.auth.models import Group

from account.constants import PROFESSOR_GROUP_NAME, STUDENT_GROUP_NAME, \
    UNIT_MANAGER_GROUP_NAME, SCHOOL_MANAGER_GROUP_NAME
from account.models import (
    User,
    Professor,
    UserIdentification,
    Degree, Student, UnitManager,
)
from account.utils import get_user_by_identification
from core.utils import update_model_instance
from operating_plan.models import OperatingPlanProgram, ProfessorRegistration


class UserIdentificationSerializer(serializers.ModelSerializer):
    """
    User identification serializer
    """

    class Meta:
        model = UserIdentification
        fields = ['identification_type', 'number']


class UserSerializer(serializers.ModelSerializer):
    """
    User serializer
    """

    user_identifications = UserIdentificationSerializer(many=True, source='useridentification_set')

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'maternal_last_name',
            'birthday',
            'gender',
            'email',
            'phone',
            'user_identifications',
            'country',
            'profile_picture',
        ]
        extra_kwargs = {
            'email': {
                'validators': [EmailValidator()],
            }
        }


class DegreeSerializer(serializers.ModelSerializer):
    """
    Degree serializer
    """

    class Meta:
        model = Degree
        fields = [
            'name',
            'degree_type',
            'certificate',
            'year',
            'country',
            'sunedu_certificate',
            'institution',
            'professor',
        ]
        extra_kwargs = {"professor": {"required": False, "allow_null": True}}

    def validate(self, attrs):
        super(DegreeSerializer, self).validate(attrs)

        if self.partial and self.instance is None:
            raise serializers.ValidationError('No existe grado con el id proporcionado')

        return attrs


class ProfessorSerializer(serializers.ModelSerializer):
    """
    Professor serializer
    """
    user = UserSerializer()
    degree_set = DegreeSerializer(many=True)
    operating_plan_program = serializers.IntegerField(required=False)
    is_director = serializers.BooleanField(required=False)
    is_coordinator = serializers.BooleanField(required=False)

    class Meta:
        model = Professor
        fields = [
            'user',
            'curriculum',
            'operating_plan_program',
            'professor_type',
            'degree_set',
            'investigations',
            'sunedu_file',
            'orcid_id',
            'is_director',
            'is_coordinator',
        ]

    def validate_operating_plan_program(self, operating_plan_program):
        if not OperatingPlanProgram.objects.filter(pk=operating_plan_program).exists():
            raise serializers.ValidationError('El plan de funcionamiento no existe')

        return operating_plan_program

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_identifications_data = user_data.pop('useridentification_set')
        degree_data = validated_data.pop('degree_set')
        operating_plan_program = validated_data.pop('operating_plan_program')
        is_director = validated_data.pop('is_director')
        is_coordinator = validated_data.pop('is_coordinator')

        user = get_user_by_identification(**user_identifications_data[0])
        if not user:
            user = User.objects.create(**user_data)
            for user_identification in user_identifications_data:
                UserIdentification.objects.create(
                    user=user, **user_identification
                )
        else:
            update_model_instance(user, user_data)

        professor_group = Group.objects.get(name=PROFESSOR_GROUP_NAME)
        user.groups.add(professor_group)
        user.save()

        professor = Professor.objects.create(user=user, **validated_data)
        ProfessorRegistration.objects.create(
            professor=professor,
            operating_plan_program_id=operating_plan_program,
            is_director=is_director,
            is_coordinator=is_coordinator,
        )

        for degree in degree_data:
            Degree.objects.create(**degree, professor=professor)

        return professor

    def update(self, instance, validated_data):
        user = instance.user
        # Update user data
        user_data = validated_data.pop('user')
        update_model_instance(user, user_data)
        user.save()
        # Update professor data
        update_model_instance(instance, validated_data)
        instance.save()
        # Remove orcid if from dict
        validated_data.pop('orcid_id')

        # Update professor registration data if is required
        professor_registration_keys = {
            'operating_plan_program', 'is_director', 'is_coordinator'
        }
        if professor_registration_keys <= validated_data.keys():
            operating_plan_program = validated_data.pop('operating_plan_program')
            professor_registration = ProfessorRegistration.objects.filter(
                operating_plan_program_id=operating_plan_program,
                professor=instance
            ).first()
            update_model_instance(professor_registration, validated_data)
            professor_registration.save()

        return instance


class StudentSerializer(serializers.ModelSerializer):
    """
    Student serializer
    """
    user = UserSerializer()
    class Meta:
        model = Student
        fields = [
            'user',
            'code',
            'cui',
        ]

    def create(self, validated_data):
        """
        Create method to create Student object
        """
        user_data = validated_data.pop('user')
        user_identifications_data = user_data.pop('useridentification_set')
        user = get_user_by_identification(**user_identifications_data[0])
        if not user:
            user = User.objects.create(**user_data)
            for user_identification in user_identifications_data:
                UserIdentification.objects.create(
                    user=user, **user_identification
                )
        else:
            update_model_instance(user, user_data)
            user.save()

        student_group = Group.objects.get(name=STUDENT_GROUP_NAME)
        user.groups.add(student_group)

        return Student.objects.create(user=user, **validated_data)

    def update(self, instance, validated_data):
        """
        Update method to update student object and its user object
        """
        user = instance.user
        # Update user data
        user_data = validated_data.pop('user')
        user_identifications = user_data.pop('useridentification_set')
        update_model_instance(user, user_data)
        user.save()
        for user_identification in user_identifications:
            update_model_instance(
                user.useridentification_set.first(), user_identification)

        # Update student data
        update_model_instance(instance, validated_data)
        instance.save()

        return instance


class UnitManagerSerializer(serializers.ModelSerializer):
    """
    Serializer for graduate unit manager
    """
    user = UserSerializer()

    class Meta:
        model = UnitManager
        fields = ['user', 'graduate_unit']

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_identifications_data = user_data.pop('useridentification_set')
        unit_manager_group = Group.objects.get(name=UNIT_MANAGER_GROUP_NAME)
        try:
            user = User.objects.get(email=user_data['email'])
        except User.DoesNotExist:
            user = User.objects.create(**user_data)

        user.groups.add(unit_manager_group)
        user.save()
        # TODO: Consider the registration of Id number and Id type with adequate validators
        # for user_identification in user_identifications_data:
        #     UserIdentification.objects.create(user=user, **user_identification)

        unit_manager = UnitManager.objects.create(user=user, **validated_data)

        return unit_manager

    def update(self, instance, validated_data):
        user = instance.user
        # Update user data
        user_data = validated_data.pop('user')
        user_identifications = user_data.pop('useridentification_set')
        update_model_instance(user, user_data)
        user.save()
        # TODO: Consider the edit of Id number and Id type with adequate validators
        # for user_identification in user_identifications:
        #     update_model_instance(
        #         user.useridentification_set.first(), user_identification)

        # Update unit manager
        update_model_instance(instance, validated_data)
        instance.save()

        return instance


class SchoolManagerSerializer(serializers.ModelSerializer):
    """
    School Manager serializer
    """

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'maternal_last_name',
            'birthday',
            'gender',
            'email',
            'phone',
        ]

    def create(self, validated_data):
        school_manager_group = Group.objects.get(name=SCHOOL_MANAGER_GROUP_NAME)

        user = User.objects.create(**validated_data)
        user.groups.add(school_manager_group)
        user.save()

        return user
