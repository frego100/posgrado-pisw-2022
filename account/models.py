"""
Account models
"""
from django_countries.fields import CountryField

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

from account.constants import (
    GenderTypes,
    DegreeTypes,
    UserIdentificationTypes,
    ProfessorTypes,
)
from account.managers import CustomUserManager
from core.constants import EMAIL_UNSA_DOMAIN
from organization.models import GraduateUnit


class User(AbstractUser):
    """
    User model
    """

    username = None
    email = models.EmailField(_('Email'), unique=True)
    personal_email = models.EmailField(_('Personal email'), blank=True)
    password = models.CharField(_('Password'), max_length=128, null=True, blank=True)
    gender = models.PositiveSmallIntegerField(
        choices=GenderTypes.choices, null=True, blank=True
    )
    birthday = models.DateField(null=True, blank=True)
    phone = models.CharField(max_length=16, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    profile_picture = models.FileField(null=True, blank=True, upload_to='profile_pictures/')
    maternal_last_name = models.CharField(max_length=150, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        if (
                EMAIL_UNSA_DOMAIN in self.personal_email
                and EMAIL_UNSA_DOMAIN not in self.email
        ):
            self.email = self.personal_email

        super(User, self).save(*args, **kwargs)

    def get_full_name(self):
        return f"{self.last_name} {self.maternal_last_name or ''}, {self.first_name}"

class UserIdentification(models.Model):
    """
    User Identification model
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    identification_type = models.PositiveSmallIntegerField(
        choices=UserIdentificationTypes.choices
    )
    number = models.CharField(max_length=20)

    def __str__(self):
        return self.number


class UnitManager(models.Model):
    """
    Unit Manager model
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    graduate_unit = models.ForeignKey(GraduateUnit, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.__str__()


class Professor(models.Model):
    """
    Professor model
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    professor_type = models.PositiveSmallIntegerField(
        choices=ProfessorTypes.choices, null=True, blank=True
    )
    curriculum = models.FileField(null=True, blank=True)
    investigations = models.FileField(null=True, blank=True)
    sunedu_file = models.FileField(null=True, blank=True)
    orcid_id = models.CharField(blank=True, max_length=300)

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def top_degree(self):
        return self.degree_set.all().order_by('-degree_type').first()


class Degree(models.Model):
    """
    Degree model
    """
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    degree_type = models.PositiveSmallIntegerField(
        choices=DegreeTypes.choices
    )
    certificate = models.FileField(null=True)
    year = models.IntegerField()
    country = CountryField()
    sunedu_certificate = models.FileField(null=True)
    institution = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Student(models.Model):
    """
    Student model
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=8, blank=True, unique=True, null=True)
    cui = models.CharField(max_length=8, blank=True, unique=True, null=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'
