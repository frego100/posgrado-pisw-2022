"""
Account utils
"""
import re

import openpyxl
import os

from django.db.models.functions import Concat
from django.db.models import Sum, F, When, Case, Value
from django.contrib.postgres.aggregates import StringAgg, ArrayAgg
from django.conf import settings

from account.constants import (
    PROFESSORS_FILE_START_INDEX,
    DEFAULT_DEGREE_DATA,
    DEGREE_TITLES,
)
from account.models import User, Professor
from organization.models import ProgramTypes



def create_professors_file(unit_id, plan_process_id):
    professors = Professor.objects.filter(
        professorregistration__operating_plan_program__operating_plan__graduate_unit_id=unit_id,
        professorregistration__operating_plan_program__operating_plan__op_plan_process_id=plan_process_id
    ).select_related('user').order_by('id').distinct().annotate(
        classes_types=ArrayAgg(
            'professorregistration__coursegroup__course__operating_plan_program__study_program__program_type',
            distinct=True
        ),
        class_hours=Sum(
            F('professorregistration__coursegroup__schedule__end_time') - F('professorregistration__coursegroup__schedule__start_time')
        ),
        study_programs=StringAgg(
            'professorregistration__coursegroup__course__operating_plan_program__study_program__code',
            ', ',
            distinct=True
        ),
        study_fields=StringAgg(
            'professorregistration__coursegroup__course__operating_plan_program__study_program__graduate_unit__study_field__code',
            ', ',
            distinct=True
        ),
        director_comment=StringAgg(
            Case(
                When(
                    professorregistration__is_director=True,
                    then=Concat(Value('Director '), F('professorregistration__operating_plan_program__study_program__code'))
                )
            ),
            ', ',
            distinct=True
        ),
        coordinator_comment=StringAgg(
            Case(
                When(
                    professorregistration__is_coordinator=True,
                    then=Concat(Value('Coordinador '), F('professorregistration__operating_plan_program__study_program__code'))
                )
            ),
            ', ',
            distinct=True
        )
    )

    professors_file = openpyxl.load_workbook(
        os.path.join(
            settings.BASE_DIR,
            'spreadsheets/Formato C9 - Relación de Docentes.xlsx'
        )
    )
    sheet = professors_file.active
    for index, professor in enumerate(professors, start=PROFESSORS_FILE_START_INDEX):
        user = professor.user
        first_name = user.first_name
        last_name = user.last_name
        maternal_last_name = user.maternal_last_name
        country = user.country.name
        identification = user.useridentification_set.all().first().number
        top_degree = professor.top_degree
        degree_data = DEFAULT_DEGREE_DATA
        total_class_hours = professor.class_hours.total_seconds() / 60 / 60 if professor.class_hours else 0

        master_classes = 'Si' if ProgramTypes.MASTER in professor.classes_types else 'No'
        doctor_classes = 'Si' if ProgramTypes.DOCTORATE in professor.classes_types else 'No'

        if top_degree:
            degree_data = [
                DEGREE_TITLES[top_degree.degree_type],
                top_degree.name,
                top_degree.institution,
                top_degree.country.name,
            ]

        professor_data = [
            last_name,
            maternal_last_name,
            first_name,
            country,
            identification,
            *degree_data,
            master_classes,
            doctor_classes,
            professor.study_fields,
            professor.study_programs,
            round(total_class_hours, 2),
        ]
        for data_index, value in enumerate(professor_data, start=3):
            sheet.cell(row=index, column=data_index).value = value

        comment = ', '.join(
            [professor.director_comment, professor.coordinator_comment]
        )
        sheet.cell(row=index, column=20).value = comment

    return professors_file


def get_user_by_identification(identification_type, number):
    """
    Get user by its identification number
    """
    try:
        return User.objects.get(
            useridentification__identification_type=identification_type,
            useridentification__number=number
        )

    except User.DoesNotExist:
        return None


def remove_after_comma_or_dot(input_string):
    # Split the input string by comma or dot
    substrings = re.split(r'[,.]', input_string, 1)
    return substrings[0] if len(substrings) > 1 else input_string