"""
Account success messages
"""

PROFESSOR_ADDED = 'El profesor fue agregado exitosamente'
PROFESSOR_CHANGED = 'El profesor fue editado exitosamente'
DEGREES_ADDED = 'Los grados fueron agregaods exitosamente'
DEGREES_CHANGED = 'Los grados fueron editados exitosamente'
DEGREE_DELETED = 'El grado fue eliminado exitosamente'
UNIT_MANAGER_ADDED = 'El coordinador de unidad fue agregado exitosamente'
UNIT_MANAGER_CHANGED = 'El coordinador de unidad fue editado exitosamente'
UNIT_MANAGER_DELETED = 'El coordinador de unidad fue eliminado exitosamente'
SCHOOL_MANAGER_ADDED = 'El coordinador de escuela fue agregado exitosamente'
SCHOOL_MANAGER_CHANGED = 'El coordinador de escuela fue editado exitosamente'
SCHOOL_MANAGER_DELETED = 'El coordinador de unidad fue eliminado exitosamente'
