"""
Account error messages
"""

COULD_NOT_ADD_PROFESSOR = 'No se pudo agregar al profesor'
COULD_NOT_CHANGE_PROFESSOR = 'No se pudo editar al profesor'
COULD_NOT_ADD_DEGREES = 'No se pudieron agregar los grados'
COULD_NOT_CHANGE_DEGREES = 'No se pudieron editar los grados'
COULD_NOT_DELETE_DEGREE = 'No se pudo eliminar el grado'
COULD_NOT_ADD_OBSERVATION = 'No se pudo agregar la observacion'
COULD_NOT_CHANGE_OBSERVATION = 'No se pudo editar la observacion'
COULD_NOT_ADD_UNIT_MANAGER = 'No se pudo agregar al coordinador de unidad'
COULD_NOT_CHANGE_UNIT_MANAGER = 'No se pudo editar el coordinador de unidad'
COULD_NOT_DELETE_UNIT_MANAGER = 'No se pudo eliminar al coordinador de unidad'
COULD_NOT_ADD_SCHOOL_MANAGER = 'No se pudo agregar al coordinador de escuela'
COULD_NOT_CHANGE_SCHOOL_MANAGER = 'No se pudo editar el coordinador de escuela'
COULD_NOT_DELETE_SCHOOL_MANAGER = 'No se pudo eliminar al coordinador de escuela'
