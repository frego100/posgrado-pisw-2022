import json

from account.tests.mutations import (
    add_professor,
    edit_professor,
    add_degrees,
    edit_degrees,
    delete_degrees,
)
from core.tests import ApiBaseTestCase
from account.models import (
    User,
    UserIdentification,
    Degree,
)
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
)


class TestAddProfessorMutation(ApiBaseTestCase):
    """
    Test cases for AddProfessor mutation
    """
    def setUp(self) -> None:
        super().setUp()
        # Operating plan
        # (Its signal creates operating plan program and observation)
        self.operating_plan = OperatingPlan.objects.create(
            graduate_unit=self.graduate_unit, year='2022', status=1
        )
        self.op_plan_program = OperatingPlanProgram.objects.first()
        self.user_2 = User.objects.create(
            email="julio@gmail.com", password="123",
            gender=1, first_name="Julio", last_name="Ramon",
            birthday="2001-05-15", phone="96677654",
            country="PE"
        )
        self.user_identification = UserIdentification.objects.create(
            user=self.user_2, identification_type=1, number='12345678'
        )

    # def test_mutation_to_add_professor_without_errors(self):
    #     """
    #     Test mutation to add a professor correctly
    #     """
    #     response = self.query(
    #         add_professor,
    #         variables={
    #             "input": {
    #                 "email": self.user_2.email,
    #                 "firstName": self.user_2.first_name,
    #                 "lastName": self.user_2.last_name,
    #                 "birthday": self.user_2.birthday,
    #                 "gender": "MALE",
    #                 "phone": self.user_2.phone,
    #                 "country": self.user_2.get_country_display(),
    #                 "operatingPlanProgram": self.op_plan_program.id,
    #                 "curriculum": "",
    #                 "professorType": "INVITED",
    #                 "orcidId": "tjeiostje",
    #                 "userIdentifications": [{
    #                     "identificationType": self.user_identification.get_identification_type_display(),
    #                     "number": self.user_identification.number
    #                 }],
    #                 "degreeSet": [],
    #                 "profilePicture": "",
    #                 "investigations": "",
    #                 "suneduFile": ""
    #             }
    #         }
    #     )
    #     result = json.loads(response.content)
    #     print(result)
    #     result = result['data']['addProfessor']

    #     professor = Professor.objects.filter(
    #         user=self.user_2
    #     )

    #     self.assertEqual(result['feedback']['status'], 'SUCCESS')
    #     self.assertIsNone(result['errors'])
    #     self.assertTrue(professor.exists())
    #     self.assertTrue(professor.count() == 1)

    def test_mutation_to_add_professor_with_invalid_data(self):
        """
        Test mutation to add professor with
        invalid data as parameters
        """
        response = self.query(
            add_professor,
            variables={
                "input": {
                    "email": "test@gmail.com",
                    "firstName": "Julio",
                    "lastName": "Lopez",
                    "birthday": "2001-05-15",
                    "gender": "MALE",
                    "phone": "96677654",
                    "country": 2,
                    "operatingPlanProgram": -1,
                    "curriculum": "",
                    "professorType": "INVITED",
                    "orcidId": "tjeiostje",
                    "userIdentifications": [{
                        "identificationType": "DNI",
                        "number": -1
                    }],
                    "degreeSet": [],
                    "profilePicture": "",
                    "investigations": "",
                    "suneduFile": ""
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addProfessor']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo agregar al profesor'
        )
        self.assertIsNotNone(result['errors'])


class TestEditProfessorMutation(ApiBaseTestCase):
    """
    Test cases for EditProfessor mutation
    """
    def setUp(self) -> None:
        super().setUp()

    def test_mutation_to_edit_professor_without_errors(self):
        """
        Test mutation to edit a professor correctly
        """
        response = self.query(
            edit_professor,
            variables={
                "input": {
                    "firstName": "Nur2",
                    "lastName": "Ali",
                    "birthday": "2001-05-15",
                    "gender": "MALE",
                    "phone": "96671288",
                    "orcidId": "12321test",
                    "professorId": self.professor.id
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editProfessor']
        self.professor.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertEqual(
            self.professor.user.first_name,
            result['professor']['user']['firstName']
        )
        self.assertEqual(
            self.professor.user.last_name,
            result['professor']['user']['lastName']
        )

    def test_mutation_to_edit_professor_with_invalid_data(self):
        """
        Test mutation to edit professor with
        invalid data as parameters
        """
        response = self.query(
            edit_professor,
            variables={
                "input": {
                    "firstName": "test",
                    "lastName": "1",
                    "birthday": "2001-05-15",
                    "gender": "MALE",
                    "phone": "9667112288",
                    "orcidId": "12321test",
                    "professorId": self.professor.id,
                    "email": "correo"
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editProfessor']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo editar al profesor'
        )
        self.assertIsNotNone(result['errors'])


class TestAddDegreeMutation(ApiBaseTestCase):
    """
    Test cases for AddDegree mutation
    """
    def setUp(self) -> None:
        super().setUp()

    # def test_mutation_to_add_degree_without_errors(self):
    #     """
    #     Test mutation to add a degree correctly
    #     """
    #     response = self.query(
    #         add_degrees,
    #         variables={
    #             "input": [
    #                 {
    #                     "professor": self.professor.id,
    #                     "name": "test",
    #                     "degreeType": "TITLE",
    #                     "certificate": None,
    #                     "year": 2020,
    #                     "country": 1,
    #                     "suneduCertificate": None,
    #                     "institution": "test"
    #                 }
    #             ]
    #         }
    #     )
    #     result = json.loads(response.content)
    #     print(result)
    #     result = result['data']['addDegrees']

    #     degree = Degree.objects.filter(
    #         professor=self.professor,
    #         name="test"
    #     )

    #     self.assertEqual(result['feedback']['status'], 'SUCCESS')
    #     self.assertIsNone(result['errors'])
    #     self.assertTrue(degree.exists())
    #     self.assertTrue(degree.count() == 1)

    def test_mutation_to_add_degree_with_invalid_data(self):
        """
        Test mutation to add degree with
        invalid data as parameters
        """
        response = self.query(
            add_degrees,
            variables={
                "input": [
                    {
                        "professor": -1,
                        "name": "test",
                        "degreeType": "TITLE",
                        "certificate": "",
                        "year": 2020,
                        "country": 2,
                        "suneduCertificate": "",
                        "institution": "test"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['addDegrees']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudieron agregar los grados'
        )
        self.assertIsNotNone(result['errors'])


class TestEditDegreeMutation(ApiBaseTestCase):
    """
    Test cases for EditDegree mutation
    """
    def setUp(self) -> None:
        super().setUp()

        self.degree = Degree.objects.create(
            professor=self.professor,
            name="Maeatria con mencion en Literatura",
            degree_type=2,
            country="PE",
            year=2000,
            certificate="cetificado.pdf",
        )

    def test_mutation_to_edit_degree_without_errors(self):
        """
        Test mutation to edit a degree correctly
        """
        response = self.query(
            edit_degrees,
            variables={
                "input": [
                    {
                        "degreeId": self.degree.id,
                        "name": "test",
                        "degreeType": "TITLE",
                        "year": 2020,
                        "country": "PE",
                        "institution": "test"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['editDegrees']
        self.degree.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertEqual(self.degree.name, "test")

    def test_mutation_to_edit_degree_with_invalid_data(self):
        """
        Test mutation to edit degree with invalid data as parameters
        """
        response = self.query(
            edit_degrees,
            variables={
                "input": [
                    {
                        "degreeId": -1,
                        "name": "test",
                        "degreeType": "TITLE",
                        "certificate": "",
                        "year": 2020,
                        "country": "PE",
                        "suneduCertificate": "",
                        "institution": "test"
                    }
                ]
            }
        )
        result = json.loads(response.content)
        result = result['data']['editDegrees']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudieron editar los grados'
        )
        self.assertIsNotNone(result['errors'])


class TestDeleteDegreeMutation(ApiBaseTestCase):
    """
    Test cases for DeleteDegree mutation
    """
    def setUp(self) -> None:
        super().setUp()

        self.degree = Degree.objects.create(
            professor=self.professor,
            name="Maeatria con mencion en Literatura",
            degree_type=2,
            country="PE",
            year=2000,
            certificate="cetificado.pdf",
        )

    def test_mutation_to_delete_degree_without_errors(self):
        """
        Test mutation to delete a degree correctly
        """
        response = self.query(
            delete_degrees,
            variables={
                "degreeId": self.degree.id,
            }
        )
        result = json.loads(response.content)
        result = result['data']['deleteDegree']

        degree = Degree.objects.filter(
            professor=self.professor,
            name="Maeatria con mencion en Literatura",
            degree_type=2,
            year=2000,
            certificate="cetificado.pdf",
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertFalse(degree.exists())
        self.assertTrue(degree.count() == 0)

    def test_mutation_to_delete_degree_with_invalid_data(self):
        """
        Test mutation to delete degree with invalid data as parameters
        """
        response = self.query(
            delete_degrees,
            variables={
               "degreeId": -1,
            }
        )
        result = json.loads(response.content)
        result = result['data']['deleteDegree']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo eliminar el grado'
        )
