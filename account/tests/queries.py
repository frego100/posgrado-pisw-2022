"""
Queries of account app
"""
get_users = '''
    query {
        getUsers {
            id
            firstName
            lastName
            birthday
            email
            gender
            phone
        }
    }
'''
get_unit_managers = '''
    query {
        getUnitManagers{
            id
            user{
                firstName
                lastName
                email
            }
            graduateUnit{
                name
            }
        }
    }
'''
get_professor = '''
    query($professorId: ID, $idType: Int, $idNumber: String){
        getProfessor(professorId: $professorId, idType: $idType, idNumber: $idNumber){
            user{
                firstName
                lastName
                birthday
                email
                gender
                phone
            }
            curriculum
            investigations
            universityTitle
            topDegree
            orcidId
        }
    }  
'''
get_degrees = '''
    query($professorId: ID){
        getDegrees(professorId: $professorId){
            id
            name
            year
            degreeType
            certificate
        }
    }  
'''
get_google_oauth_link = '''
    query{
        getGoogleOauthLink
    }  
'''
me = '''
    query{
        me{
            id
            firstName
            lastName
            email
            groups{
                name
            }
            unitmanager{
                id
                graduateUnit{
                    id
                    name
                }
            }
        }
    }  
'''
