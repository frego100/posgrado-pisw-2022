import json

from account.tests.queries import get_users, get_unit_managers, get_professor, \
    get_degrees, get_google_oauth_link
from core.tests import ApiBaseTestCase
from account.models import (
    UserIdentification,
    UnitManager,
    Degree,  
)


class TestAccountQueries(ApiBaseTestCase):
    """
    Test graphql queries of Account app
    """

    def setUp(self) -> None:
        super().setUp()

        self.user_identification = UserIdentification.objects.create(
            user=self.user, identification_type=1, number='12345678'
        )

    def test_get_users_query_without_errors(self):
        """
        Test query to get all users without errors
        """
        response = self.query(get_users)
        result = json.loads(response.content)
        result = result['data']['getUsers']

        self.assertEqual(len(result), 1)
        self.assertEqual(
            str(self.user.id),
            result[0]['id']
        )
        self.assertEqual(
            self.user.first_name,
            result[0]['firstName']
        )
        self.assertEqual(
            self.user.last_name,
            result[0]['lastName']
        )
        self.assertEqual(
            self.user.email,
            result[0]['email']
        )

    def test_get_unit_managers_query_without_errors(self):
        """
        Test query to get all unit managers without errors
        """
        self.unit_manager = UnitManager.objects.create(
            user=self.user,
            graduate_unit=self.graduate_unit
        )
        response = self.query(get_unit_managers)
        result = json.loads(response.content)
        result = result['data']['getUnitManagers']

        self.assertEqual(len(result), 1)
        self.assertEqual(
            self.unit_manager.graduate_unit.name,
            result[0]['graduateUnit']['name']
        )
        self.assertEqual(
            self.unit_manager.user.first_name,
            result[0]['user']['firstName']
        )
        self.assertEqual(
            self.unit_manager.user.last_name,
            result[0]['user']['lastName']
        )
        self.assertEqual(
            self.unit_manager.user.email,
            result[0]['user']['email']
        )

    def test_get_professor_query_without_errors(self):
        """
        Test query to get a professor without errors
        """
        self.professor.curriculum = "plan.pdf"
        self.professor.save()

        response = self.query(
            get_professor,
            variables={
                "professorId": self.professor.id,
                "idType": self.professor.professor_type,
                "idNumber": self.user_identification.number  
            }
        )
        result = json.loads(response.content)
        result = result['data']['getProfessor']

        self.assertEqual(
            self.professor.user.first_name,
            result['user']['firstName']
        )
        self.assertEqual(
            self.professor.user.last_name,
            result['user']['lastName']
        )
        self.assertEqual(
            self.professor.user.email,
            result['user']['email']
        )
        self.assertEqual(
            self.professor.curriculum,
            result['curriculum']
        )
    
    def test_get_professor_query_with_invalid_professor_id(self):
        """
        Test query to get a professor with invalid professor id
        """
        response = self.query(
            get_professor,
            variables={
                "professorId": -1,
                "idType": self.professor.professor_type,
                "idNumber": self.user_identification.number  
            }
        )
        result = json.loads(response.content)
        
        self.assertIsNone(
            result['data']['getProfessor']
        )
    
    def test_get_professor_query_with_invalid_type_id(self):
        """
        Test query to get a professor with invalid professor id
        """
        response = self.query(
            get_professor,
            variables={
                "idType": -1,
                "idNumber": self.user_identification.number  
            }
        )
        result = json.loads(response.content)
        
        self.assertIsNone(
            result['data']['getProfessor']
        )

    def test_get_professor_query_with_invalid_id_number(self):
        """
        Test query to get a professor with invalid id number
        """
        response = self.query(
            get_professor,
            variables={
                "idType": self.professor.professor_type,
                "idNumber": -1  
            }
        )
        result = json.loads(response.content)
        
        self.assertIsNone(
            result['data']['getProfessor']
        ) 
    
    def test_get_degrees_query_without_errors(self):
        """
        Test query to get degrees without errors
        """
        degree = Degree.objects.create(
            professor=self.professor,
            name="Maeatria con mencion en Literatura",
            degree_type=2,
            year=2000,
            certificate="cetificado.pdf",
        )

        response = self.query(
            get_degrees,
            variables={"professorId": self.professor.id}
        )
        result = json.loads(response.content)
        result = result['data']['getDegrees']

        self.assertEqual(len(result), 1)

        self.assertEqual(
            degree.name,
            result[0]['name']
        )
        self.assertEqual(
            degree.year,
            result[0]['year']
        )
        self.assertEqual(
            degree.certificate,
            result[0]['certificate']
        )

    def test_get_degrees_query_with_ivalid_professor_id(self):
        """
        Test query to get degrees with 
        ivalid professor id
        """
        
        response = self.query(
            get_degrees,
            variables={"professorId": -1}
        )
        result = json.loads(response.content)

        result = result['data']['getDegrees']
        self.assertEqual(len(result), 0)

    def test_get_google_oauth_link_query_without_errors(self):
        """
        Test query to get google oauth link without errors
        """
        response = self.query(
            get_google_oauth_link,
        )
        result = json.loads(response.content)

        self.assertIsNotNone(
            result['data']['getGoogleOauthLink']
        )

    # def test_get_me_query_without_errors(self):
    #     """
    #     Test query to get current user without errors
    #     """
    #     response = self.query(
    #         me,
    #     )
    #     result = json.loads(response.content)
    #     print(result)
    #     result = result['data']['me']
