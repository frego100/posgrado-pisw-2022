"""
Mutations of account app
"""
add_professor = '''
    mutation AddProfessor($input: AddProfessorInput!) {
        addProfessor(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
edit_professor = '''
    mutation EditProfessor($input: EditProfessorInput!) {
        editProfessor(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            professor{
                id
                user{
                    id
                    firstName
                    lastName
                }
            }
        }
    }   
'''
login = '''
    mutation($code: String!){
        login(code: $code){
            payload
            token
            refreshExpiresIn
        }
    } 
'''
add_degrees = '''
    mutation AddDegrees($input: [AddDegreeInput]!) {
        addDegrees(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            degrees{
                name
            }
        }
    }  
'''
edit_degrees = '''
    mutation EditDegrees($input: [EditDegreeInput]!) {
        editDegrees(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            degrees{
                name
            }
        }
    }  
'''
delete_degrees = '''
    mutation DeleteDegree($degreeId: ID!) {
        deleteDegree(degreeId: $degreeId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }
'''
