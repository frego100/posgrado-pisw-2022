from django.db import models


class GenderTypes(models.IntegerChoices):
    """
    Gender types choices
    """
    MALE = 1, 'Masculino'
    FEMALE = 2, 'Femenino'


class DegreeTypes(models.IntegerChoices):
    """
    Degree types choices
    """
    TITLE = 0, 'Titulo'
    BACHELOR = 1, 'Bachiller'
    MASTER = 2, 'Maestría'
    DOCTORATE = 3, 'Doctorado'


class UserIdentificationTypes(models.IntegerChoices):
    """
    UserIdentification types choices
    """
    DNI = 1, 'DNI'
    FOREIGN_CARD = 2, 'Carné de extranjería'


class ProfessorTypes(models.IntegerChoices):
    """
    Professor types choices
    """
    INVITED = 1, 'Invitado'
    FROM_UNIVERSITY = 2, 'De la Universidad'


SCHOOL_MANAGER_GROUP_NAME = 'School manager'
UNIT_MANAGER_GROUP_NAME = 'Unit manager'
PROFESSOR_GROUP_NAME = 'Professor'
STUDENT_GROUP_NAME = 'Student'

PROFESSORS_FILE_START_INDEX = 9
DEFAULT_DEGREE_DATA = ['Sin Grado Academico', '-', '-', '-']

DEGREE_TITLES = {
    DegreeTypes.DOCTORATE: 'Doctor',
    DegreeTypes.MASTER: 'Maestro',
    DegreeTypes.BACHELOR: 'Bachiller',
    DegreeTypes.TITLE: 'Titulo'
}
