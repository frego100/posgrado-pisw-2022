"""
Account admin
"""
from django.contrib import admin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from account.models import (
    User,
    UnitManager,
    Professor,
    Degree,
    UserIdentification,
    Student,
)


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    """
    User admin
    """

    ordering = ('email',)
    form = UserChangeForm

    fieldsets = (
        (
            None,
            {
                'fields': (
                    'email',
                    'password',
                )
            },
        ),
        (
            'User information',
            {
                'fields': (
                    'first_name',
                    'last_name',
                    'maternal_last_name',
                    'birthday',
                    'groups',
                    'gender',
                    'country',
                    'profile_picture',
                    'personal_email'
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': (
                    'email',
                    'password1',
                    'password2',
                    'first_name',
                    'last_name',
                ),
            },
        ),
    )
    list_display = ('email', 'first_name', 'last_name', 'maternal_last_name', )
    search_fields = ('email', 'last_name', 'first_name',)


@admin.register(UserIdentification)
class UserIdentificationAdmin(admin.ModelAdmin):
    """
    UserIdentification admin
    """

    list_display = ('__str__', 'identification_type', 'user')
    list_filter = ('identification_type', )
    search_fields = (
        'number', 'user__email', 'user__last_name', 'user__first_name',
    )


@admin.register(UnitManager)
class UnitManagerAdmin(admin.ModelAdmin):
    """
    Unit manager admin
    """

    list_display = ('__str__', 'graduate_unit')


@admin.register(Professor)
class ProfessorAdmin(admin.ModelAdmin):
    """
    Professor admin
    """

    list_display = ('__str__', 'professor_type', )
    search_fields = ('user__first_name', 'user__last_name', )


@admin.register(Degree)
class DegreeAdmin(admin.ModelAdmin):
    """
    Degree admin
    """

    list_display = ('__str__', 'professor', 'year', 'institution', )
    list_filter = ('year', )
    search_fields = ('name', 'institution', )



@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    """
    Student admin
    """

    list_display = ('__str__', 'cui', )
    search_fields = ('user__first_name', 'user__last_name', 'cui', )
