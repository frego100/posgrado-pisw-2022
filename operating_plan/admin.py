from django.contrib import admin

from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    ProfessorRegistration,
    Observation,
    ObservationCategory,
    OperatingPlanDocument,
    OperatingPlanProcess,
)


@admin.register(OperatingPlanProcess)
class OperatingPlanProcessAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'start_date', 'end_date', 'is_active')


@admin.register(OperatingPlan)
class OperatingPlanAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'status', 'sent_late')
    list_filter = ('status', 'op_plan_process', )
    search_fields = ('graduate_unit__name', )


@admin.register(OperatingPlanProgram)
class OperatingPlanProgramAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'operating_plan', )
    list_filter = (
        'operating_plan__op_plan_process__year',
        'operating_plan__graduate_unit',
    )


@admin.register(ProfessorRegistration)
class ProfessorRegistrationAdmin(admin.ModelAdmin):
    list_display = (
        '__str__', 'operating_plan_program', 'is_director', 'is_coordinator'
    )
    list_filter = (
        'operating_plan_program__operating_plan__op_plan_process__year',
        'operating_plan_program',
    )


@admin.register(Observation)
class ObservationAdmin(admin.ModelAdmin):

    list_display = ('__str__', 'category', 'comment', )
    list_filter = ('category__op_plan_process__year',)


@admin.register(ObservationCategory)
class ObservationCategoryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'index', 'op_plan_process',)
    list_filter = ('op_plan_process__year',)
    ordering = ('index',)


@admin.register(OperatingPlanDocument)
class OperatingPlanDocumentAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'operating_plan_program', 'date_uploaded', ]
