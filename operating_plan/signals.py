from django.dispatch import receiver
from django.db.models.signals import post_save

from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    Observation,
    ObservationCategory,
)


@receiver(post_save, sender=OperatingPlan)
def create_operating_plan_program(
        sender, instance: OperatingPlan, created, **kwargs
):
    """
    Create operating plan program for each study program
    :param sender: sender
    :param instance: operating plan instance
    :parm created: if instance was created
    :param kwargs: kwargs
    :return: None
    """

    if created:
        study_programs = instance.graduate_unit.studyprogram_set.all()

        for study_program in study_programs:
            OperatingPlanProgram.objects.create(
                operating_plan=instance, study_program=study_program
            )


@receiver(post_save, sender=OperatingPlanProgram)
def create_observations(
        sender, instance: OperatingPlanProgram, created, **kwargs
):
    """
    Create observations for the operating plan program
    :param sender: sender
    :param instance: operating plan program instance
    :parm created: if instance was created
    :param kwargs: kwargs
    :return: None
    """

    if created:
        observation_categories = ObservationCategory.objects.filter(
            op_plan_process=instance.operating_plan.op_plan_process
        )

        for category in observation_categories:
            Observation.objects.create(
                operating_plan_program=instance, category=category
            )
