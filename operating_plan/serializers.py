import pathlib

import openpyxl
from rest_framework import serializers

from operating_plan.messages.error_messages import (
    INVALID_OPERATING_PLAN_PROCESS_DATES,
    INVALID_POSTPONEMENT_DATE,
)
from operating_plan.models import (
    Observation,
    ProfessorRegistration,
    OperatingPlanProcess,
)


class ObservationSerializer(serializers.ModelSerializer):
    """
    Observation serializer
    """

    class Meta:
        model = Observation
        fields = [
            'operating_plan_program', 'category', 'comment', 'is_approved',
        ]


class ProfessorRegistrationSerializer(serializers.ModelSerializer):
    """
    ProfessorRegistration serializer
    """

    class Meta:
        model = ProfessorRegistration
        fields = [
            'operating_plan_program',
            'professor',
            'is_director',
            'is_coordinator',
        ]


class OperatingPlanProcessSerializer(serializers.ModelSerializer):
    """
    OperatingPlanProcess serializer
    """

    class Meta:
        model = OperatingPlanProcess
        fields = [
            'year', 'start_date', 'end_date',
            'process_letter', 'descriptive_index_file', 'op_index',
            'postponement_date'
        ]

    def validate_op_index(self, op_index):
        """
        Validate op_index file
        """
        extension = pathlib.Path(op_index.name).suffix

        if extension not in ['.xlsx']:
            raise serializers.ValidationError(
                "El archivo debe ser una hoja de cálculo"
            )

        workbook = openpyxl.load_workbook(op_index)
        sheet = workbook.active

        format_error = (
            "Verificar el formato del archivo. "
            "Si el error persiste contactar a soporte."
        )
        # Check headers
        for row in sheet.iter_rows(min_row=1, max_row=1, max_col=2):
            if row[0].value != 'Orden' or row[1].value != 'Sección':
                raise serializers.ValidationError(format_error)

        return op_index

    def validate(self, attrs):
        """
        Validation of start date and end date
        :param attrs: inputs
        :return: attrs
        """
        start_date = attrs['start_date']
        end_date = attrs['end_date']

        if end_date <= start_date:
            raise serializers.ValidationError(
                {'dates': INVALID_OPERATING_PLAN_PROCESS_DATES}
            )

        postponement_date = None
        if 'postponement_date' in attrs:
            postponement_date = attrs['postponement_date']

        if postponement_date:
            if postponement_date <= end_date:
                raise serializers.ValidationError(
                    {'postponement_date': INVALID_POSTPONEMENT_DATE}
                )

        return attrs
