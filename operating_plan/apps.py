from django.apps import AppConfig


class OperatingPlanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'operating_plan'

    def ready(self):
        import operating_plan.signals
