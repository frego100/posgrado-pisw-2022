"""
Queries of operating plan app
"""
get_operating_plans = '''
    query($year: Int){
        getOperatingPlans(year: $year){
            graduateUnit{
                id
                name
            }
            id
            status
            year	
        }
    }
'''

get_all_program_operating_plans = '''
    query($unit: ID, $year: Int){
        getAllProgramOperatingPlans(unit: $unit, year: $year){
            id
            studyProgram{
                id
                name
                programType
            }	
            planDocument
            planDocumentUrl	
        }
    }
'''

get_program_operating_plan = '''
    query($programOpPlanId: ID){
        getProgramOperatingPlan(programOpPlanId: $programOpPlanId){
            studyProgram{
                name
            }
            planDocument
            planDocumentUrl   
        }
    }
'''

get_all_operating_plan_professors = '''
    query($programOpPlanId: ID){
        getAllOperatingPlanProfessors(programOpPlanId: $programOpPlanId){
            id
            professor{
                id
                user{
                    firstName
                    lastName
                    useridentificationSet{
                        identificationType
                        number
                    }
                    gender
                    email
                }
            }
        }
    }
'''

get_all_operating_plan_observations = '''
    query($programOpPlanId: ID){
        getAllOperatingPlanObservations(programOpPlanId: $programOpPlanId){
            id
            category{
                id
            }
            comment
            isApproved		
        }
    }
'''

get_operating_plan_template = '''
    query{
        getOperatingPlanTemplate
    }
'''
