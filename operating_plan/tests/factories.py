import factory

from operating_plan.models import OperatingPlan, OperatingPlanProgram
from organization.tests.factories import GraduateUnitFactory, \
    StudyProgramFactory


class OperatingPlanFactory(factory.Factory):
    class Meta:
        model = OperatingPlan

    graduate_unit = factory.SubFactory(GraduateUnitFactory)
    year = 2022
    status = 1


class OperatingPlanProgramFactory(factory.Factory):
    class Meta:
        model = OperatingPlanProgram

    operating_plan = factory.SubFactory(OperatingPlanFactory)
    study_program = factory.SubFactory(StudyProgramFactory)
