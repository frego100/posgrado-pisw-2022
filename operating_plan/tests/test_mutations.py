import json
import os
from django.core.files.uploadedfile import SimpleUploadedFile
from core.tests import ApiBaseTestCase
from django.utils import timezone
from operating_plan.constants import OperatingPlanStatusChoices
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    ProfessorRegistration,
    Observation,
    ObservationCategory,
)
from operating_plan.tests.mutations import (
    create_professor_registration,
    delete_professor_registration,
    create_operating_plan,
    send_operating_plan,
    approve_operating_plan,
    observe_operating_plan,
    upload_plan_document,
    add_observation,
    edit_observation,
)


class BaseTestOperatingPlanMutations(ApiBaseTestCase):
    """
    Base test class for mutation in operating plan app
    """
    def setUp(self) -> None:
        super(BaseTestOperatingPlanMutations, self).setUp()
        self.observation_category = ObservationCategory.objects.create(
            name="ObservationCategory"
        )
        self.operating_plan = OperatingPlan.objects.create(
            graduate_unit=self.graduate_unit, year='2022', status=1
        )
        self.op_plan_program = OperatingPlanProgram.objects.first()


class TestCreateProfessorRegistrationMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for CreateProfessorRegistration mutation
    """
    def test_mutation_to_create_professor_registration_without_errors(self):
        """
        Test mutation to create professor registration correctly
        """
        response = self.query(
            create_professor_registration,
            variables={
                "input": {
                    "operatingPlanProgram": self.op_plan_program.id,
                    "professor": self.professor.id
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['createProfessorRegistration']

        professor_registration = ProfessorRegistration.objects.filter(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertTrue(professor_registration.exists())
        self.assertTrue(professor_registration.count() == 1)

    def test_mutation_to_create_professor_registration_twice(self):
        """
        Test mutation to create professor registration fails when the professor
        is already registered on the operating plan program
        """
        # Create a existing professor registration
        ProfessorRegistration.objects.create(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )

        response = self.query(
            create_professor_registration,
            variables={
                "input": {
                    "operatingPlanProgram": self.op_plan_program.id,
                    "professor": self.professor.id
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['createProfessorRegistration']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'El docente ya está registrado'
        )
        professor_registration = ProfessorRegistration.objects.filter(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )
        # Assert the registration is not duplicated
        self.assertTrue(professor_registration.count() == 1)

    def test_mutation_to_create_professor_with_invalid_data(self):
        """
        Test mutation to create professor registration with
        invalid data as parameters
        """
        response = self.query(
            create_professor_registration,
            variables={
                "input": {
                    "operatingPlanProgram": '-1',
                    "professor": '-1'
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['createProfessorRegistration']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'], 'No puedo agregarse')
        self.assertIsNotNone(result['errors'])


class TestDeleteProfessorRegistrationMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for DeleteProfessorRegistration mutation
    """

    def setUp(self) -> None:
        super().setUp()
        # Create a existing professor registration
        self.professor_registration = ProfessorRegistration.objects.create(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )

    def test_mutation_to_delete_professor_registration_without_errors(self):
        """
        Test mutation to delete professor registration correctly
        """
        response = self.query(
            delete_professor_registration,
            variables={
                "registrationId": self.professor_registration.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['deleteProfessorRegistration']

        # Search professor registration
        professor_registration = ProfessorRegistration.objects.filter(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertFalse(professor_registration.exists())
        self.assertTrue(professor_registration.count() == 0)

    def test_mutation_to_delete_professor__with_invalid_data(self):
        """
        Test mutation to delete professor registration with
        invalid data as parameters
        """
        response = self.query(
            delete_professor_registration,
            variables={"registrationId": '-1'}
        )
        result = json.loads(response.content)
        result = result['data']['deleteProfessorRegistration']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertIsNone(result['errors'])


class TestCreateOperatingPlanMutation(ApiBaseTestCase):
    """
    Test cases for CreateOperatingPlan mutation
    """
    def test_mutation_to_create_operating_plan_without_errors(self):
        """
        Test mutation to create operating plan correctly
        """
        response = self.query(
            create_operating_plan,
        )
        result = json.loads(response.content)
        result = result['data']['createOperatingPlan']

        operating_plan = OperatingPlan.objects.filter(
            graduate_unit=self.graduate_unit, 
            year= timezone.now().year
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertTrue(operating_plan.exists())
        self.assertTrue(operating_plan.count() == 1)

    def test_mutation_to_create_operating_plan_twice(self):
        """
        Test mutation to create operating plan when the operating plan
        is already registered
        """
        response = self.query(
            create_operating_plan,
        )
        result = json.loads(response.content)
        result = result['data']['createOperatingPlan']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'],
            'El plan de funcionamiento no pudo ser creado'
        )
        self.assertEqual(
            result['errors'][0]['messages'][0],
            'Existe un plan de funcionamiento para el año'
        )
        operating_plan = OperatingPlan.objects.filter(
            graduate_unit=self.graduate_unit,
            year=timezone.now().year
        )
        # Assert the registration is not duplicated
        self.assertTrue(operating_plan.count() == 1)


class TestSendOperatingPlanMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for SendOperatingPlan mutation
    """
    def test_mutation_to_send_operating_plan_without_errors(self):
        """
        Test mutation to send operating plan correctly
        """
        response = self.query(
            send_operating_plan,
            variables={
                "opPlanId": self.operating_plan.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['sendOperatingPlan']
        self.operating_plan.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertEqual(self.operating_plan.status, 2)

    def test_mutation_to_send_operating_plan_twice(self):
        """
        Test mutation to send operating plan fails when 
        it is already sent
        """
        self.operating_plan.status = OperatingPlanStatusChoices.RECEIVED
        self.operating_plan.send_date = timezone.now()
        self.operating_plan.save()

        response = self.query(
            send_operating_plan,
            variables={
                "opPlanId": self.operating_plan.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['sendOperatingPlan']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'],
            'El plan de funcionamiento ya ha sido enviado'
        )

    def test_mutation_to_send_operating_plan_with_invalid_data(self):
        """
        Test mutation to send operating plan with
        invalid data as parameters
        """
        response = self.query(
            send_operating_plan,
            variables={
                "opPlanId": -1
            }
        )
        result = json.loads(response.content)
        result = result['data']['sendOperatingPlan']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'El plan de funcionamiento no existe.')


class TestApproveOperatingPlanMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for ApproveOperatingPlan mutation
    """
    def test_mutation_to_approve_operating_plan_without_errors(self):
        """
        Test mutation to approve operating plan correctly
        """
        self.operating_plan.status = OperatingPlanStatusChoices.RECEIVED
        self.operating_plan.send_date = timezone.now()
        self.operating_plan.save()
        file_route = f"{os.getcwd()}/tests/files/Documento.pdf"
        rd_file = SimpleUploadedFile(
            'rd.pdf', content=open(file_route, 'rb').read(),
        )

        response = self.query(
            approve_operating_plan,
            variables={
                "opPlanId": self.operating_plan.id
            },
            files={"approvalRd": rd_file}
        )
        result = json.loads(response.content)
        result = result['data']['approveOperatingPlan']
        self.operating_plan.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertIsNotNone(self.operating_plan.approval_rd)
        self.assertEqual(self.operating_plan.status, 3)

    def test_mutation_to_approve_operating_plan_with_invalid_data(self):
        """
        Test mutation to approve operating plan with
        invalid data as parameters
        """
        response = self.query(
            approve_operating_plan,
            variables={
                "opPlanId": -1,
                "approvalRd": 'f'
            }
        )
        result = json.loads(response.content)
        result = result['data']['approveOperatingPlan']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'El plan de funcionamiento no existe.')


class TestObserveOperatingPlanMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for ObserveOperatingPlan mutation
    """
    def test_mutation_to_observe_operating_plan_without_errors(self):
        """
        Test mutation to observe operating plan correctly
        """
        response = self.query(
            observe_operating_plan,
            variables={
                "opPlanId": self.operating_plan.id
            }
        )
        result = json.loads(response.content)
        result = result['data']['observeOperatingPlan']
        self.operating_plan.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertEqual(
            result['feedback']['message'],
            'El plan de funcionamiento ha sido observado'
        )
        self.assertIsNone(result['errors'])
        self.assertEqual(self.operating_plan.status, 4)

    def test_mutation_to_observe_operating_plan_with_invalid_data(self):
        """
        Test mutation to observe operating plan with
        invalid data as parameters
        """
        response = self.query(
            observe_operating_plan,
            variables={
                "opPlanId": -1
            }
        )
        result = json.loads(response.content)
        result = result['data']['observeOperatingPlan']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'],
            'El plan de funcionamiento no existe.'
        )


class TestUploadPlanDocumentMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for UploadPlanDocument mutation
    """
    # def test_mutation_to_upload_plan_document_without_errors(self):
    #     """
    #     Test mutation to upload plan document correctly
    #     """
    #     file_route = f"{os.getcwd()}/tests/files/Documento.pdf"
    #     plan_file = SimpleUploadedFile(
    #         'file.pdf', content=open(file_route, 'rb').read(),
    #     )
    #     response = self.query(
    #         upload_plan_document,
    #         variables={
    #             "input": {
    #                 "opPlanProgramId": self.op_plan_program.id
    #             }
    #         },
    #         files={"input": {"opPlanFile": plan_file}}
    #     )
    #     result = json.loads(response.content)
    #     print(result)
    #     result = result['data']['uploadPlanDocument']

    #     self.assertEqual(result['feedback']['status'], 'SUCCESS')
    #     self.assertEqual(result['feedback']['message'], 'El documento fue subido correctamente')
    #     self.assertIsNone(result['errors'])
    #     self.assertIsNotNone(self.op_plan_program.plan_document)

    def test_mutation_to_upload_plan_document_with_invalid_data(self):
        """
        Test mutation to upload plan document with
        invalid data as parameters
        """
        response = self.query(
            upload_plan_document,
            variables={
                "input": {
                    "opPlanProgramId": -1,
                    "opPlanFile": 'doc.yyy'
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['uploadPlanDocument']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'],
            'El documento no pudo ser subido'
        )


class TestAddObservationMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for AddObservation mutation
    """

    def test_mutation_to_add_observation_without_errors(self):
        """
        Test mutation to add Observation correctly
        """
        response = self.query(
            add_observation,
            variables={
                "input": {
                    "operatingPlanProgram": self.op_plan_program.id,
                    "category": self.observation_category.id,
                    "comment": "Test Observation",
                    "isApproved": False
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addObservation']

        observation_registration = Observation.objects.filter(
            operating_plan_program=self.op_plan_program,
            category=self.observation_category.id,
            comment="Test Observation",
            is_approved=False
        )

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertIsNone(result['errors'])
        self.assertTrue(observation_registration.exists())
        self.assertTrue(observation_registration.count() == 1)

    def test_mutation_to_add_observation_with_invalid_data(self):
        """
        Test mutation to add observation with
        invalid data as parameters
        """
        response = self.query(
            add_observation,
            variables={
                "input": {
                    "operatingPlanProgram": -1,
                    "category": -1,
                    "comment": "",
                    "isApproved": ""
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['addObservation']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(result['feedback']['message'],
                         'No se pudo agregar la observacion')
        self.assertIsNotNone(result['errors'])


class TestEditObservationMutation(BaseTestOperatingPlanMutations):
    """
    Test cases for EditObservation mutation
    """

    def setUp(self) -> None:
        super().setUp()
        self.observation = Observation.objects.first()

    def test_mutation_to_edit_observation_without_errors(self):
        """
        Test mutation to add Observation correctly
        """
        response = self.query(
            edit_observation,
            variables={
                "input": {
                    "observationId": self.observation.id,
                    "operatingPlanProgram": self.op_plan_program.id,
                    "category": self.observation_category.id,
                    "comment": "Edit Observation Test",
                    "isApproved": False
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editObservation']
        self.observation.refresh_from_db()

        self.assertEqual(result['feedback']['status'], 'SUCCESS')
        self.assertEqual(result['feedback']['message'],
                         'La observacion fue editada correctamente')
        self.assertIsNone(result['errors'])
        self.assertEqual(self.observation.comment, 'Edit Observation Test')

    def test_mutation_to_edit_observation_with_invalid_data(self):
        """
        Test mutation to edit observation with
        invalid data as parameters
        """
        response = self.query(
            edit_observation,
            variables={
                "input": {
                    "observationId": -1,
                    "operatingPlanProgram": -1,
                    "category": self.observation_category.id,
                    "comment": "Edit Observation Test",
                    "isApproved": False
                }
            }
        )
        result = json.loads(response.content)
        result = result['data']['editObservation']

        self.assertEqual(result['feedback']['status'], 'ERROR')
        self.assertEqual(
            result['feedback']['message'], 'No se pudo editar la observacion')
        self.assertIsNotNone(result['errors'])
