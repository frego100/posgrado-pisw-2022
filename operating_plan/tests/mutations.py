"""
Mutations of operating plan app
"""
create_professor_registration = '''
    mutation CreateProfessorRegistration($input: CreateProfessorRegistrationInput!) {
        createProfessorRegistration(input: $input) {
            feedback {
                message
                status
            }
            errors {
                field
                messages
            }
            professorRegistration {
                id,
                operatingPlanProgram{
                    id
                }
                professor{
                    id
                }
            }
        }
    }
'''
delete_professor_registration = '''
    mutation DeleteProfessorRegistration($registrationId: ID!) {
        deleteProfessorRegistration(registrationId: $registrationId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
create_operating_plan = '''
    mutation CreateOperatingPlan{
        createOperatingPlan{
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }
'''
send_operating_plan = '''
    mutation SendOperatingPlan($opPlanId: ID!){
        sendOperatingPlan(opPlanId: $opPlanId){
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }
'''
approve_operating_plan = '''
    mutation ApproveOperatingPlan($opPlanId: ID!, $approvalRd: Upload!) {
        approveOperatingPlan(opPlanId: $opPlanId, approvalRd: $approvalRd) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    } 
'''
observe_operating_plan = '''
    mutation ObserveOperatingPlan($opPlanId: ID!) {
        observeOperatingPlan(opPlanId: $opPlanId) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
upload_plan_document = '''
    mutation UploadOperatingPlanDocumentMutation($input: UploadOperatingPlanDocumentInput!) {
        uploadPlanDocument(input: $input){
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
        }
    }  
'''
add_observation = '''
    mutation AddObservation($input: AddObservationInput!) {
        addObservation(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            observation{
                id
                category{
                    id
                }
                comment
                isApproved
            }
        }
    }  
'''
edit_observation = '''
    mutation EditObservation($input: EditObservationInput!) {
        editObservation(input: $input) {
            feedback{
                message
                status
            }
            errors{
                field
                messages
            }
            observation{
                id
                category{
                    id
                }
                comment
                isApproved
            }
        }
    }   
'''
