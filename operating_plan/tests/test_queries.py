import json

from core.tests import ApiBaseTestCase
from account.models import User, Professor, UserIdentification
from operating_plan.tests.queries import *
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    ProfessorRegistration,
    ObservationCategory,
    Observation, OperatingPlanProcess,
)


class TestOperatingPlansQueries(ApiBaseTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.year = 2022
        ObservationCategory.objects.create(name="ObservationCategory")
        # Operating plan
        # (Its signal creates operating plan program and observation)
        self.process = OperatingPlanProcess.objects.create(
            year=self.year, start_date="2022-01-01", end_date="2022-01-01"
        )
        self.operating_plan = OperatingPlan.objects.create(
            graduate_unit=self.graduate_unit,
            op_plan_process=self.process,
            status=1
        )
        self.op_plan_program = OperatingPlanProgram.objects.first()
        # Create professor registration
        ProfessorRegistration.objects.create(
            operating_plan_program=self.op_plan_program,
            professor=self.professor
        )

    def test_get_operating_plans_without_errors(self):
        """
        Test query to get operating plans without errors
        """
        response = self.query(
            get_operating_plans,
            variables={
                "process": self.process.id
            }
        )
        result = json.loads(response.content)['data']['getOperatingPlans']

        self.assertEqual(len(result), 1)
        self.assertEqual(
            self.graduate_unit.name, result[0]['graduateUnit']['name']
        )
        self.assertEqual(
            self.operating_plan.get_status_display(), result[0]['status']
        )

    def test_get_operating_plans_with_invalid_process(self):
        """
        Test query to get operating plans with invalid year
        """
        response = self.query(
            get_operating_plans,
            variables={
                "process": '-1'
            }
        )
        result = json.loads(response.content)

        self.assertEqual(len(result['data']['getOperatingPlans']), 0)

    def test_get_all_operating_plan_programs_without_errors(self):
        """
        Test query to get all operating plan programs without errors
        """
        OperatingPlanProgram.objects.create(
            operating_plan=self.operating_plan,
            study_program=self.study_program,
            plan_document="test"
        )

        response = self.query(
            get_all_program_operating_plans,
            variables={
                "unit": self.graduate_unit.id,
                "process": self.process.id
            }
        )

        result = json.loads(response.content)['data']

        self.assertEqual(len(result['getAllProgramOperatingPlans']), 2)
        self.assertEqual(
            self.study_program.name,
            result['getAllProgramOperatingPlans'][0]['studyProgram']['name']
        )

    def test_get_all_operating_plan_programs_with_invalid_graduate_unit(self):
        """
        Test query to get all operating plan programs
        with invalid graduate unit
        """
        response = self.query(
            get_all_program_operating_plans,
            variables={
                "unit": '555',
                "process": self.process.id
            }
        )

        result = json.loads(response.content)
        self.assertEqual(len(result['data']['getAllProgramOperatingPlans']), 0)

    def test_get_all_operating_plan_programs_with_invalid_year(self):
        """
        Test query to get all operating plan programs with invalid process
        """
        response = self.query(
            get_all_program_operating_plans,
            variables={
                "unit": self.graduate_unit.id,
                "process": '-1'
            }
        )

        result = json.loads(response.content)
        self.assertEqual(len(result['data']['getAllProgramOperatingPlans']), 0)

    def test_get_operating_plan_program_without_errors(self):
        """
        Test query to get an operating plan program without errors
        """
        response = self.query(
            get_program_operating_plan,
            variables={
                "programOpPlanId": self.op_plan_program.id
            }
        )
        result = json.loads(response.content)

        self.assertEqual(
            self.study_program.name,
            result['data']['getProgramOperatingPlan']['studyProgram']['name']
        )

        self.assertEqual(
            self.op_plan_program.plan_document,
            result['data']['getProgramOperatingPlan']['planDocument']
        )

    def test_get_op_plan_program_with_invalid_op_plan_program_id(self):
        """
        Test query to get an operating plan program with invalid
        operating plan program id
        """
        response = self.query(
            get_program_operating_plan,
            variables={
                "programOpPlanId": '777'
            }
        )

        result = json.loads(response.content)
        self.assertIsNone(
            result['data']['getProgramOperatingPlan']
        )

    def test_get_all_operating_plan_professors_without_errors(self):
        """
        Test query to get all operating plan professors without errors
        """
        self.user2 = User.objects.create(
            email="juan@gmail.com", password="password", gender=1
        )
        self.professor2 = Professor.objects.create(
            user=self.user2,
            professor_type=1,
            curriculum=""
        )
        self.user_identification = UserIdentification.objects.create(
            user=self.user, identification_type=1, number='12345678'
        )
        ProfessorRegistration.objects.create(
            operating_plan_program=self.op_plan_program,
            professor=self.professor2
        )

        response = self.query(
            get_all_operating_plan_professors,
            variables={
                "programOpPlanId": self.op_plan_program.id
            }
        )

        result = json.loads(response.content)['data']
        result = result['getAllOperatingPlanProfessors']

        self.assertEqual(len(result), 2)
        professor = result[0]['professor']['user']
        self.assertEqual(self.user.email, professor['email'])
        self.assertEqual(
            self.user_identification.number,
            professor['useridentificationSet'][0]['number']
        )

    def test_get_all_op_plan_professors_with_invalid_op_plan_program_id(self):
        """
        Test query to get all operating plan professors with invalid
        operating plan program id
        """
        response = self.query(
            get_all_operating_plan_professors,
            variables={
                "programOpPlanId": '-1'
            }
        )

        result = json.loads(response.content)
        self.assertEqual(
            len(result['data']['getAllOperatingPlanProfessors']), 0
        )

    def test_get_all_operating_plan_observations_without_errors(self):
        """
        Test query to get all operating plan observations without errors
        """
        observation = Observation.objects.filter(
            operating_plan_program=self.op_plan_program
        ).first()
        observation.comment = "XX"
        observation.is_approved = True
        observation.save()

        response = self.query(
            get_all_operating_plan_observations,
            variables={
                "programOpPlanId": self.op_plan_program.id
            }
        )

        result = json.loads(response.content)
        self.assertEqual(
            len(result['data']['getAllOperatingPlanObservations']), 1)
        self.assertEqual(
            observation.comment,
            result['data']['getAllOperatingPlanObservations'][0]['comment']
        )
        self.assertEqual(
            observation.is_approved,
            result['data']['getAllOperatingPlanObservations'][0]['isApproved']
        )

    def test_get_all_op_plan_observations_with_invalid_op_plan_program(self):
        """
        Test query to get all operating plan observations with invalid
        operating plan program id
        """
        response = self.query(
            get_all_operating_plan_observations,
            variables={
                "programOpPlanId": '20001'
            }
        )

        result = json.loads(response.content)
        self.assertEqual(
            len(result['data']['getAllOperatingPlanObservations']), 0)

    # def test_get_operating_plan_template_without_errors(self):
    #     """
    #     Test query to get an operating plan template without errors
    #     """
    #     template = OperatingPlanDocumentTemplate.objects.create(
    #         document_template="test"
    #     )
    #
    #     response = self.query(
    #         get_operating_plan_template
    #     )
    #     result = json.loads(response.content)
    #
    #     self.assertEqual(
    #         template.document_template.url,
    #         result['data']['getOperatingPlanTemplate']
    #     )

    def test_get_operating_plan_template_when_there_is_no_template(self):
        """
        Test query to get an operating plan template without errors
        """
        response = self.query(
            get_operating_plan_template
        )

        result = json.loads(response.content)
        self.assertIsNone(
            result['data']['getOperatingPlanTemplate']
        )

    def tearDown(self) -> None:
        pass
