import datetime

from django.db import models
from django.utils import timezone

from account.models import Professor
from operating_plan.constants import OperatingPlanStatusChoices
from organization.models import GraduateUnit, StudyProgram


class OperatingPlanProcess(models.Model):
    """
    Operating plan process
    """
    year = models.PositiveSmallIntegerField(unique=True)
    start_date = models.DateField()
    end_date = models.DateField()
    process_letter = models.FileField(
        null=True, blank=True, upload_to='operating_plan/process_letters/'
    )
    descriptive_index_file = models.FileField(
        null=True, blank=True, upload_to='operating_plan/descriptive_index/'
    )
    op_index = models.FileField(
        null=True, blank=True, upload_to='operating_plan/index/'
    )
    postponement_date = models.DateField(null=True, blank=True)

    @property
    def is_active(self):
        current_date = timezone.now().date()
        if current_date < self.start_date:
            return False

        if self.postponement_date:
            return self.postponement_date >= current_date

        return self.end_date >= current_date

    def __str__(self):
        return str(self.year)


class OperatingPlan(models.Model):
    """
    Operating plan model
    """
    graduate_unit = models.ForeignKey(GraduateUnit, on_delete=models.CASCADE)
    op_plan_process = models.ForeignKey(
        OperatingPlanProcess, on_delete=models.CASCADE, null=True
    )
    send_date = models.DateTimeField(null=True, blank=True)
    approval_date = models.DateTimeField(null=True, blank=True)
    status = models.PositiveSmallIntegerField(
        choices=OperatingPlanStatusChoices.choices,
        default=OperatingPlanStatusChoices.IN_PROGRESS
    )
    approval_rd = models.FileField(null=True, blank=True)

    @property
    def sent_late(self):
        if not self.send_date:
            return None

        return self.send_date.date() > self.op_plan_process.end_date

    def __str__(self):
        return f"{self.graduate_unit.name} - {self.op_plan_process.year}"

    class Meta:
        unique_together = ['graduate_unit', 'op_plan_process', ]


class OperatingPlanProgram(models.Model):
    """
    Operating plan program model
    """
    operating_plan = models.ForeignKey(OperatingPlan, on_delete=models.CASCADE)
    study_program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    plan_document = models.FileField(null=True, blank=True)

    def __str__(self):
        return f'{self.study_program.name} - {self.operating_plan.op_plan_process.year}'


class ProfessorRegistration(models.Model):
    """
    ProfessorRegistration model
    """
    operating_plan_program = models.ForeignKey(
        OperatingPlanProgram, on_delete=models.CASCADE
    )
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    is_director = models.BooleanField(default=False)
    is_coordinator = models.BooleanField(default=False)
    is_in_operating_plan = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.professor.__str__()} - {self.operating_plan_program.__str__()}'

    def save(self, *args, **kwargs):
        if not self.pk:
            is_approved = self.operating_plan_program.operating_plan.status == OperatingPlanStatusChoices.APPROVED
            self.is_in_operating_plan = not is_approved

        super(ProfessorRegistration, self).save(*args, **kwargs)


class ObservationCategory(models.Model):
    """
    Observation category
    """
    op_plan_process = models.ForeignKey(
        OperatingPlanProcess, on_delete=models.CASCADE, null=True
    )
    name = models.CharField(max_length=300)
    index = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name


class Observation(models.Model):
    """
    Observation model
    """
    operating_plan_program = models.ForeignKey(
        OperatingPlanProgram, on_delete=models.CASCADE)
    category = models.ForeignKey(ObservationCategory, on_delete=models.CASCADE)
    comment = models.TextField(blank=True)
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.operating_plan_program.__str__()


class OperatingPlanDocument(models.Model):
    """
    OperatingPlanDocument model
    """
    operating_plan_program = models.ForeignKey(
        OperatingPlanProgram, on_delete=models.CASCADE
    )
    plan_document = models.FileField()
    date_uploaded = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"PlanFuncionamiento_{self.date_uploaded.strftime('%d_%m_%y-%H:%M')}"
