"""
File with util functions for operating plan app
"""
import openpyxl

from django.contrib.auth.models import Group
from django_countries import countries

from account.models import UserIdentification, Professor, User
from account.constants import PROFESSOR_GROUP_NAME
from enrollment.models import EnrollmentPeriod
from operating_plan.constants import (
    PROFESSORS_LIST_FILE_START_INDEX,
    OperatingPlanStatusChoices,
    PROFESSORS_LIST_MAX_COL,
)
from operating_plan.models import (
    ObservationCategory,
    ProfessorRegistration,
    OperatingPlan,
)
from organization.constants import ProgramTypes
from organization.models import StudyProgram


def create_observation_categories_from_index_file(index_file, op_process):
    """
    Create the ObservationCategory objects from index file uploaded in
    operating plan process
    """
    workbook = openpyxl.load_workbook(index_file)
    sheet = workbook.active

    for row in sheet.iter_rows(min_row=2, max_col=2):
        index, category = row
        if index.value and category.value:
            ObservationCategory.objects.create(
                op_plan_process_id=op_process,
                name=str(category.value),
                index=index.value,
            )
        else:
            break


def create_professor_registrations(op_plan_program, professors_list):
    """
    Create professor objects and corresponding professor registrations for the
    given operating plan program
    """
    id_types = {
        "DNI": 1,
        "Carné de extranjería": 2
    }
    professor_types = {
        "Invitado": 1,
        "De la Universidad": 2
    }
    other_charges = {
        "Si": True,
        "No": False
    }
    genders = {
        "Masculino": 1,
        "Femenino": 2
    }
    workbook = openpyxl.load_workbook(professors_list)
    sheet = workbook.active
    identifications = []
    new_users = []
    existing_users = []
    new_professors = []
    existing_professors = []
    professor_registrations = []
    existing_registrations = []

    for row in sheet.iter_rows(
            min_row=PROFESSORS_LIST_FILE_START_INDEX,
            max_col=PROFESSORS_LIST_MAX_COL
    ):
        id_type, id_number, name, last_name, maternal_last_name, birthday, gender, country, email, phone, professor_type, orcid, is_director, is_coordinator = row
        if None in [id_type.value, id_number.value, name.value, last_name.value,
                    birthday.value, gender.value,
                    country.value, email.value, phone.value,
                    professor_type.value, orcid.value, is_director.value,
                    is_coordinator.value]:
            break

        last_name = last_name.value.title()
        maternal_last_name = maternal_last_name.value.title()

        try:
            user = User.objects.get(
                useridentification__number=str(int(id_number.value)),
                useridentification__identification_type=id_types[
                    id_type.value]
            )
            user.first_name = name.value.title()
            user.last_name = last_name
            user.maternal_last_name = maternal_last_name
            user.personal_email = email.value
            user.gender = genders[gender.value]
            user.birthday = birthday.value
            user.phone = str(int(phone.value))

            if country.value in dict(countries):
                user.country = country.value
            else:
                user.country = None

            existing_users.append(user)
            try:
                professor = user.professor
                professor.professor_type = professor_types[professor_type.value]
                professor.orcid_id = orcid.value
                existing_professors.append(professor)
                existing_professor_registration = ProfessorRegistration.objects.filter(
                    operating_plan_program=op_plan_program,
                    professor=professor
                )
                if existing_professor_registration.exists():
                    professor_registration = existing_professor_registration.first()
                    professor_registration.is_director = other_charges[
                                                             is_director.value]
                    professor_registration.is_coordinator = other_charges[
                        is_coordinator.value]
                    existing_registrations.append(professor_registration)

                else:
                    professor_registration = ProfessorRegistration(
                        professor=professor,
                        operating_plan_program=op_plan_program,
                        is_director=other_charges[is_director.value],
                        is_coordinator=other_charges[is_coordinator.value]
                    )
                    professor_registrations.append(professor_registration)

            except AttributeError:
                professor = Professor(
                    user=user,
                    professor_type=professor_types[professor_type.value],
                    orcid_id=orcid.value
                )
                new_professors.append(professor)
                professor_registrations.append(
                    ProfessorRegistration(
                        professor=professor,
                        operating_plan_program=op_plan_program,
                        is_director=other_charges[is_director.value],
                        is_coordinator=other_charges[is_coordinator.value]
                    )
                )

        except User.DoesNotExist:
            user = User(
                first_name=name.value.title(),
                last_name=last_name,
                maternal_last_name=maternal_last_name,
                birthday=birthday.value,
                personal_email=email.value,
                email=email.value,
                phone=str(int(phone.value)),
                gender=genders[gender.value],
            )
            if country.value in dict(countries):
                user.country = country.value
            else:
                user.country = None

            new_users.append(user)
            identification = UserIdentification(
                user=user,
                identification_type=id_types[id_type.value],
                number=str(int(id_number.value))
            )
            identifications.append(identification)
            professor = Professor(
                user=user,
                professor_type=professor_types[professor_type.value],
                orcid_id=orcid.value
            )
            new_professors.append(professor)
            professor_registration = ProfessorRegistration(
                professor=professor,
                operating_plan_program=op_plan_program,
                is_director=other_charges[is_director.value],
                is_coordinator=other_charges[is_coordinator.value]
            )
            professor_registrations.append(professor_registration)

    User.objects.bulk_update(
        existing_users,
        fields=[
            'first_name',
            'last_name',
            'maternal_last_name',
            'personal_email',
            'gender',
            'phone',
            'birthday',
            'country'
        ]
    )
    Professor.objects.bulk_update(
        existing_professors, fields=['professor_type', 'orcid_id']
    )
    ProfessorRegistration.objects.bulk_update(
        existing_registrations, fields=['is_director', 'is_coordinator']
    )
    User.objects.bulk_create(new_users)
    UserIdentification.objects.bulk_create(identifications)
    Professor.objects.bulk_create(new_professors)
    ProfessorRegistration.objects.bulk_create(professor_registrations)
    professor_group = Group.objects.get(name=PROFESSOR_GROUP_NAME)
    for existing_user in existing_users:
        professor_group.user_set.add(existing_user)
    for new_user in new_users:
        professor_group.user_set.add(new_user)


def create_enrollment_periods(operating_plan: OperatingPlan):
    """
    Create enrollment periods for each program
    """
    programs = StudyProgram.objects.filter(
        operatingplanprogram__operating_plan=operating_plan,
        operatingplanprogram__operating_plan__status=OperatingPlanStatusChoices.APPROVED
    )
    enrollment_periods = []
    year = operating_plan.op_plan_process.year
    process = operating_plan.op_plan_process
    # Enrollment periods are created for each study program
    for program in programs:
        # Masters: 4 periods
        enrollment_periods.extend(
            (
                EnrollmentPeriod(process=process, year=year, period=1, program=program),
                EnrollmentPeriod(process=process, year=year, period=2, program=program),
                EnrollmentPeriod(process=process, year=year - 1, period=3, program=program),
                EnrollmentPeriod(process=process, year=year - 1, period=4, program=program)
            )
        )
        # Doctorate: 6 periods
        if program.program_type == ProgramTypes.DOCTORATE:
            enrollment_periods.extend(
                (
                    EnrollmentPeriod(
                        process=process, year=year - 2, period=5, program=program),
                    EnrollmentPeriod(
                        process=process, year=year - 2, period=6, program=program),
                )
            )

    EnrollmentPeriod.objects.bulk_create(enrollment_periods)
