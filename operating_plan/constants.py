from django.db import models


class OperatingPlanStatusChoices(models.IntegerChoices):
    """
    Operating Plan choices
    """
    IN_PROGRESS = 1, 'En curso'
    RECEIVED = 2, 'Recibido'
    APPROVED = 3, 'Aprobado'
    OBSERVED = 4, 'Observado'


PROFESSORS_LIST_FILE_START_INDEX = 2
PROFESSORS_LIST_MAX_COL = 14
