"""
Operating plan success messages
"""

REGISTRATION_DELETED = 'Registro de profesor eliminado'
OBSERVATION_ADDED = 'La observacion fue agregada correctamente'
OBSERVATION_CHANGED = 'La observacion fue editada correctamente'

UPLOAD_PLAN_DOCUMENT_SAVED = 'El documento fue subido correctamente'
OPERATING_PLAN_PROCESS_CREATED = 'El proceso de plan de funcionamiento fue creado correctamente'
OPERATING_PLAN_PROCESS_UPDATED = 'El proceso de plan de funcionamiento fue actualizado correctamente'
OPERATING_PLAN_SENT = 'El plan de funcionamiendo fue enviado correctamente.'

PROFESSOR_REGISTERED = "El docente fue agregado al plan de funcionamiento"
PROFESSORS_LIST_UPLOADED = "La lista de docentes fue subida correctamente"
