"""
Operating plan error messages
"""

COULD_NOT_DELETE_REGISTRATION = 'No se pudo eliminar el registro de profesor'
UPLOAD_PLAN_DOCUMENT_FAILED = 'El documento no pudo ser subido'
OPERATING_PLAN_YEAR_ERROR = 'Existe un plan de funcionamiento para el año'

INVALID_OPERATING_PLAN_PROCESS_DATES = 'La fecha de fin del proceso no puede ser mayor a la fecha de inicio'
INVALID_POSTPONEMENT_DATE = 'La fecha de aplazamiento debe ser mayor a la fecha final'
COULD_NOT_CREATE_OPERATING_PLAN_PROCESS = 'El proceso de registro de plan de funcionamiento no pudo ser creado'
COULD_NOT_UPDATE_OPERATING_PLAN_PROCESS = 'El proceso de registro de plan de funcionamiento no pudo ser actualizado'

PROFESSOR_ALREADY_REGISTERED = 'El docente ya se encuentra registrado en el plan de funcionamiento'
COULD_NOT_REGISTER_PROFESSOR = "El docente no pudo ser agregado al plan de funcionamiento"

COULD_NOT_REGISTER_PROFESSORS_LIST = "La lista de docentes no pudo ser cargada"
INVALID_PROFESSORS_LIST = "Hubo un error al procesar el documento de la lista de docentes"

OPERATING_PLAN_ALREADY_SENT = 'El plan de funcionamiento ya ha sido enviado'
OPERATING_PLAN_PROCESS_IS_OVER = 'El plazo para enviar el plan de funcionamiento ha vencido'

COULD_NOT_PROCESS_OP_INDEX_FILE = 'Hubo un error al procesar el índice del plan de funcionamiento.'
