"""
Operating plan mutations
"""
from django.db.models import Q
from graphene import (
    ObjectType,
    ID,
    Field,
)
from graphene_django.types import ErrorType

from django.utils import timezone
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required

from account.messages.error_messages import (
    COULD_NOT_ADD_OBSERVATION,
    COULD_NOT_CHANGE_OBSERVATION
)
from core.schema.mutations import BaseMutation
from core.schema.types import FeedbackType, EnumStatus
from core.utils import get_object_by_id
from operating_plan.constants import OperatingPlanStatusChoices
from operating_plan.messages.success_messages import (
    REGISTRATION_DELETED,
    OBSERVATION_CHANGED,
    OBSERVATION_ADDED,
    OPERATING_PLAN_PROCESS_CREATED,
    UPLOAD_PLAN_DOCUMENT_SAVED, PROFESSOR_REGISTERED, PROFESSORS_LIST_UPLOADED,
    OPERATING_PLAN_SENT, OPERATING_PLAN_PROCESS_UPDATED,
)
from operating_plan.messages.error_messages import (
    COULD_NOT_DELETE_REGISTRATION,
    UPLOAD_PLAN_DOCUMENT_FAILED,
    COULD_NOT_CREATE_OPERATING_PLAN_PROCESS,
    PROFESSOR_ALREADY_REGISTERED,
    COULD_NOT_REGISTER_PROFESSOR,
    COULD_NOT_REGISTER_PROFESSORS_LIST,
    INVALID_PROFESSORS_LIST,
    OPERATING_PLAN_PROCESS_IS_OVER,
    OPERATING_PLAN_ALREADY_SENT, COULD_NOT_UPDATE_OPERATING_PLAN_PROCESS,
    COULD_NOT_PROCESS_OP_INDEX_FILE,
)
from operating_plan.models import (
    ProfessorRegistration,
    OperatingPlan, Observation, OperatingPlanProgram, OperatingPlanDocument,
    OperatingPlanProcess
)
from operating_plan.schema.inputs import (
    AddObservationInput,
    EditObservationInput,
    CreateProfessorRegistrationInput,
    UploadOperatingPlanDocumentInput, CreateOpPlanProcessInput,
    UploadProfessorsListInput, EditOpPlanProcessInput,
)
from operating_plan.schema.types import (
    ObservationType,
    ProfessorRegistrationType, OperatingPlanProcessType,
)
from operating_plan.serializers import (
    ObservationSerializer,
    ProfessorRegistrationSerializer, OperatingPlanProcessSerializer,
)
from operating_plan.utils import (
    create_observation_categories_from_index_file,
    create_professor_registrations, create_enrollment_periods,
)
from organization.models import GraduateUnit


class DeleteProfessorRegistrationMutation(BaseMutation):
    """
    Delete professor registration mutation
    """

    class Arguments:
        registration_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        registration_id = kwargs.pop('registration_id')
        registration = get_object_by_id(ProfessorRegistration, registration_id)
        is_assigned_to_groups = registration.coursegroup_set.exists()

        if registration and not is_assigned_to_groups:
            if registration.operating_plan_program.operating_plan.status == OperatingPlanStatusChoices.APPROVED:
                registration.is_deleted = True
                registration.save()

            else:
                registration.delete()

            return DeleteProfessorRegistrationMutation(
                feedback=FeedbackType(
                    message=REGISTRATION_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteProfessorRegistrationMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_REGISTRATION,
                status=EnumStatus.ERROR
            )
        )


class CreateProfessorRegistrationMutation(BaseMutation):
    """
    Create professor registration mutation
    """

    professor_registration = Field(ProfessorRegistrationType)

    class Arguments:
        input = CreateProfessorRegistrationInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        professor_id = inputs.pop('professor')
        op_plan_program_id = inputs.pop('operating_plan_program')
        is_director = inputs.pop('is_director')
        is_coordinator = inputs.pop('is_coordinator')

        professor_registration_serializer = ProfessorRegistrationSerializer(
            data={
                'operating_plan_program': op_plan_program_id,
                'professor': professor_id,
                'is_director': is_director,
                'is_coordinator': is_coordinator
            }
        )

        if professor_registration_serializer.is_valid():
            is_registered = ProfessorRegistration.objects.filter(
                operating_plan_program=op_plan_program_id,
                professor=professor_id
            ).exists()

            if is_registered:
                return CreateProfessorRegistrationMutation(
                    feedback=FeedbackType(
                        message=PROFESSOR_ALREADY_REGISTERED,
                        status=EnumStatus.ERROR
                    ),
                )

            professor_registration = professor_registration_serializer.save()

            return CreateProfessorRegistrationMutation(
                feedback=FeedbackType(
                    message=PROFESSOR_REGISTERED,
                    status=EnumStatus.SUCCESS
                ),
                professor_registration=professor_registration
            )

        return CreateProfessorRegistrationMutation(
            feedback=FeedbackType(
                message=COULD_NOT_REGISTER_PROFESSOR,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(
                professor_registration_serializer.errors
            )
        )


class SendOperatingPlanMutation(BaseMutation):
    """
    Send operating plan mutation
    """

    class Arguments:
        op_plan_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        operating_plan_id = kwargs.pop('op_plan_id')
        operating_plan = get_object_by_id(OperatingPlan, operating_plan_id)

        if not operating_plan:
            return SendOperatingPlanMutation(
                feedback=FeedbackType(
                    message="El plan de funcionamiento no existe.",
                    status=EnumStatus.ERROR
                ),
            )

        if operating_plan.status == OperatingPlanStatusChoices.RECEIVED:
            return SendOperatingPlanMutation(
                feedback=FeedbackType(
                    message=OPERATING_PLAN_ALREADY_SENT,
                    status=EnumStatus.ERROR
                ),
            )

        if not operating_plan.op_plan_process.is_active:
            return SendOperatingPlanMutation(
                feedback=FeedbackType(
                    message=OPERATING_PLAN_PROCESS_IS_OVER,
                    status=EnumStatus.ERROR
                ),
            )

        if not operating_plan.send_date:
            operating_plan.send_date = timezone.now()

        operating_plan.status = OperatingPlanStatusChoices.RECEIVED
        operating_plan.save()
        # Create OperatingPlanDocument objects
        op_program_plans = operating_plan.operatingplanprogram_set.filter(
            ~Q(plan_document='')
        )
        for op_program_plan in op_program_plans:
            OperatingPlanDocument.objects.create(
                operating_plan_program=op_program_plan,
                plan_document=op_program_plan.plan_document
            )
        
        return SendOperatingPlanMutation(
            feedback=FeedbackType(
                message=OPERATING_PLAN_SENT,
                status=EnumStatus.SUCCESS
            ),
        )


class ApproveOperatingPlanMutation(BaseMutation):
    """
    Approve Operating Plan Mutation
    """

    class Arguments:
        op_plan_id = ID(required=True)
        approval_rd = Upload(required=True, description="RD de aprobación")

    @login_required
    def mutate(self, info, **kwargs):
        status_id = kwargs.pop('op_plan_id')
        approval_rd = kwargs.pop('approval_rd')
        operating_plan = get_object_by_id(OperatingPlan, status_id)

        if operating_plan and approval_rd:
            operating_plan.status = OperatingPlanStatusChoices.APPROVED
            operating_plan.approval_date = timezone.now()
            operating_plan.approval_rd = approval_rd
            operating_plan.save()

            create_enrollment_periods(operating_plan)

            return ApproveOperatingPlanMutation(
                feedback=FeedbackType(
                    message="El plan de funcionamiento fue aprobado",
                    status=EnumStatus.SUCCESS
                ),
            )
        return ApproveOperatingPlanMutation(
            feedback=FeedbackType(
                message="El plan de funcionamiento no existe.",
                status=EnumStatus.ERROR
            )
        )


class AddObservationMutation(BaseMutation):
    """
    Add observation mutation
    """

    observation = Field(ObservationType)

    class Arguments:
        input = AddObservationInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        category = inputs.pop('category', None)
        operating_plan_program = inputs.pop('operating_plan_program')
        comment = inputs.pop('comment')
        is_approved = inputs.pop('is_approved')

        observation_serializer = ObservationSerializer(
            data={
                'operating_plan_program': operating_plan_program,
                'category': category,
                'comment': comment,
                'is_approved': is_approved
            }
        )
        observation_is_valid = observation_serializer.is_valid()

        if observation_is_valid:
            observation = observation_serializer.save()
            return AddObservationMutation(
                feedback=FeedbackType(
                    message=OBSERVATION_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                observation=observation
            )

        errors = observation_serializer.errors

        return AddObservationMutation(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_OBSERVATION,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class EditObservationMutation(BaseMutation):
    """
    Edit professor mutation
    """

    observation = Field(ObservationType)

    class Arguments:
        input = EditObservationInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        observation_id = inputs.pop('observation_id', None)
        observation = get_object_by_id(Observation, observation_id)
        serializer = ObservationSerializer(instance=observation, data=inputs,
                                           partial=True)

        if serializer.is_valid():
            observation = serializer.save()
            return EditObservationMutation(
                feedback=FeedbackType(
                    message=OBSERVATION_CHANGED,
                    status=EnumStatus.SUCCESS
                ),
                observation=observation
            )

        errors = serializer.errors

        return EditObservationMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CHANGE_OBSERVATION,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(errors)
        )


class ObserveOperatingPlanMutation(BaseMutation):
    """
    Observe OperatingPlan Mutation
    """

    class Arguments:
        op_plan_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        status_id = kwargs.pop('op_plan_id')
        operating_plan = get_object_by_id(OperatingPlan, status_id)

        if operating_plan:
            operating_plan.status = OperatingPlanStatusChoices.OBSERVED
            operating_plan.save()

            return ApproveOperatingPlanMutation(
                feedback=FeedbackType(
                    message="El plan de funcionamiento ha sido observado",
                    status=EnumStatus.SUCCESS
                ),
            )
        return ApproveOperatingPlanMutation(
            feedback=FeedbackType(
                message="El plan de funcionamiento no existe.",
                status=EnumStatus.ERROR
            )
        )


class UploadOperatingPlanDocumentMutation(BaseMutation):
    """
    Mutation to upload operating plan document
    """
    class Arguments:
        input = UploadOperatingPlanDocumentInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')

        op_plan_program_id = inputs.pop('op_plan_program_id')
        op_plan_file = inputs.pop('op_plan_file')

        op_plan_program = get_object_by_id(
            OperatingPlanProgram, op_plan_program_id
        )
        if op_plan_program and op_plan_file:
            op_plan_program.plan_document = op_plan_file
            op_plan_program.save()

            return UploadOperatingPlanDocumentMutation(
                feedback=FeedbackType(
                    message=UPLOAD_PLAN_DOCUMENT_SAVED,
                    status=EnumStatus.SUCCESS
                ),
            )

        return UploadOperatingPlanDocumentMutation(
            feedback=FeedbackType(
                message=UPLOAD_PLAN_DOCUMENT_FAILED,
                status=EnumStatus.ERROR
            ),
        )


class CreateOperatingPlanProcessMutation(BaseMutation):
    """
    Create operating plan process
    """
    op_plan_process = Field(OperatingPlanProcessType)

    class Arguments:
        input = CreateOpPlanProcessInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        data = kwargs.get('input')

        serializer = OperatingPlanProcessSerializer(data=data)

        if serializer.is_valid():
            op_plan_process = serializer.save()
            try:
                # Create observation categories according to index uploaded
                create_observation_categories_from_index_file(
                    index_file=data['op_index'], op_process=op_plan_process.id
                )
            # TODO: Catch specific exceptions
            except Exception as e:
                op_plan_process.delete()
                return CreateOperatingPlanProcessMutation(
                    feedback=FeedbackType(
                        message=COULD_NOT_CREATE_OPERATING_PLAN_PROCESS,
                        status=EnumStatus.ERROR
                    ),
                    errors=ErrorType.from_errors(
                        {"op_index": [COULD_NOT_PROCESS_OP_INDEX_FILE, [str(e)]]}
                    )
                )

            # When save operating plan process, operating plans
            # should be created for each graduate unit
            graduate_units = GraduateUnit.objects.all()
            for unit in graduate_units:
                OperatingPlan.objects.create(
                    op_plan_process=op_plan_process,
                    graduate_unit=unit
                )

            return CreateOperatingPlanProcessMutation(
                feedback=FeedbackType(
                    message=OPERATING_PLAN_PROCESS_CREATED,
                    status=EnumStatus.SUCCESS
                ),
                op_plan_process=op_plan_process
            )

        return CreateOperatingPlanProcessMutation(
            feedback=FeedbackType(
                message=COULD_NOT_CREATE_OPERATING_PLAN_PROCESS,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class EditOperatingPlanProcessMutation(BaseMutation):
    """
    Edit operating plan process
    """
    op_plan_process = Field(OperatingPlanProcessType)

    class Arguments:
        input = EditOpPlanProcessInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        data = kwargs.get('input')
        process_id = data.pop('process_id')
        process = get_object_by_id(OperatingPlanProcess, process_id)
        serializer = OperatingPlanProcessSerializer(
            instance=process, data=data, partial=True
        )

        if serializer.is_valid():
            op_plan_process = serializer.save()

            return EditOperatingPlanProcessMutation(
                feedback=FeedbackType(
                    message=OPERATING_PLAN_PROCESS_UPDATED,
                    status=EnumStatus.SUCCESS
                ),
                op_plan_process=op_plan_process
            )

        return CreateOperatingPlanProcessMutation(
            feedback=FeedbackType(
                message=COULD_NOT_UPDATE_OPERATING_PLAN_PROCESS,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class UploadProfessorsList(BaseMutation):
    """
    Create Professor registrations by the upload of professors list file
    """
    class Arguments:
        input = UploadProfessorsListInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')

        operating_plan_program = inputs.pop('operating_plan_program')
        professors_list = inputs.pop('professors_list')

        op_plan_program = get_object_by_id(
            OperatingPlanProgram, operating_plan_program
        )
        try:
            create_professor_registrations(op_plan_program, professors_list)

        except Exception as e:
            return UploadProfessorsList(
                feedback=FeedbackType(
                    message=COULD_NOT_REGISTER_PROFESSORS_LIST,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(
                    {"file": [INVALID_PROFESSORS_LIST, [str(e)]]}
                )
            )

        return UploadProfessorsList(
            feedback=FeedbackType(
                message=PROFESSORS_LIST_UPLOADED,
                status=EnumStatus.SUCCESS
            ),
        )


class Mutation(ObjectType):
    approve_operating_plan = ApproveOperatingPlanMutation.Field()
    send_operating_plan = SendOperatingPlanMutation.Field()
    delete_professor_registration = DeleteProfessorRegistrationMutation.Field()
    create_professor_registration = CreateProfessorRegistrationMutation.Field()
    add_observation = AddObservationMutation.Field()
    edit_observation = EditObservationMutation.Field()
    observe_operating_plan = ObserveOperatingPlanMutation.Field()
    upload_plan_document = UploadOperatingPlanDocumentMutation.Field()
    create_operating_plan_process = CreateOperatingPlanProcessMutation.Field()
    update_operating_plan_process = EditOperatingPlanProcessMutation.Field()
    upload_professors_list = UploadProfessorsList.Field()
