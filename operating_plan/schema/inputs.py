"""
Inputs classes for OperatingPlan mutations
"""
from graphene import (
    InputObjectType,
    ID,
    String,
    Boolean, Int, Date,
)
from graphene_file_upload.scalars import Upload


class AddObservationInput(InputObjectType):
    """
    Add observation input
    """

    category = ID(required=True)
    operating_plan_program = ID(required=True)
    comment = String(required=True)
    is_approved = Boolean(required=True)


class EditObservationInput(InputObjectType):
    """
    Edit observation input
    """

    operating_plan_program = ID()
    category = ID()
    comment = String()
    is_approved = Boolean()
    observation_id = ID(required=True)


class UploadOperatingPlanDocumentInput(InputObjectType):
    """
    Upload operating plan document input
    """
    op_plan_program_id = ID()
    op_plan_file = Upload(required=True, description="Operating plan document")


class CreateProfessorRegistrationInput(InputObjectType):
    """
    Input of mutation to create professor registration
    """

    operating_plan_program = ID(required=True)
    professor = ID(required=True)
    is_director = Boolean(required=True)
    is_coordinator = Boolean(required=True)


class CreateOpPlanProcessInput(InputObjectType):
    """
    Input of mutation to create operating plan process
    """

    year = Int(required=True)
    start_date = Date(required=True)
    end_date = Date(required=True)
    process_letter = Upload(required=True)
    descriptive_index_file = Upload(required=True)
    op_index = Upload(required=True)


class EditOpPlanProcessInput(InputObjectType):
    """
    Input of mutation to update operating plan process
    """
    process_id = ID(required=True)
    year = Int()
    start_date = Date()
    end_date = Date()
    postponement_date = Date()


class UploadProfessorsListInput(InputObjectType):
    """
    Input of mutation to upload professors list
    """
    operating_plan_program = ID(required=True)
    professors_list = Upload(required=True)
