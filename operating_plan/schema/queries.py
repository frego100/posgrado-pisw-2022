"""
Operating plan queries
"""
from graphene import (
    ObjectType,
    List,
    ID,
    Int,
    Field,
    String,
    Boolean,
)

from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required
from pyasn1.type.univ import Null

from academics.models import Course, CourseGroup
from academics.schema.types import CourseType, CourseGroupType
from core.utils import get_object_by_id
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    ProfessorRegistration,
    Observation,
    OperatingPlanDocument,
    OperatingPlanStatusChoices,
    OperatingPlanProcess,
)
from operating_plan.schema.types import (
    OperatingPlanType,
    OperatingPlanProgramType,
    ProfessorRegistrationType, ObservationType, OperatingPlanDocumentType,
    OperatingPlanProcessType,
)


class Query(ObjectType):
    """
    OperatingPlan Query Object
    """

    get_operating_plans = List(
        OperatingPlanType,
        process=ID(),
        description=_('Get all operating plans by operating plan process')
    )
    get_operating_plan_processes = List(
        OperatingPlanProcessType,
        active=Boolean(required=False),
        description=_('Get operating plan processes')
    )
    get_operating_plan_process = Field(
        OperatingPlanProcessType,
        process=ID(),
        description=_('Get operating plan process')
    )
    get_all_program_operating_plans = List(
        OperatingPlanProgramType,
        unit=ID(),
        process=ID(),
        description=_('Get program operating plans related to graduate unit')
    )
    get_program_operating_plan = Field(
        OperatingPlanProgramType,
        program_op_plan_id=ID(),
        description=_('Get program operating plan by id')
    )
    get_all_operating_plan_professors = List(
        ProfessorRegistrationType,
        operating_plan_program_id=ID(required=True),
        is_deleted=Boolean(),
        is_in_operating_plan=Boolean(),
        approved=Boolean(required=False),
        description=_('Get professors registered in program operating plan')
    )
    get_all_operating_plan_observations = List(
        ObservationType,
        program_op_plan_id=ID(),
        description=_('Get observations registered in program operating plan')
    )
    get_operating_plan_template = Field(
        String,
        process=ID(),
        description=_('Get operating plan document template')
    )
    get_operating_plan_program_documents = List(
        OperatingPlanDocumentType,
        program_op_plan_id=ID(),
        description=_('Get operating plan documents sent by study program')
    )
    get_professors = List(
        ProfessorRegistrationType,
        study_program_id=ID(),
        process=ID(required=False),
    )
    get_courses_approved = List(
        CourseType,
        study_program_id=ID(),
        process=ID()
    )
    get_courses_group_approved = List(
        CourseGroupType,
        study_program_id=ID(),
        process=ID()
    )
    get_student_enrollment_processes = List(
        OperatingPlanProcessType,
    )
    get_professor_operating_plan_programs = List(
        OperatingPlanProgramType,
        process_id=ID(required=True),
        description=_('Get operating plan programs where professor is assigned')
    )

    @login_required
    def resolve_get_operating_plans(self, info, **kwargs):
        process = kwargs.pop('process')
        return OperatingPlan.objects.filter(op_plan_process_id=process).order_by('graduate_unit__name')

    @login_required
    def resolve_get_operating_plan_processes(self, info, **kwargs):
        active = kwargs.pop('active', None)
        if active:
            unit_id = info.context.user.unitmanager.graduate_unit_id
            return OperatingPlanProcess.objects.filter(
                operatingplan__graduate_unit=unit_id,
                operatingplan__status=OperatingPlanStatusChoices.APPROVED
            )
        return OperatingPlanProcess.objects.all()

    @login_required
    def resolve_get_operating_plan_process(self, info, process):
        return get_object_by_id(OperatingPlanProcess, process)

    @login_required
    def resolve_get_all_program_operating_plans(self, info, unit, process):
        return OperatingPlanProgram.objects.filter(
            operating_plan__graduate_unit_id=unit,
            operating_plan__op_plan_process_id=process
        ).order_by('study_program__code')

    @login_required
    def resolve_get_program_operating_plan(self, info, program_op_plan_id):
        return get_object_by_id(OperatingPlanProgram, program_op_plan_id)

    @login_required
    def resolve_get_all_operating_plan_professors(self, info, **kwargs):
        approved = kwargs.pop('approved', None)
        professors = ProfessorRegistration.objects.filter(**kwargs)
        if approved:
            return professors.filter(
                operating_plan_program__operating_plan__status=OperatingPlanStatusChoices.APPROVED
            )
        return professors

    @login_required
    def resolve_get_all_operating_plan_observations(self, info, program_op_plan_id):
        return Observation.objects.filter(
            operating_plan_program_id=program_op_plan_id
        ).order_by('category__index')

    @login_required
    def resolve_get_operating_plan_template(self, info, process):
        op_process: OperatingPlanProcess = get_object_by_id(
            OperatingPlanProcess, process
        )
        if op_process:
            return op_process.descriptive_index_file.url
        else:
            return None

    @login_required
    def resolve_get_operating_plan_program_documents(
            self, info, program_op_plan_id
    ):
        return OperatingPlanDocument.objects.filter(
            operating_plan_program_id=program_op_plan_id
        )

    @login_required
    def resolve_get_professors(
            self, info, **kwargs
    ):
        study_program_id = kwargs.get('study_program_id')
        process = kwargs.get('process')
        professors = ProfessorRegistration.objects.filter(
            operating_plan_program__study_program_id=study_program_id,
            operating_plan_program__operating_plan__status=OperatingPlanStatusChoices.APPROVED,
        ).distinct()

        if process:
            return professors.filter(
                operating_plan_program__operating_plan__op_plan_process_id=process
            )

        return professors

    @login_required
    def resolve_get_courses_approved(
            self, info, study_program_id, process
    ):
        return Course.objects.filter(
            operating_plan_program__study_program_id=study_program_id,
            operating_plan_program__operating_plan__status=OperatingPlanStatusChoices.APPROVED,
            operating_plan_program__operating_plan__op_plan_process_id=process
        ).order_by('study_plan_subject__code')

    @login_required
    def resolve_get_courses_group_approved(
            self, info, study_program_id, process
    ):
        return CourseGroup.objects.filter(
            course__operating_plan_program__study_program_id=study_program_id,
            course__operating_plan_program__operating_plan__status=OperatingPlanStatusChoices.APPROVED,
            course__operating_plan_program__operating_plan__op_plan_process_id=process,
        )

    @login_required
    def resolve_get_student_enrollment_processes(self, info):
        student_id = info.context.user.student.id
        return OperatingPlanProcess.objects.filter(
            enrollmentperiod__enrollment__student_id=student_id,
        ).distinct()

    @login_required
    def resolve_get_professor_operating_plan_programs(self, info, **kwargs):
        professor_id = info.context.user.professor.id
        process_id = kwargs.get('process_id')

        return OperatingPlanProgram.objects.filter(
            course__coursegroup__professor__professor_id=professor_id,
            operating_plan__op_plan_process=process_id
        ).distinct()
