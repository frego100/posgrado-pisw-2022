import graphene
from graphene_django.types import DjangoObjectType

from academics.models import CourseGroup
from operating_plan.constants import OperatingPlanStatusChoices
from operating_plan.models import (
    OperatingPlan,
    OperatingPlanProgram,
    ProfessorRegistration, Observation, ObservationCategory,
    OperatingPlanDocument, OperatingPlanProcess,
)


class OperatingPlanProcessType(DjangoObjectType):
    """
    OperatingPlanProcess graphql type
    """

    class Meta:
        model = OperatingPlanProcess

    is_active = graphene.Boolean()
    count_approved = graphene.JSONString()
    descriptive_index_file_url = graphene.String()

    def resolve_is_active(self, info):
        return self.is_active

    def resolve_process_letter_url(self, info):
        if self.process_letter:
            return self.process_letter.url

        return ''

    def resolve_descriptive_index_file_url(self, info):
        return self.descriptive_index_file.url if self.descriptive_index_file else ''

    def resolve_count_approved(self, info):
        total = self.operatingplan_set.count()
        approved = self.operatingplan_set.filter(
            status=OperatingPlanStatusChoices.APPROVED
        ).count()
        return {'approved': approved, 'not_yet_approved': total - approved}


class OperatingPlanType(DjangoObjectType):
    """
    OperatingPlan type
    """

    class Meta:
        model = OperatingPlan

    status = graphene.String()
    approval_rd_name = graphene.String()
    approval_rd_url = graphene.String()
    sent_late = graphene.Boolean()

    def resolve_approval_rd_name(self, info):
        if self.approval_rd:
            return f"RD_Aprobacion_{self.op_plan_process.year}"

        return ''

    def resolve_approval_rd_url(self, info):
        if self.approval_rd:
            return self.approval_rd.url

        return ''

    def resolve_status(self, info):
        return self.get_status_display()

    def resolve_sent_late(self, info):
        return self.sent_late


class OperatingPlanProgramType(DjangoObjectType):
    """
    OperatingPlanProgram type
    """

    class Meta:
        model = OperatingPlanProgram

    plan_document_url = graphene.String()
    is_approved = graphene.Boolean()

    def resolve_plan_document_url(self, info):
        if self.plan_document:
            return self.plan_document.url

        return None

    def resolve_is_approved(self: OperatingPlanProgram, info):
        return self.observation_set.filter(is_approved=False).count() == 0


class ProfessorRegistrationType(DjangoObjectType):
    """
    ProfessorRegistration type
    """

    assigned_courses = graphene.Field(
        graphene.Int,
        study_program_id=graphene.ID(),
        process=graphene.ID()
    )

    class Meta:
        model = ProfessorRegistration

    def resolve_assigned_courses(self: ProfessorRegistration, info, **kwargs):
        operating_plan_program_id = kwargs.get('operating_plan_program_id')
        process = kwargs.get('process')
        filters = {}

        if operating_plan_program_id:
            filters['course__operating_plan_program_id'] = operating_plan_program_id

        if process:
            filters['course__operating_plan_program__operating_plan__op_plan_process_id'] = process

        return self.coursegroup_set.all().filter(**filters).distinct().count()


class ObservationType(DjangoObjectType):
    """
    Observation type
    """

    class Meta:
        model = Observation


class ObservationCategoryType(DjangoObjectType):
    """
    ObservationCategory type
    """

    class Meta:
        model = ObservationCategory


class OperatingPlanDocumentType(DjangoObjectType):
    """
    OperatingPlanDocumentType
    """

    class Meta:
        model = OperatingPlanDocument

    plan_document_name = graphene.String()
    plan_document_url = graphene.String()

    def resolve_plan_document_name(self, info):
        return self.__str__()

    def resolve_plan_document_url(self, info):
        if self.plan_document:
            return self.plan_document.url

        return None
