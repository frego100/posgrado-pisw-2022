# Postgraduate backend

## System Requirements

There is the complete list of packages required by the project.
Update your system before start.

    $ sudo apt-get update
    
### Pip3
    
    $ sudo apt-get install python3-pip

### Postgresql
    
    $ sudo apt install postgresql postgresql-contrib
    $ sudo apt-get install binutils libproj-dev gdal-bin postgis
    
### Redis (Just in case)
    
    $ sudo apt install redis-server

## Install Project

### 1) Create a virtualenv with python3.8 and activate

```
$ python3 -m venv .venv  
$ source .venv/bin/activate 
```

### 2) Install requirements
```
$ pip install -r requirements.txt
```

### 3) Set .env
For environment variables configuration, you will need a to create an .env file at the same level as settings.py, just
copy the .env_example file.

## Run Project
### 1) Run migrations

    $ python manage.py migrate

### 3) Load fixtures

    $ python manage.py loaddata fixtures/organization.json
    $ python manage.py loaddata fixtures/account.json
    $ python manage.py loaddata fixtures/academics.json


### 2) Run the project

    $ python manage.py runserver

## Postgres Database
### 1) Open postgres shell
### 2) Create database and user

    $ CREATE USER username WITH PASSWORD 'password';
    $ CREATE DATABASE name_db OWNER username ENCODING 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8' TEMPLATE=template0;
    $ GRANT ALL PRIVILEGES ON DATABASE name_db TO username;
    $ GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO username;
    $ ALTER USER username CREATEDB;
    $ ALTER ROLE username SUPERUSER;

### 3) Set postgres database on .env
Replace DATABASE_URL variable

    $ DATABASE_URL=postgres://username:password@localhost:5432/name_db

### * If want to recreate database

    $ DROP DATABASE name_db;
    $ CREATE DATABASE name_db OWNER username ENCODING 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8' TEMPLATE=template0;
    $ GRANT ALL PRIVILEGES ON DATABASE name_db TO username;
