import json

from django.test import tag
from graphene_django.utils.testing import graphql_query, GraphQLTestCase
from graphene_file_upload.django.testing import file_graphql_query


DEFAULT_GRAPHQL_URL = '/graphql/'


@tag('unit')
class BaseGraphQLTestCase(GraphQLTestCase):
    """
    Based on: https://www.sam.today/blog/testing-graphql-with-graphene-django/
    """

    # URL to graphql endpoint
    GRAPHQL_URL = DEFAULT_GRAPHQL_URL

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls._credentials = {}
        cls._user_agent = {
            'HTTP_USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Hotjar Chrome/74.0.3729.131 Safari/537.36',
        }
        cls._referer = {
            'HTTP_REFERER': 'http://local.hotjar.com/',
        }

    def query(self, query, op_name=None, input_data=None, variables=None,
              headers=None, files=None):
        """
        Args:
            query (string)    - GraphQL query to run
            op_name (string)  - If the query is a mutation or named query, you must
                                supply the op_name.  For annon queries ("{ ... }"),
                                should be None (default).
            input_data (dict) - If provided, the $input variable in GraphQL will be set
                                to this value. If both ``input_data`` and ``variables``,
                                are provided, the ``input`` field in the ``variables``
                                dict will be overwritten with this value.
            variables (dict)  - If provided, the "variables" field in GraphQL will be
                                set to this value.
            headers (dict)    - If provided, the headers in POST request to GRAPHQL_URL
                                will be set to this value.

        Returns:
            Response object from client
        """
        if headers is None:
            headers = {}
        headers.update(self._credentials)
        headers.update(self._user_agent)
        headers.update(self._referer)

        if files:
            return file_graphql_query(
                query,
                op_name=op_name,
                input_data=input_data,
                variables=variables,
                files=files,
                headers=headers,
                graphql_url=self.GRAPHQL_URL,
            )
        return graphql_query(
            query,
            op_name=op_name,
            input_data=input_data,
            variables=variables,
            headers=headers,
            graphql_url=self.GRAPHQL_URL,
        )

    def assertLoginResponseNoErrors(self, resp):
        """
        Assert that the call went through correctly.
        the call was fine.
        :resp HttpResponse: Response
        """
        content = json.loads(resp.content)
        self.assertNotIn('errors', list(content.keys()))

    def assertLoginResponseHasSpecificError(self, resp, message):
        """
        Assert that the call has some specific message error.
        Take care: Even with errors, GraphQL returns status 200!
        :resp HttpResponse: Response
        :message String: Error message to be found
        """
        content = json.loads(resp.content)
        errors = content['errors'][0]
        self.assertIn(message, list(errors.values()))

    @classmethod
    def get_field_from_response(cls, response, field, sub_field=None):
        content = json.loads(response.content)['data']
        field = next(iter(content.values()))[field]
        field = field[sub_field] if sub_field else field
        return field

    def assertResponseFormFieldErrorIsNone(self, response):
        """
        For Forms: Assert that the response doesn't have fieldErrors.
        :response HttpResponse: Response
        """

        self.assertIsNone(self.get_field_from_response(response, 'fieldErrors'))

    def assertResponseFormFieldError(self, response, field, contains=True):
        """
        For Forms: Assert that the response has or not some specific error
        in a field.
        :response HttpResponse: Response
        :field String: Field to check if exists or not in the fieldError list
        :contains Boolean: If contains is True do a AssertIn,
        otherwise AssertNotIn
        """
        list_of_errors = []
        for error in self.get_field_from_response(response, 'fieldErrors'):
            list_of_errors.append(error['field'])

        assert_function = self.assertIn if contains else self.assertNotIn
        assert_function(field, list_of_errors)

    def assertResponseField(self, response, field='success', sub_field=None,
                            value=None):
        """
        Assert that the response has success some field (with some subfield)
        with some value
        :response HttpResponse: Response
        :field String: Key to check in dictionary
        :sub_field String: Key inside the "field" of the dictionary
        :value String: Value expected in that field/sub-field of dictionary
        """
        if value:
            self.assertEqual(
                self.get_field_from_response(response, field, sub_field), value
            )
        else:
            self.get_field_from_response(response, field, sub_field)
