from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from django.test import SimpleTestCase, tag


class SeleniumBaseTestCase(SimpleTestCase):
    """
    Base selenium test case
    """
    driver = None

    @classmethod
    def setUpClass(cls) -> None:
        super(SeleniumBaseTestCase, cls).setUpClass()
        chrome_options = Options()
        cls.driver = webdriver.Chrome(chrome_options=chrome_options)
        cls.driver.implicitly_wait(2)
        # cls.driver.maximize_window()
        cls.driver.set_window_size(width=1920, height=1080)

    @classmethod
    def tearDownClass(cls) -> None:
        sleep(2)
        super(SeleniumBaseTestCase, cls).tearDownClass()


@tag('local_selenium')
class LocalSeleniumTestCase(SeleniumBaseTestCase, StaticLiveServerTestCase):
    """
    Base test case of end to end tests
    """
    port = 8000
    host = 'localhost'

    @classmethod
    def setUpClass(cls) -> None:
        sleep(2)
        super(LocalSeleniumTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()
        super(LocalSeleniumTestCase, cls).tearDownClass()
