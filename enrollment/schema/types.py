"""
Enrollments types
"""
import graphene
from graphene import (
    String,
    ObjectType,
    Int,
    Field
)
from graphene_django.types import DjangoObjectType

from django.db.models import Count

from enrollment.models import (
    StudentCourseAttendance,
    Enrollment,
    GroupEnrollment,
    AcademicSession,
    AdmissionProcess,
    StudyProgramAdmission,
    EnrollmentPeriod,
    EnrollmentRemoval,
    StudentGrade,
    EnrollmentUpdate,
)
from enrollment.constants import ATTENDANCE_TYPE_MAP


class AttendanceCountType(ObjectType):
    """
    Attendance Count type
    """

    on_time = Int()
    late = Int()
    absent = Int()
    excused = Int()


class GroupEnrollmentType(DjangoObjectType):
    """
    group enrollment type
    """

    attendances = Field(AttendanceCountType)
    status = graphene.String()

    class Meta:
        model = GroupEnrollment

    def resolve_attendances(self: GroupEnrollment, info):
        attendances = self.studentcourseattendance_set.exclude(
            attendance__isnull=True
        ).values(
            'attendance'
        ).annotate(attendances=Count('attendance'))
        attendance_dict = {
            'on_time': 0,
            'late': 0,
            'absent': 0,
            'excused': 0
        }
        for attendance in attendances:
            attendance_type = attendance['attendance']
            attendance_dict[ATTENDANCE_TYPE_MAP[attendance_type]] = attendance['attendances']

        return attendance_dict

    def resolve_status(self, info):
        return self.get_status_display()


class StudentCourseAttendanceType(DjangoObjectType):
    """
    studentCourse attendance type
    """

    class Meta:
        model = StudentCourseAttendance

    attendance = graphene.String()


class AcademicSessionType(DjangoObjectType):
    """
    Academic Session type
    """

    class Meta:
        model = AcademicSession


class AdmissionProcessType(DjangoObjectType):
    """
    Acceptance Record type
    """

    excel_file = String()

    class Meta:
        model = AdmissionProcess
        fields = ('id', 'excel_file', 'date', 'name', )

    def resolve_excel_file(self: AdmissionProcess, info):
        if self.excel_file:

            return self.excel_file.url

        return None


class StudyProgramAdmissionType(DjangoObjectType):
    """
    Study Program Acceptance type
    """

    class Meta:
        model = StudyProgramAdmission


class AvailableEnrollmentPeriodType(ObjectType):
    """
    Available Enrollment Period type
    """

    year = Int()
    enrollment_period = Int()


class EnrollmentPeriodType(DjangoObjectType):
    """
    Enrollment Period type
    """

    class Meta:
        model = EnrollmentPeriod


class EnrollmentRemovalType(DjangoObjectType):
    """
    Enrollment Removal type
    """

    class Meta:
        model = EnrollmentRemoval

    withdraw_type = graphene.String()
    removal_resolution = graphene.String()

    def resolve_withdraw_type(self, info):
        return self.get_withdraw_type_display()

    def resolve_removal_resolution(self: EnrollmentRemoval, info):
        return self.removal_resolution.url if self.removal_resolution else None


class EnrollmentType(DjangoObjectType):
    """
    student course attendance type
    """

    class Meta:
        model = Enrollment

    status = graphene.String()

    def resolve_status(self, info):
        return self.get_status_display()


class GradeType(DjangoObjectType):
    """
    StudentGrade type
    """
    class Meta:
        model = StudentGrade


class EnrollmentUpdateType(DjangoObjectType):
    """
    Enrollment Update type
    """
    class Meta:
        model = EnrollmentUpdate

    enrollment_update_resolution = graphene.String()

    def resolve_enrollment_update_resolution(self: EnrollmentUpdate, info):
        return self.enrollment_update_resolution.url if self.enrollment_update_resolution else None