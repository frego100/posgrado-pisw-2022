"""
Enrollments mutations
"""
from dateutil import rrule
from graphene import (
    ObjectType,
    List,
    ID,
    String,
    Int, Field,
)
from graphene_django.types import ErrorType
from graphene_file_upload.scalars import Upload
from zipfile import BadZipFile

from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import login_required

from academics.models import Course, CourseGroup, CourseProgress, StudyPlanSubject
from account.models import User
from account.serializers import StudentSerializer
from enrollment.messages.success_messages import (
    ENROLLMENT_GROUP_SET,
    STUDENT_COURSE_ATTENDANCE_ADDED,
    STUDENT_COURSE_ATTENDANCE_DELETED,
    STUDENT_COURSE_ATTENDANCE_CHANGED,
    ACCEPTANCE_RECORD_ADDED,
    STUDENT_ENROLLED,
    ACADEMIC_SESSIONS_CREATED,
    ENROLLMENT_REMOVAL_CREATED,
    STUDENT_GRADES_CREATED,
    COURSE_ENROLLMENTS_DELETED,
    PERIOD_ENROLLMENTS_DELETED,
    ENROLLMENT_UPDATE_CREATED,
    STUDENTS_ADDED, STUDENT_COURSE_ATTENDANCE_STATUS_CHANGED,
)
from enrollment.messages.error_messages import (
    COULD_NOT_ADD_GROUP_ENROLLMENTS,
    COULD_NOT_ADD_STUDENT_COURSE_ATTENDANCE,
    COULD_NOT_DELETE_STUDENT_COURSE_ATTENDANCE,
    COULD_NOT_CHANGE_STUDENT_COURSE_ATTENDANCE,
    COULD_NOT_SET_ENROLLMENT_GROUP,
    COULD_NOT_ADD_ACCEPTANCE_RECORD,
    COULD_NOT_ENROLL_STUDENT,
    COULD_NOT_CREATE_ACADEMIC_SESSIONS,
    COULD_NOT_CREATE_ENROLLMENT_REMOVAL,
    COULD_NOT_CREATE_STUDENTS_GRADES,
    COULD_NOT_DELETE_COURSE_ENROLLMENTS,
    COULD_NOT_DELETE_PERIOD_ENROLLMENTS,
    STUDENT_HAS_ENROLLMENTS_ALREADY_CREATED,
    COULD_NOT_REGISTER_STUDENT,
    COULD_NOT_ADD_STUDENTS,
)
from enrollment.schema.inputs import (
    AddStudentCourseAttendanceInput,
    EditStudentCourseAttendanceInput,
    SetEnrollmentGroupInput,
    CreateEnrollmentRemovalInput,
    AddManualEnrollmentInput,
    EditStudentGradeInput,
    SetStudentGradeInput,
    CreateEnrollmentUpdateInput,
)
from enrollment.models import (
    StudentCourseAttendance,
    AcademicSession,
    GroupEnrollment,
    Enrollment,
    StudentGrade,
    EnrollmentPeriod,
    EnrollmentUpdate,
)
from enrollment.schema.types import (
    StudentCourseAttendanceType, GradeType,
)
from enrollment.serializers import (
    StudentGradeSerializer,
    ManualEnrollmentSerializer,
    StudentCourseAttendanceSerializer,
    EnrollmentRemovalSerializer,
)
from enrollment.utils import (
    create_group_enrollments_from_file,
    create_admission_process_from_file,
    update_enrollment_status,
    student_has_enrollments_already_created, create_students_from_file,
)
from core.schema.mutations import BaseMutation
from core.schema.types import FeedbackType, EnumStatus
from core.utils import get_object_by_id, pop_sub_dict


class AddStudentCourseAttendanceMutation(BaseMutation):
    """
    Add student course attendance mutation
    """
    student_course_attendances = List(StudentCourseAttendanceType)

    class Arguments:
        input = List(AddStudentCourseAttendanceInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, student_course_attendance in enumerate(inputs):
            serializer = StudentCourseAttendanceSerializer(
                data=student_course_attendance)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update(
                    {f'{key}_{index}': serializer.errors[key] for key in
                     serializer.errors})

        if errors:
            return AddStudentCourseAttendanceMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_STUDENT_COURSE_ATTENDANCE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        student_course_attendances = [serializer.save() for serializer in
                                      serializers]

        return AddStudentCourseAttendanceMutation(
            feedback=FeedbackType(
                message=STUDENT_COURSE_ATTENDANCE_ADDED,
                status=EnumStatus.SUCCESS
            ),
            student_course_attendances=student_course_attendances
        )


class EditStudentCourseAttendanceMutation(BaseMutation):
    """
    Edit student course attendance mutation
    """

    student_course_attendances = List(StudentCourseAttendanceType)

    class Arguments:
        input = List(EditStudentCourseAttendanceInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, student_course_attendance in enumerate(inputs):
            student_course_attendance_id = student_course_attendance.pop(
                'student_course_attendance_id'
            )
            student_course_attendance_model = get_object_by_id(
                StudentCourseAttendance,
                student_course_attendance_id
            )
            student_course_attendance_model.is_editable_attendance = False
            serializer = StudentCourseAttendanceSerializer(
                instance=student_course_attendance_model,
                data=student_course_attendance,
                partial=True
            )

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update(
                    {
                        f'{key}_{index}': serializer.errors[key]
                        for key in serializer.errors
                    }
                )

        if errors:
            return EditStudentCourseAttendanceMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_CHANGE_STUDENT_COURSE_ATTENDANCE,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        student_course_attendances = [
            serializer.save() for serializer in serializers
        ]
        return EditStudentCourseAttendanceMutation(
            feedback=FeedbackType(
                message=STUDENT_COURSE_ATTENDANCE_CHANGED,
                status=EnumStatus.SUCCESS
            ),
            student_course_attendances=student_course_attendances
        )


class DeleteStudentCourseAttendanceMutation(BaseMutation):
    """
    Delete student course attendance  mutation
    """

    class Arguments:
        student_course_attendance_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        student_course_attendance_id = kwargs.pop(
            'student_course_attendance_id')
        student_course_attendance = get_object_by_id(StudentCourseAttendance,
                                                     student_course_attendance_id)

        if student_course_attendance:
            student_course_attendance.delete()

            return DeleteStudentCourseAttendanceMutation(
                feedback=FeedbackType(
                    message=STUDENT_COURSE_ATTENDANCE_DELETED,
                    status=EnumStatus.SUCCESS
                )
            )

        return DeleteStudentCourseAttendanceMutation(
            feedback=FeedbackType(
                message=COULD_NOT_DELETE_STUDENT_COURSE_ATTENDANCE,
                status=EnumStatus.ERROR
            )
        )


class OpenStudentAttendanceRegistration(BaseMutation):
    """
    Mutation to enable student to put attendances
    """

    class Arguments:
        academic_session_id = ID(required=True)

    @login_required
    def mutate(self, infor, **kwargs):
        academic_session_id = kwargs.pop('academic_session_id')
        StudentCourseAttendance.objects.filter(
            academic_session_id=academic_session_id
        ).update(is_editable_attendance=True)

        return OpenStudentAttendanceRegistration(
            feedback=FeedbackType(
                message=STUDENT_COURSE_ATTENDANCE_STATUS_CHANGED,
                status=EnumStatus.SUCCESS
            ),
        )


class StudentCourseAttendanceRegistration(BaseMutation):
    """
    Add student course attendance registration mutation
    """
    student_course_attendance = Field(StudentCourseAttendanceType)

    class Arguments:
        input = EditStudentCourseAttendanceInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        input = kwargs.pop('input')
        student_course_attendance_id = input.pop('student_course_attendance_id')
        attendance = get_object_by_id(
            StudentCourseAttendance, student_course_attendance_id
        )
        serializer = StudentCourseAttendanceSerializer(
            instance=attendance, data=input, partial=True)

        if serializer.is_valid():
            student_course_attendance = serializer.save()
            attendance.is_editable_attendance = False
            attendance.save()
            return StudentCourseAttendanceRegistration(
                feedback=FeedbackType(
                    message=STUDENT_COURSE_ATTENDANCE_ADDED,
                    status=EnumStatus.SUCCESS
                ),
                student_course_attendance=student_course_attendance
            )

        return StudentCourseAttendanceRegistration(
            feedback=FeedbackType(
                message=COULD_NOT_ADD_STUDENT_COURSE_ATTENDANCE,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class CreateAcademicSessions(BaseMutation):
    """
    Create Academic Sessions mutation
    """

    class Arguments:
        course_group_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        course_group_id = kwargs.get('course_group_id')
        course_group = CourseGroup.objects.prefetch_related(
            'schedule_set',
            'groupenrollment_set'
        ).get(pk=course_group_id)
        schedules = course_group.schedule_set.all()
        group_enrollments = course_group.groupenrollment_set.all()
        start_date = course_group.start_date
        end_date = course_group.end_date

        academic_sessions = []
        student_attendances = []
        course_progress_instances = []

        if start_date and end_date:
            for schedule in schedules:
                day = schedule.day - 1

                dates = list(
                    rrule.rrule(
                        freq=rrule.WEEKLY,
                        byweekday=day,
                        dtstart=course_group.start_date,
                        until=course_group.end_date
                    )
                )

                for date in dates:
                    academic_session = AcademicSession(
                        date=date,
                        schedule=schedule
                    )
                    academic_sessions.append(academic_session)

                    for group_enrollment in group_enrollments:
                        student_attendances.append(
                            StudentCourseAttendance(
                                academic_session=academic_session,
                                group_enrollment=group_enrollment
                            )
                        )

                    course_progress_instances.append(
                        CourseProgress(
                            academic_session=academic_session
                        )
                    )

            AcademicSession.objects.bulk_create(academic_sessions)
            StudentCourseAttendance.objects.bulk_create(student_attendances)
            CourseProgress.objects.bulk_create(course_progress_instances)

            return CreateAcademicSessions(
                feedback=FeedbackType(
                    message=ACADEMIC_SESSIONS_CREATED,
                    status=EnumStatus.SUCCESS
                )
            )

        return CreateAcademicSessions(
            feedback=FeedbackType(
                message=COULD_NOT_CREATE_ACADEMIC_SESSIONS,
                status=EnumStatus.ERROR
            )
        )


class AddStudentsMutation(BaseMutation):
    """
    Mutation to register student enrollments for some enrollment period
    """

    class Arguments:
        students_file = Upload(required=True)
        enrollment_period_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        try:
            # All existing enrollments (except the ones added by enrollment update)
            # must be deleted to avoid duplicates
            period_id = kwargs.get('enrollment_period_id')
            enrollment_period = EnrollmentPeriod.objects.get(pk=period_id)
            enrollment_period.enrollment_set.all().exclude(
                is_enrollment_update=True
            ).delete()

            create_students_from_file(**kwargs)

            return AddStudentsMutation(
                feedback=FeedbackType(
                    message=STUDENTS_ADDED,
                    status=EnumStatus.SUCCESS
                )
            )

        # TODO: Handle specific exceptions
        except (BadZipFile, Exception) as e:

            return AddStudentsMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_STUDENTS,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({'non_field_errors': [str(e)]})
            )


class AddGroupEnrollmentsMutation(BaseMutation):
    """
    Add group enrollments mutation
    """
    total_enrollments = Int()
    failed_enrollments = List(Int)

    class Arguments:
        enrollments_file = Upload(required=True)
        course_id = ID(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        enrollments_file = kwargs.get('enrollments_file')
        course_id = kwargs.get('course_id')

        course: Course = get_object_by_id(Course, course_id)
        try:
            if not Enrollment.objects.filter(
                enrollment_period__program=course.operating_plan_program.study_program,
                enrollment_period__process=course.operating_plan_program.operating_plan.op_plan_process,
                enrollment_period__period=course.study_plan_subject.semester
            ).exists():
                return AddGroupEnrollmentsMutation(
                    feedback=FeedbackType(
                        message="Aún no se han registrado matrículas en el periodo del curso correspondiente",
                        status=EnumStatus.ERROR
                    ),
                )

            # All existing group enrollments (except the ones added by enrollment update)
            # must be deleted to avoid duplicates
            GroupEnrollment.objects.filter(group__course_id=course_id).exclude(
                enrollment__is_enrollment_update=True
            ).delete()

            total_enrollments, failed_enrollments = create_group_enrollments_from_file(
                enrollments_file, course_id
            )
            if total_enrollments == 0:
                return AddGroupEnrollmentsMutation(
                    feedback=FeedbackType(
                        message="No se registraron estudiantes debido a que no están matriculados en el programa de estudios",
                        status=EnumStatus.ERROR
                    ),
                    total_enrollments=total_enrollments,
                    failed_enrollments=failed_enrollments
                )

            return AddGroupEnrollmentsMutation(
                feedback=FeedbackType(
                    message=f"Se registraron {total_enrollments} matrículas en la asignatura.",
                    status=EnumStatus.SUCCESS
                ),
                total_enrollments=total_enrollments,
                failed_enrollments=failed_enrollments
            )

        # TODO: Handle specific exceptions
        except (BadZipFile, ObjectDoesNotExist, Exception) as e:

            return AddGroupEnrollmentsMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_GROUP_ENROLLMENTS,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({'non_field_errors': str(e)})
            )


class SetEnrollmentGroup(BaseMutation):
    """
    Set Enrollment Group mutation
    """

    class Arguments:
        input = List(SetEnrollmentGroupInput)

    @login_required
    def mutate(self, info, **kwargs):
        group_enrollments = kwargs.get('input')
        course_groups = {}
        group_enrollment_instances = []

        for group_enrollment in group_enrollments:
            group_enrollment_id = group_enrollment.get('group_enrollment_id')
            group_id = group_enrollment.get('group_id')
            group_enrollment = get_object_by_id(
                GroupEnrollment, group_enrollment_id
            )
            if group_id in course_groups:
                group = course_groups[group_id]

            else:
                group = get_object_by_id(CourseGroup, group_id)
                course_groups[group_id] = group

            if group_enrollment and group:
                group_enrollment.group = group
                group_enrollment.is_assigned = True
                group_enrollment_instances.append(group_enrollment)

            else:

                return SetEnrollmentGroup(
                    feedback=FeedbackType(
                        message=COULD_NOT_SET_ENROLLMENT_GROUP,
                        status=EnumStatus.ERROR
                    )
                )

        GroupEnrollment.objects.bulk_update(
            group_enrollment_instances, ['group', 'is_assigned']
        )

        return SetEnrollmentGroup(
            feedback=FeedbackType(
                message=ENROLLMENT_GROUP_SET,
                status=EnumStatus.SUCCESS
            )
        )


class AddAdmissionProcessMutation(BaseMutation):
    """
    Add Admission Process mutation
    """

    class Arguments:
        students_file = Upload(required=True)
        name = String(required=True)
        year = Int(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        students_file = kwargs.get('students_file')
        name = kwargs.get('name')
        year = kwargs.get('year')

        try:
            create_admission_process_from_file(students_file, name, year)

            return AddAdmissionProcessMutation(
                feedback=FeedbackType(
                    message=ACCEPTANCE_RECORD_ADDED,
                    status=EnumStatus.SUCCESS
                )
            )

        # TODO: Handle specific exceptions
        except (BadZipFile, Exception) as e:

            return AddAdmissionProcessMutation(
                feedback=FeedbackType(
                    message=COULD_NOT_ADD_ACCEPTANCE_RECORD,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors({'non_field_errors': [str(e)]})
            )


class AddManualEnrollment(BaseMutation):
    """
    Add Manual Enrollment
    """

    class Arguments:
        input = AddManualEnrollmentInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        serializer = ManualEnrollmentSerializer(data=inputs)
        if serializer.is_valid():
            serializer.save()

            return AddManualEnrollment(
                feedback=FeedbackType(
                    message=STUDENT_ENROLLED,
                    status=EnumStatus.SUCCESS
                )
            )

        else:
            return AddManualEnrollment(
                feedback=FeedbackType(
                    message=COULD_NOT_ENROLL_STUDENT,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(serializer.errors)
            )


class CreateEnrollmentRemoval(BaseMutation):
    """
    Create Enrollment Removal mutation
    """

    class Arguments:
        input = CreateEnrollmentRemovalInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        data = kwargs.get('input')
        serializer = EnrollmentRemovalSerializer(data=data)

        if serializer.is_valid():
            enrollment_removal = serializer.save()
            update_enrollment_status(enrollment_removal)

            return CreateEnrollmentRemoval(
                feedback=FeedbackType(
                    message=ENROLLMENT_REMOVAL_CREATED,
                    status=EnumStatus.SUCCESS
                ),
            )

        return CreateEnrollmentRemoval(
            feedback=FeedbackType(
                message=COULD_NOT_CREATE_ENROLLMENT_REMOVAL,
                status=EnumStatus.ERROR
            ),
            errors=ErrorType.from_errors(serializer.errors)
        )


class SetStudentsGrades(BaseMutation):
    """
    Set students grades mutation
    """
    grades = List(GradeType)

    class Arguments:
        input = List(SetStudentGradeInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, grade in enumerate(inputs):
            serializer = StudentGradeSerializer(data=grade)

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update(
                    {f'{key}_{index}': serializer.errors[key] for key in serializer.errors}
                )

        if errors:
            return SetStudentsGrades(
                feedback=FeedbackType(
                    message=COULD_NOT_CREATE_STUDENTS_GRADES,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        grades = [serializer.save() for serializer in serializers]

        return SetStudentsGrades(
            feedback=FeedbackType(
                message=STUDENT_GRADES_CREATED,
                status=EnumStatus.SUCCESS
            ),
            grades=grades
        )


class EditStudentGrades(BaseMutation):
    """
    Edit students grades mutation
    """
    grades = List(GradeType)

    class Arguments:
        input = List(EditStudentGradeInput)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.pop('input')
        errors = {}
        serializers = []
        for index, grade in enumerate(inputs):
            student_grade_id = grade.pop('student_grade')
            student_grade = get_object_by_id(StudentGrade, student_grade_id)

            serializer = StudentGradeSerializer(
                instance=student_grade, data=grade, partial=True
            )

            if serializer.is_valid():
                serializers.append(serializer)
            else:
                errors.update({
                    f'{key}_{index}': serializer.errors[key]
                    for key in serializer.errors
                })

        if errors:
            return EditStudentGrades(
                feedback=FeedbackType(
                    message=COULD_NOT_CREATE_STUDENTS_GRADES,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(errors)
            )

        grades = [serializer.save() for serializer in serializers]

        return EditStudentGrades(
            feedback=FeedbackType(
                message=STUDENT_GRADES_CREATED,
                status=EnumStatus.SUCCESS
            ),
            grades=grades
        )


class DeleteCourseEnrollments(BaseMutation):
    """
    Delete course enrollments
    """

    class Arguments:
        course_id = ID()

    @login_required
    def mutate(self, info, **kwargs):
        course_id = kwargs.pop('course_id')

        try:
            course = Course.objects.get(pk=course_id)
            course.enrollment_file.delete()
            GroupEnrollment.objects.filter(group__course_id=course_id).exclude(
                enrollment__is_enrollment_update=True
            ).delete()

            return DeleteCourseEnrollments(
                feedback=FeedbackType(
                    message=COURSE_ENROLLMENTS_DELETED,
                    status=EnumStatus.SUCCESS
                ),
            )

        except Course.DoesNotExist:

            return DeleteCourseEnrollments(
                feedback=FeedbackType(
                    message=COULD_NOT_DELETE_COURSE_ENROLLMENTS,
                    status=EnumStatus.ERROR
                ),
            )


class DeletePeriodEnrollments(BaseMutation):
    """
    Delete period enrollments
    """

    class Arguments:
        period_id = ID()

    @login_required
    def mutate(self, info, **kwargs):
        period_id = kwargs.pop('period_id')
        try:
            enrollment = EnrollmentPeriod.objects.get(pk=period_id)
            enrollment.enrollment_file.delete()
            enrollment.enrollment_set.all().exclude(
                is_enrollment_update=True
            ).delete()

            return DeletePeriodEnrollments(
                feedback=FeedbackType(
                    message=PERIOD_ENROLLMENTS_DELETED,
                    status=EnumStatus.SUCCESS
                ),
            )
        except EnrollmentPeriod.DoesNotExist:

            return DeletePeriodEnrollments(
                feedback=FeedbackType(
                    message=COULD_NOT_DELETE_PERIOD_ENROLLMENTS,
                    status=EnumStatus.ERROR
                ),
            )


class CreateEnrollmentUpdate(BaseMutation):
    """
    Create Enrollment Update mutation
    """

    class Arguments:
        input = CreateEnrollmentUpdateInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        inputs = kwargs.get('input')
        enrollment_data = pop_sub_dict(
            inputs,
            [
                'group_enrollments',
                'enrollment_update_resolution',
                'resolution_number',
                'study_program',
                'process'
            ]
        )
        student_data = pop_sub_dict(
            inputs['student'], ['code', 'cui']
        )
        user_data = inputs['student']
        user_identifications = user_data['user_identifications'][0]

        serializer_data = {'user': user_data, **student_data}
        new_serializer = {}
        # Yes user, yes student
        try:
            user = User.objects.get(
                useridentification__number=user_identifications['number']
            )
            student = user.student

            new_serializer = {
                "instance": student,
                "data": serializer_data,
                "partial": True
            }
        # Yes user, no student
        except AttributeError:
            new_serializer = {"data": serializer_data}
        # No user nor student
        except User.DoesNotExist:
            new_serializer = {"data": serializer_data}

        finally:
            student_serializer = StudentSerializer(**new_serializer)

        # Create or update student
        if not student_serializer.is_valid():
            return CreateEnrollmentUpdate(
                feedback=FeedbackType(
                    message=COULD_NOT_REGISTER_STUDENT,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(student_serializer.errors)
            )
        student = student_serializer.save()

        # Validate if student has no enrollments created
        # for the current process year
        if student_has_enrollments_already_created(
                student=student,
                process=enrollment_data['process'],
                study_program=enrollment_data['study_program']
        ):
            return CreateEnrollmentUpdate(
                feedback=FeedbackType(
                    message=STUDENT_HAS_ENROLLMENTS_ALREADY_CREATED,
                    status=EnumStatus.ERROR
                ),
                errors=ErrorType.from_errors(
                    {"enrollment": [STUDENT_HAS_ENROLLMENTS_ALREADY_CREATED]}
                )
            )

        enrollments = []
        # Create enrollments
        # TODO: Handle with serializers
        try:
            for enrollment_item in enrollment_data['group_enrollments']:
                course_group = CourseGroup.objects.get(
                    pk=enrollment_item['course_group']
                )
                semester = course_group.course.study_plan_subject.semester
                enrollment_period = EnrollmentPeriod.objects.get(
                    program=enrollment_data['study_program'],
                    process=enrollment_data['process'],
                    period=semester
                )
                enrollment, _ = Enrollment.objects.get_or_create(
                    student=student,
                    enrollment_period=enrollment_period,
                    defaults={
                        "is_enrollment_update": True,
                        "courses": 0,
                        "credits": 0
                    }
                )
                try:
                    equivalent_subject = StudyPlanSubject.objects.get(
                        pk=enrollment_item['equivalent_subject'])
                except StudyPlanSubject.DoesNotExist:
                    equivalent_subject = None

                GroupEnrollment.objects.create(
                    enrollment=enrollment,
                    group=course_group,
                    is_assigned=False,
                    enrollment_num=enrollment_item['enrollment_num'],
                    equivalent_subject=equivalent_subject,
                )
                enrollments.append(enrollment)

            for enrollment in enrollments:
                enrollment.courses = enrollment.groupenrollment_set.count()
                num_of_credits = enrollment.groupenrollment_set.all().values_list(
                    'group__course__study_plan_subject__credits', flat=True
                )
                enrollment.credits = sum(list(num_of_credits))
                enrollment.save()

            enrollment_update = EnrollmentUpdate.objects.create(
                student=student,
                study_program_id=enrollment_data['study_program'],
                process_id=enrollment_data['process'],
                enrollment_update_resolution=enrollment_data['enrollment_update_resolution'],
                resolution_number=enrollment_data['resolution_number']
            )
            enrollment_update.enrollments.add(*enrollments)

            return CreateEnrollmentUpdate(
                feedback=FeedbackType(
                    message=ENROLLMENT_UPDATE_CREATED,
                    status=EnumStatus.SUCCESS
                ),
            )

        except ObjectDoesNotExist as e:
            return CreateEnrollmentUpdate(
                feedback=FeedbackType(
                    message=str(e),
                    status=EnumStatus.ERROR
                ),
            )


class Mutation(ObjectType):
    add_student_course_attendance = AddStudentCourseAttendanceMutation.Field()
    edit_student_course_attendance = EditStudentCourseAttendanceMutation.Field()
    delete_student_course_attendance = DeleteStudentCourseAttendanceMutation.Field()
    open_student_attendance_registration = OpenStudentAttendanceRegistration.Field()
    student_course_attendance_registration = StudentCourseAttendanceRegistration.Field()
    create_academic_sessions = CreateAcademicSessions.Field()
    add_group_enrollments = AddGroupEnrollmentsMutation.Field()
    set_enrollment_group = SetEnrollmentGroup.Field()
    add_admission_process = AddAdmissionProcessMutation.Field()
    add_manual_enrollment = AddManualEnrollment.Field()
    create_enrollment_removal = CreateEnrollmentRemoval.Field()
    set_students_grades = SetStudentsGrades.Field()
    edit_students_grades = EditStudentGrades.Field()
    delete_period_enrollments = DeletePeriodEnrollments.Field()
    delete_course_enrollments = DeleteCourseEnrollments.Field()
    create_enrollment_update = CreateEnrollmentUpdate.Field()
    add_students = AddStudentsMutation.Field()
