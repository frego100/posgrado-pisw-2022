"""
Enrollments inputs
"""
import graphene
from graphene import (
    InputObjectType,
    ID,
    String,
    Date, List, Int,
)
from graphene_file_upload.scalars import Upload

from account.schema.inputs import AddUserIdentificationInput, StudentInput
from account.schema.types import GenderEnum
from enrollment.constants import AttendanceType


AttendanceEnum = graphene.Enum.from_enum(AttendanceType)


class AddStudentCourseAttendanceInput(InputObjectType):
    """
    Add Student course Attendance input
    """ 
    group_enrollment = ID()
    observations = String()
    attendance = ID()
    date = Date()


class StudentCourseAttendanceRegistrationInput(InputObjectType):
    """
    Add Student course Attendance input
    """
    group_enrollment = ID()
    attendance = ID()
    academic_session = ID()


class EditStudentCourseAttendanceInput(InputObjectType):
    """
    Edit Student course Attendance input
    """

    student_course_attendance_id = ID(required=True)
    attendance = AttendanceEnum()


class SetEnrollmentGroupInput(InputObjectType):
    """
    Set Enrollment Group input
    """

    group_enrollment_id = ID(required=True)
    group_id = ID(required=True)


class AddManualEnrollmentInput(InputObjectType):
    """
    Add Manual Enrollment input
    """

    student = ID(required=True)
    course = ID(required=True)
    resolution_file = Upload(required=True)


class CreateEnrollmentRemovalInput(InputObjectType):
    """
    Create enrollment removal input
    """

    enrollment = ID(required=True)
    group_enrollments = List(ID)
    removal_resolution = Upload(required=True)
    resolution_number = String(required=True)


class SetStudentGradeInput(InputObjectType):
    """
    Input class to set students grades
    """

    student_enrollment = ID(required=True)
    score = Int(required=True)


class EditStudentGradeInput(InputObjectType):
    """
    Input class to edit student grade
    """

    student_grade = ID(required=True)
    score = Int(required=True)


class GroupEnrollmentInput(InputObjectType):
    """
    Input class for group enrollment input
    """
    course_group = ID(required=True)
    enrollment_num = Int(required=True)
    equivalent_subject = ID(required=False)


class CreateEnrollmentUpdateInput(InputObjectType):
    """
    Input class to create enrollment update
    """
    student = StudentInput(required=True)
    group_enrollments = List(GroupEnrollmentInput, required=True)
    enrollment_update_resolution = Upload(required=True)
    resolution_number = String(required=True)
    study_program = ID(required=True)
    process = ID(required=True)
