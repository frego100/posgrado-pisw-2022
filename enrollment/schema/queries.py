from graphene import ObjectType, List, ID, Date, Field, Int
from graphql_jwt.decorators import login_required

from academics.models import Course
from academics.schema.types import CourseType
from account.models import Student
from account.schema.types import StudentType
from core.utils import get_object_by_id
from enrollment.models import (
    StudentCourseAttendance,
    GroupEnrollment,
    AdmissionProcess,
    StudyProgramAdmission,
    EnrollmentPeriod,
    AcademicSession, Enrollment, EnrollmentRemoval, EnrollmentUpdate,
)
from enrollment.schema.types import (
    StudentCourseAttendanceType,
    GroupEnrollmentType,
    AcademicSessionType,
    AdmissionProcessType,
    StudyProgramAdmissionType,
    EnrollmentPeriodType,
    EnrollmentType,
    EnrollmentRemovalType,
    EnrollmentUpdateType,
)


class Query(ObjectType):
    """
    Enrollment Query Object
    """

    get_all_student_course_attendance = List(
        StudentCourseAttendanceType,
        group_enrollment_id=ID(),
        description='Get all attendance group of a student'
    )
    get_all_student_course_attendance_by_date = List(
        StudentCourseAttendanceType,
        course_group_id=ID(),
        date=Date(),
        description='Get all attendance recorded by date'
    )
    get_course_group_students = List(
        GroupEnrollmentType,
        course_group_id=ID()
    )
    get_academic_sessions = List(
        AcademicSessionType,
        course_group_id=ID()
    )
    get_student_attendances = List(
        StudentCourseAttendanceType,
        course_group_id=ID()
    )
    get_student_attendances_professor = List(
        StudentCourseAttendanceType,
        academic_session_id=ID()
    )

    get_course_students = List(
        GroupEnrollmentType,
        course_id=ID()
    )
    get_admission_years = List(Int)
    get_admission_processes = List(
        AdmissionProcessType,
        study_program_id=ID(),
        year=Int()
    )
    get_admitted_students = Field(
        StudyProgramAdmissionType,
        admission_process_id=ID(),
        study_program_id=ID()
    )
    get_admitted_students_by_year = List(
        StudentType,
        admission_year=Int(),
        study_program_id=ID(),
    )
    get_available_enrollment_periods = List(
        EnrollmentPeriodType,
        program_id=ID()
    )
    get_all_enrollment_periods = List(
        EnrollmentPeriodType,
        program_id=ID()
    )
    get_semester_courses = List(
        CourseType,
        enrollment_period_id=ID()
    )
    get_student_enrollments = List(
        EnrollmentType,
        student_id=ID(),
        process=ID(),
        program=ID(required=False),
    )
    get_student_group_enrollments = List(
        GroupEnrollmentType,
        enrollment_id=ID()
    )
    get_enrollment_removals = List(
        EnrollmentRemovalType,
        process=ID(required=True),
        graduate_unit=ID(required=False)
    )
    get_enrollment_removal = Field(
        EnrollmentRemovalType,
        enrollment_removal_id=ID(required=True)
    )
    get_enrollments_by_period = List(
        EnrollmentType,
        enrollment_period_id=ID(required=True)
    )
    get_enrollment_periods_by_process = List(
        EnrollmentPeriodType,
        process_id=ID(required=True),
        program_id=ID(required=True)
    )
    get_enrollment_updates = List(
        EnrollmentUpdateType,
        program_id=ID(required=True),
        process_id=ID(required=True)
    )
    get_enrollment_update = Field(
        EnrollmentUpdateType,
        enrollment_update_id=ID(required=True),
    )

    @login_required
    def resolve_get_all_student_course_attendance(self, info, group_enrollment_id):
        return StudentCourseAttendance.objects.filter(
            group_enrollment_id=group_enrollment_id
        )

    @login_required
    def resolve_get_course_group_students(self, info, course_group_id):
        return GroupEnrollment.objects.filter(
            group_id=course_group_id
        )

    @login_required
    def resolve_get_academic_sessions(self, info, course_group_id):
        return AcademicSession.objects.filter(
            schedule__course_id=course_group_id
        ).order_by('date')

    @login_required
    def resolve_get_student_attendances(self, info, course_group_id):
        student_id = info.context.user.student.id
        return StudentCourseAttendance.objects.filter(
            group_enrollment__enrollment__student_id=student_id,
            group_enrollment__group_id=course_group_id
        ).order_by('academic_session__date')

    @login_required
    def resolve_get_student_attendances_professor(self, info, academic_session_id):
        return StudentCourseAttendance.objects.filter(
            academic_session_id=academic_session_id
        )

    @login_required
    def resolve_get_course_students(self, info, course_id):
        return GroupEnrollment.objects.filter(
            group__course_id=course_id
        )

    @login_required
    def resolve_get_admission_years(self, info):
        return list(set(
            AdmissionProcess.objects.values_list('admission_year', flat=True)
        ))

    @login_required
    def resolve_get_admission_processes(self, info, study_program_id, year):
        return AdmissionProcess.objects.filter(
            studyprogramadmission__study_program_id=study_program_id,
            admission_year=year
        ).distinct()

    @login_required
    def resolve_get_admitted_students(
            self, info, admission_process_id, study_program_id
    ):
        try:
            study_program_admission = StudyProgramAdmission.objects.get(
                admission_process_id=admission_process_id,
                study_program_id=study_program_id
            )

        except StudyProgramAdmission.DoesNotExist:
            study_program_admission = None

        return study_program_admission

    @login_required
    def resolve_get_admitted_students_by_year(
            self, info, admission_year, study_program_id
    ):
        study_program_admissions = StudyProgramAdmission.objects.filter(
            study_program_id=study_program_id,
            admission_process__admission_year=admission_year
        )

        return Student.objects.filter(
            studyprogramadmission__in=study_program_admissions
        ).order_by('user__last_name').distinct()

    @login_required
    def resolve_get_available_enrollment_periods(self, info, program_id):
        enrollment_periods = EnrollmentPeriod.objects.filter(
            program_id=program_id
        )
        available_periods = [x for x in enrollment_periods if x.is_filled]
        return available_periods

    @login_required
    def resolve_get_all_enrollment_periods(self, info, program_id):
        enrollment_periods = EnrollmentPeriod.objects.filter(
            program_id=program_id
        )
        filled_periods = [x for x in enrollment_periods if not x.is_filled]
        return filled_periods

    @login_required
    def resolve_get_semester_courses(self, info, enrollment_period_id):
        enrollment_period = EnrollmentPeriod.objects.get(
            pk=enrollment_period_id
        )

        return Course.objects.filter(
            operating_plan_program__study_program_id=enrollment_period.program_id,
            operating_plan_program__operating_plan__op_plan_process=enrollment_period.process,
            study_plan_subject__semester=enrollment_period.period
        ).order_by('study_plan_subject__code')

    # TODO: Might be used to get the study programs for a Student
    # def resolve_get_student_enrollment_programs(self, info, student_id, process):
    #     return StudyProgram.objects.filter(
    #         enrollmentperiod__enrollment__student_id=student_id,
    #         enrollmentperiod__enrollment__groupenrollment__isnull=False,
    #         enrollmentperiod__process_id=process,
    #     ).distinct()

    @login_required
    def resolve_get_student_enrollments(self, info, student_id, process, program=None):
        enrollments = Enrollment.objects.filter(
            student_id=student_id,
            enrollment_period__process_id=process,
            groupenrollment__isnull=False
        )
        if program:
            enrollments.filter(enrollment_period__program_id=program)

        return enrollments.distinct()

    @login_required
    def resolve_get_student_group_enrollments(self, info, enrollment_id):
        return GroupEnrollment.objects.filter(enrollment_id=enrollment_id)

    @login_required
    def resolve_get_enrollment_removal(self, info, enrollment_removal_id):
        return get_object_by_id(EnrollmentRemoval, enrollment_removal_id)

    @login_required
    def resolve_get_enrollment_removals(self, info, process, graduate_unit=None):
        enrollment_removals = EnrollmentRemoval.objects.filter(
            enrollment__enrollment_period__process_id=process,
        )
        if graduate_unit:
            enrollment_removals = enrollment_removals.filter(
                enrollment__enrollment_period__program__graduate_unit_id=graduate_unit
            )

        return enrollment_removals

    @login_required
    def resolve_get_enrollments_by_period(self, info, **kwargs):
        enrollment_period_id = kwargs.get('enrollment_period_id')

        return Enrollment.objects.filter(
            enrollment_period_id=enrollment_period_id
        )

    @login_required
    def resolve_get_enrollment_periods_by_process(self, info, **kwargs):
        process_id = kwargs.get('process_id')
        program_id = kwargs.get('program_id')

        return EnrollmentPeriod.objects.filter(
            process_id=process_id, program_id=program_id
        ).order_by('period')

    @login_required
    def resolve_get_enrollment_updates(self, info, **kwargs):
        process_id = kwargs.get('process_id')
        program_id = kwargs.get('program_id')

        return EnrollmentUpdate.objects.filter(
            process_id=process_id, study_program_id=program_id
        )

    @login_required
    def resolve_get_enrollment_update(self, info, **kwargs):
        enrollment_update_id = kwargs.get('enrollment_update_id')

        return get_object_by_id(EnrollmentUpdate, enrollment_update_id)
