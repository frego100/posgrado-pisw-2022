"""
Enrollment utils
"""
import openpyxl
from dataclasses import dataclass, field

from django.contrib.auth.models import Group
from django.utils import timezone
from django.utils.dateparse import parse_date

from account.models import User, Student, UserIdentification
from account.constants import UserIdentificationTypes, STUDENT_GROUP_NAME
from account.utils import remove_after_comma_or_dot
from core.utils import get_object_by_id
from enrollment.constants import (
    GROUP_ENROLLMENTS_FILE_START_INDEX,
    ACCEPTANCE_RECORD_FILE_START_INDEX,
    EnrollmentRemovalTypes,
    EnrollmentStatus,
    ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES,
    STUDENTS_FILE_START_INDEX,
)
from academics.models import Course, CourseGroup
from enrollment.models import (
    Enrollment,
    GroupEnrollment,
    AdmissionProcess,
    StudyProgramAdmission,
    EnrollmentRemoval,
    EnrollmentPeriod,
)
from organization.models import StudyProgram


def create_students_from_file(students_file, enrollment_period_id):
    """
    Create enrollments from student file. Not-existing students will be created
    """

    workbook = openpyxl.load_workbook(students_file)
    sheet = workbook.active
    students = []
    update_students = []
    enrollments = []
    identifications = []
    users = []

    enrollment_period = get_object_by_id(EnrollmentPeriod, enrollment_period_id)

    if not enrollment_period:
        # TODO: Raise and handle exception
        return

    for row in sheet.iter_rows(min_row=STUDENTS_FILE_START_INDEX):
        _, cui, code, dni, name, _, courses, total_credits, email = row
        # When the sheet row is blank, we stop iterating over rows
        if not cui.value or not dni.value or not name.value:
            break

        last_name, first_name = name.value.split(', ')
        last_name = last_name.replace('/', ' ')
        last_name, maternal_last_name = last_name.split(' ', 1)
        dni = remove_after_comma_or_dot(str(dni.value))

        try:
            user = User.objects.select_related(
                'student'
            ).get(useridentification__number=dni)
            user.email = email.value
            users.append(user)

        except User.DoesNotExist:
            user = User.objects.create(
                email=email.value,
                first_name=first_name,
                last_name=last_name,
                maternal_last_name=maternal_last_name
            )
            identifications.append(
                UserIdentification(
                    user=user,
                    number=dni,
                    identification_type=UserIdentificationTypes.DNI
                )
            )

        cui = int(cui.value)
        try:
            student = user.student
            student.cui = cui
            student.code = code.value
            update_students.append(student)

        except AttributeError:
            student = Student(
                user=user,
                cui=cui,
                code=code.value
            )
            students.append(student)

        enrollments.append(
            Enrollment(
                student=student,
                credits=total_credits.value,
                courses=courses.value,
                enrollment_period=enrollment_period
            )
        )

    student_group = Group.objects.get(name=STUDENT_GROUP_NAME)
    student_group.user_set.add(*[student.user for student in students])
    Student.objects.bulk_create(students)
    Student.objects.bulk_update(update_students, fields=['cui', 'code'])
    Enrollment.objects.bulk_create(enrollments)
    User.objects.bulk_update(users, ['email'])
    UserIdentification.objects.bulk_create(identifications)
    enrollment_period.enrollment_file = students_file
    enrollment_period.save()


def create_group_enrollments_from_file(enrollments_file, course_id):
    """
    Create Group Enrollments from file
    """

    workbook = openpyxl.load_workbook(enrollments_file)
    sheet = workbook.active
    group_enrollments = []

    course: Course = Course.objects.select_related(
        'operating_plan_program__operating_plan',
    ).prefetch_related('coursegroup_set').get(pk=course_id)
    semester = course.study_plan_subject.semester
    enrollment_period = course.operating_plan_program.study_program.enrollmentperiod_set.get(
        period=semester,
        process_id=course.operating_plan_program.operating_plan.op_plan_process_id
    )
    group = course.coursegroup_set.first()
    enrollments = enrollment_period.enrollment_set.all().select_related('student')

    enrollments_dict = {
        enrollment.student.cui: enrollment for enrollment in enrollments
    }
    total_enrollments = 0
    failed_enrollments = []
    for index, row in enumerate(
            sheet.iter_rows(
                min_row=GROUP_ENROLLMENTS_FILE_START_INDEX
            ), start=GROUP_ENROLLMENTS_FILE_START_INDEX
    ):
        _, cui, name, enrollment_num = row

        cui = cui.value
        if cui:
            cui = str(int(cui))
            if cui in enrollments_dict:
                enrollment = enrollments_dict[cui]
                group_enrollment = GroupEnrollment(
                    enrollment=enrollment,
                    group=group,
                    enrollment_num=enrollment_num.value
                )
                group_enrollments.append(group_enrollment)
                total_enrollments += 1
        else:
            failed_enrollments.append(index)

    GroupEnrollment.objects.bulk_create(group_enrollments)
    course.enrollment_file = enrollments_file
    course.save()

    return total_enrollments, failed_enrollments


@dataclass
class AdmissionItem:
    study_program: StudyProgram
    students: list = field(default_factory=lambda: [])
    program_acceptance: StudyProgramAdmission = None

    def get_program_admission(self, record):
        self.program_acceptance = StudyProgramAdmission(
            admission_process=record,
            study_program=self.study_program
        )

        return self.program_acceptance

    def set_students(self):
        self.program_acceptance.students.set(self.students)


def create_admission_process_from_file(students_file, name, year):
    """
    Create Admission Process from file
    """

    workbook = openpyxl.load_workbook(students_file)
    sheet = workbook.worksheets[0]
    users = []
    students = []
    existing_students = []
    identifications = []
    admission_by_program_dict = {}

    for row in sheet.iter_rows(min_row=ACCEPTANCE_RECORD_FILE_START_INDEX):
        _, study_program, last_name, first_name, dni, birthday, personal_email, phone_number, *_ = row
        # When the sheet row is blank, we stop iterating over rows
        if not study_program.value:
            break

        dni = remove_after_comma_or_dot(str(dni.value))
        study_program = str(study_program.value).upper()
        if study_program not in admission_by_program_dict:
            study_program_instance = StudyProgram.objects.filter(
                name=study_program
            ).first()
            admission_by_program_dict[study_program] = AdmissionItem(
                study_program=study_program_instance
            )

        current_record = admission_by_program_dict[study_program]
        try:
            user = User.objects.get(
                useridentification__number=dni,
                useridentification__identification_type=UserIdentificationTypes.DNI
            )
            try:
                student = user.student
                existing_students.append(student)

            except AttributeError:
                student = Student(user=user)
                students.append(student)

        except User.DoesNotExist:
            last_name = last_name.value.title()
            last_name, maternal_last_name = last_name.split(' ', 1)
            user = User(
                first_name=first_name.value.title(),
                last_name=last_name,
                maternal_last_name=maternal_last_name,
                birthday=parse_date(str(birthday.value)),
                personal_email=personal_email.value,
                email=personal_email.value.lower(),
                phone=phone_number.value
            )
            users.append(user)
            identifications.append(
                UserIdentification(
                    user=user,
                    number=dni,
                    identification_type=UserIdentificationTypes.DNI
                )
            )
            student = Student(user=user)
            students.append(student)

        current_record.students.append(student)

    student_group = Group.objects.get(name=STUDENT_GROUP_NAME)
    User.objects.bulk_create(users)
    UserIdentification.objects.bulk_create(identifications)
    student_group.user_set.add(*[student.user for student in students])
    Student.objects.bulk_create(students)
    record = AdmissionProcess.objects.create(
        excel_file=students_file, name=name, admission_year=year
    )
    StudyProgramAdmission.objects.bulk_create(
        [
            admission_by_program_dict[key].get_program_admission(record)
            for key in admission_by_program_dict
        ]
    )

    for key in admission_by_program_dict:
        admission_by_program_dict[key].set_students()


def update_enrollment_status(enrollment_removal: EnrollmentRemoval):
    """
    Update enrollments status after enrollment removal is approved
    """
    group_enrollments = enrollment_removal.group_enrollments

    if enrollment_removal.withdraw_type == EnrollmentRemovalTypes.TOTAL:
        enrollment = enrollment_removal.enrollment
        enrollment.status = EnrollmentStatus.WITHDRAWN
        enrollment.save()

    for group_enrollment in group_enrollments.all():
        group_enrollment.status = EnrollmentStatus.WITHDRAWN
        group_enrollment.save()


def verify_grade_register_period(course_group: CourseGroup):
    """
    Validate if the period to register student grades is active
    for a given course
    """
    deadline = course_group.end_date + timezone.timedelta(
        days=ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES
    )
    return course_group.end_date < timezone.now().date() < deadline


def student_has_enrollments_already_created(student, process, study_program):
    """
    Check if a given student has any enrollments
    in a given process and study program
    """
    return Enrollment.objects.filter(
        enrollment_period__program_id=study_program,
        enrollment_period__process_id=process,
        student=student,
    ).exists()


def get_admitted_students(admission_year: int, study_program: StudyProgram):
    """
    Get the admitted students of a study program in a specific year
    """
    return Student.objects.filter(
        studyprogramadmission__study_program=study_program,
        studyprogramadmission__admission_process__admission_year=admission_year
    ).distinct()
