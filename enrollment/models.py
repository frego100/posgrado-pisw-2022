from django.db import models

from academics.models import CourseGroup, Schedule, StudyPlanSubject
from account.models import Student
from enrollment.constants import (
    AttendanceType,
    EnrollmentStatus,
    EnrollmentRemovalTypes,
    EnrollmentRemovalStatusTypes,
)
from operating_plan.models import OperatingPlanProcess
from organization.models import StudyProgram


class AdmissionProcess(models.Model):
    """
    Accepted Record model
    """

    name = models.CharField(max_length=150)
    excel_file = models.FileField(upload_to='acceptance_records')
    date = models.DateTimeField(auto_now_add=True)
    admission_year = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class StudyProgramAdmission(models.Model):
    admission_process = models.ForeignKey(
        AdmissionProcess, on_delete=models.CASCADE
    )
    students = models.ManyToManyField(Student)
    study_program = models.ForeignKey(
        StudyProgram,
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return f'{self.admission_process} - {self.study_program}'


class EnrollmentPeriod(models.Model):
    """
    Enrollment Period model
    """
    process = models.ForeignKey(OperatingPlanProcess, on_delete=models.CASCADE)
    program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    year = models.IntegerField()
    period = models.PositiveSmallIntegerField()
    date = models.DateField(auto_now_add=True)
    enrollment_file = models.FileField(
        null=True,
        blank=True,
        upload_to='period_enrollments/'
    )

    def __str__(self):
        return f'{self.program} {self.year} - {self.period}'

    @property
    def is_filled(self):
        return not self.enrollment_set.all().exists()


class Enrollment(models.Model):
    """
    Enrollment model
    """

    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(
        choices=EnrollmentStatus.choices, default=EnrollmentStatus.ENROLLED
    )
    enrollment_period = models.ForeignKey(
        EnrollmentPeriod, on_delete=models.CASCADE, null=True
    )
    credits = models.IntegerField()
    courses = models.IntegerField()
    is_enrollment_update = models.BooleanField(default=False)

    def __str__(self):
        return self.student.__str__()


class GroupEnrollment(models.Model):
    """
    Group enrollment model
    """

    enrollment = models.ForeignKey(Enrollment, on_delete=models.CASCADE)
    group = models.ForeignKey(CourseGroup, on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(
        choices=EnrollmentStatus.choices, default=EnrollmentStatus.ENROLLED
    )
    is_assigned = models.BooleanField(default=False)
    enrollment_num = models.IntegerField()
    date = models.DateField(auto_now_add=True)
    resolution_file = models.FileField(
        upload_to='enrollment_resolutions/', blank=True, null=True
    )
    equivalent_subject = models.ForeignKey(
        StudyPlanSubject, on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        return self.enrollment.__str__()


class AcademicSession(models.Model):
    """
    Attendance Session model
    """

    date = models.DateField()
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)


class StudentCourseAttendance(models.Model):
    """
    StudentCourse Attendance model
    """

    group_enrollment = models.ForeignKey(
        GroupEnrollment, on_delete=models.CASCADE
    )
    attendance = models.PositiveSmallIntegerField(
        choices=AttendanceType.choices, null=True, default=None
    )
    academic_session = models.ForeignKey(
        AcademicSession, on_delete=models.CASCADE, null=True
    )
    is_editable_attendance = models.BooleanField(default=False)

    def __str__(self):
        return f"Date {self.academic_session.date}"


class EnrollmentRemoval(models.Model):
    """
    Enrollment Removal model
    """
    enrollment = models.ForeignKey(Enrollment, on_delete=models.CASCADE)
    group_enrollments = models.ManyToManyField(GroupEnrollment)
    withdraw_type = models.PositiveSmallIntegerField(
        choices=EnrollmentRemovalTypes.choices,
        default=EnrollmentRemovalTypes.PARTIAL
    )
    removal_resolution = models.FileField(
        upload_to='enrollment_removals/resolutions/', null=True, blank=True
    )
    resolution_number = models.CharField(max_length=100)
    created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.enrollment.student.__str__()


class StudentGrade(models.Model):
    """
    StudentGrade model
    """
    student_enrollment = models.OneToOneField(
        GroupEnrollment, on_delete=models.CASCADE
    )
    score = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.student_enrollment.__str__()} - {self.score}"


class EnrollmentUpdate(models.Model):
    """
    EnrollmentUpdate model
    """
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    enrollments = models.ManyToManyField(Enrollment)
    study_program = models.ForeignKey(StudyProgram, on_delete=models.CASCADE)
    process = models.ForeignKey(OperatingPlanProcess, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    enrollment_update_resolution = models.FileField(null=True, blank=True)
    resolution_number = models.CharField(max_length=100)

    def __str__(self):
        return self.student.__str__()

    @property
    def group_enrollments(self):
        group_enrollments = []
        for enrollment in self.enrollments.all():
            group_enrollments.append(enrollment.groupenrollment_set.all())

        return group_enrollments
