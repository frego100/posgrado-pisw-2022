"""
Enrollments success messages
"""

STUDENT_COURSE_ATTENDANCE_ADDED = 'El registro de asistencia de alumno fue agregado'
STUDENT_COURSE_ATTENDANCE_CHANGED = 'El registro de asistencia de alumno fue editado'
STUDENT_COURSE_ATTENDANCE_DELETED = 'El registro de asistencia de alumno fue eliminado'
STUDENT_COURSE_ATTENDANCE_STATUS_CHANGED = 'Se habilitó el registro de asistencia para los alumnos'
STUDENTS_ADDED = 'Los alumnos fueron agregados exitosamente'
GROUP_ENROLLMENTS_ADDED = 'Las matriculas de grupo fueron agregadas'
ENROLLMENT_GROUP_SET = 'Se ha guardado el grupo de la matricula'
ACCEPTANCE_RECORD_ADDED = 'Se ha guardado el registro de ingresantes'
ACADEMIC_SESSIONS_CREATED = 'Se han creado las sesiones academicas'
STUDENT_ENROLLED = 'Se ha matriculado al alumno'
ENROLLMENT_REMOVAL_CREATED = "Se ha registrado el retiro de matrícula"

STUDENT_GRADES_CREATED = "Se ha registrado las notas correctamente"
COURSE_ENROLLMENTS_DELETED = 'Se han eliminado las matriculas en la asignatura'
PERIOD_ENROLLMENTS_DELETED = 'Se han eliminado las matriculas en el periodo'
ENROLLMENT_UPDATE_CREATED = "Se ha registrado la reactualización de matrícula"
