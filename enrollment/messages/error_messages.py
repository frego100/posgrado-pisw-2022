"""
Enrollments error messages
"""
from enrollment.constants import ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES

COULD_NOT_ADD_STUDENT_COURSE_ATTENDANCE = 'No se pudo agregar la asistencia del alumno en el curso'
COULD_NOT_DELETE_STUDENT_COURSE_ATTENDANCE = 'No se pudo eliminar la asistencia del alumno en el curso'
COULD_NOT_CHANGE_STUDENT_COURSE_ATTENDANCE = 'No se pudo editar la asistencia del alumno en el curso'
COULD_NOT_CHANGE_STUDENT_COURSE_ATTENDANCE_STATE = 'No se pudo habilitar el registro de asistencia para los estudiantes'
COULD_NOT_ADD_GROUP_ENROLLMENTS = 'No se pudieron agregar Las matriculas de grupo'
COULD_NOT_SET_ENROLLMENT_GROUP = 'No se pudo guardar el grupo de la matricula'
COULD_NOT_ADD_ACCEPTANCE_RECORD = 'No se pudo guardar el registro de ingresantes'
COULD_NOT_CREATE_ACADEMIC_SESSIONS = 'No se pudieron crear las sesiones academicas'
COULD_NOT_ENROLL_STUDENT = 'No se pudo matricular al alumno'
COULD_NOT_CREATE_ENROLLMENT_REMOVAL = 'No se pudo registrar el retiro de matrícula'

COULD_NOT_CREATE_STUDENTS_GRADES = "No se pudo registrar las notas"
COULD_NOT_UPDATE_STUDENTS_GRADES = "No se pudo actualizar las notas"
INVALID_GRADES_REGISTER_PERIOD = (
    f"El plazo para el registro de notas es "
    f"{ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES} "
    f"días después del fin de curso"
)
COULD_NOT_DELETE_COURSE_ENROLLMENTS = 'No se pudieron eliminar las matriculas del curso'
COULD_NOT_DELETE_PERIOD_ENROLLMENTS = 'No se pudieron eliminar las matriculas del periodo'
STUDENT_HAS_ENROLLMENTS_ALREADY_CREATED = "El estudiante ya presenta matrícula en el presente año académico"
COULD_NOT_ADD_STUDENTS = 'No se pudieron agregar los alumnos'
COULD_NOT_REGISTER_STUDENT = 'No se pudo registrar el alumno'
