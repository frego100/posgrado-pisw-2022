# Generated by Django 3.2.13 on 2022-11-12 07:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('academics', '0006_auto_20221112_0233'),
        ('enrollment', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentcourseattendance',
            name='date',
        ),
        migrations.RemoveField(
            model_name='studentcourseattendance',
            name='observations',
        ),
        migrations.AlterField(
            model_name='studentcourseattendance',
            name='attendance',
            field=models.PositiveSmallIntegerField(choices=[(1, 'A tiempo'), (2, 'Falta'), (3, 'Tarde'), (4, 'Falta Justificada')], default=None, null=True),
        ),
        migrations.CreateModel(
            name='AcademicSession',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('schedule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='academics.schedule')),
            ],
        ),
        migrations.AddField(
            model_name='studentcourseattendance',
            name='academic_session',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='enrollment.academicsession'),
        ),
    ]
