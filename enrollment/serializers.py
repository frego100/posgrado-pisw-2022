"""
Enrollments serializers
"""
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from account.models import Student
from academics.models import Course
from account.serializers import StudentSerializer
from core.utils import validate_existing_instance
from enrollment.constants import EnrollmentRemovalTypes
from enrollment.messages.error_messages import INVALID_GRADES_REGISTER_PERIOD
from enrollment.models import (
    StudentCourseAttendance,
    StudentGrade,
    EnrollmentRemoval,
    Enrollment,
    GroupEnrollment, EnrollmentUpdate,
)
from enrollment.utils import verify_grade_register_period


class StudentCourseAttendanceSerializer(serializers.ModelSerializer):
    """
    Student Course Attendance serializer
    """

    class Meta:
        model = StudentCourseAttendance
        fields = ['attendance', ]


class ManualEnrollmentSerializer(serializers.Serializer):

    student = serializers.CharField()
    course = serializers.CharField()
    resolution_file = serializers.FileField()

    def validate_student(self, student):
        return validate_existing_instance(Student, student, 'El estudiante')

    def validate_course(self, course):
        return validate_existing_instance(Course, course, 'El curso')

    def validate(self, attrs):
        attrs = super(ManualEnrollmentSerializer, self).validate(attrs)
        student: Student = attrs['student']
        course: Course = attrs['course']
        is_enrolled = course.coursegroup_set.filter(
            groupenrollment__enrollment__student=student
        ).exists()

        if is_enrolled:
            raise serializers.ValidationError(
                'El alumno ya fue matriculado en el curso'
            )

        return attrs

    def create(self, validated_data):
        student: Student = validated_data['student']
        course: Course = validated_data['course']
        resolution_file = validated_data['resolution_file']
        group = course.coursegroup_set.first()

        enrollment = Enrollment.objects.create(
            student=student,
            credits=0,
            courses=1
        )
        group_enrollment = GroupEnrollment.objects.create(
            enrollment=enrollment,
            group=group,
            enrollment_num=1,
            resolution_file=resolution_file
        )

        return group_enrollment


class EnrollmentRemovalSerializer(serializers.ModelSerializer):
    """
    Enrollment Removal serializer
    """

    class Meta:
        model = EnrollmentRemoval
        fields = ['enrollment', 'group_enrollments', 'removal_resolution', 'resolution_number']

    def validate(self, attrs):
        """
        Validate if group enrollments are related to given enrollment
        """
        enrollment = attrs['enrollment']
        group_enrollments = attrs['group_enrollments']

        available_group_enrollments = set(enrollment.groupenrollment_set.all())

        if not set(group_enrollments).issubset(available_group_enrollments):
            raise serializers.ValidationError(
                'El curso no pertenece a la matrícula enviada'
            )

        return attrs

    def create(self, validated_data: dict) -> EnrollmentRemoval:
        """
        Create method to set if enrollment removal is partial or total
        @param validated_data: validated data
        @return: enrollment removal
        """
        group_enrollments = validated_data.get('group_enrollments')
        enrollment = validated_data.get('enrollment')

        all_group_enrollments = set(enrollment.groupenrollment_set.all())
        if set(group_enrollments) == all_group_enrollments:
            validated_data['withdraw_type'] = EnrollmentRemovalTypes.TOTAL
        else:
            validated_data['withdraw_type'] = EnrollmentRemovalTypes.PARTIAL

        return super().create(validated_data)


class StudentGradeSerializer(serializers.ModelSerializer):
    """
    StudentGrade serializer
    """

    class Meta:
        model = StudentGrade
        fields = ['student_enrollment', 'score']

    def validate_score(self, score):
        """
        Validate score grade
        """
        if score > 20 or score < 0:
            raise serializers.ValidationError(f"{score} no es una nota válida")

        return score

    def validate(self, attrs):
        if self.partial:
            course_group = self.instance.student_enrollment.group

        else:
            course_group = attrs['student_enrollment'].group

        if not verify_grade_register_period(course_group):
            raise ValidationError(INVALID_GRADES_REGISTER_PERIOD)

        return attrs
