from django.contrib import admin

from enrollment.models import (
    Enrollment,
    GroupEnrollment,
    StudentCourseAttendance,
    AdmissionProcess,
    StudyProgramAdmission,
    EnrollmentPeriod,
    EnrollmentRemoval,
    StudentGrade,
    AcademicSession,
    EnrollmentUpdate,
)


class GroupEnrollmentInline(admin.TabularInline):
    model = GroupEnrollment
    extra = 0
    readonly_fields = ('id', )

@admin.register(Enrollment)
class EnrollmentAdmin(admin.ModelAdmin):
    """
    Enrollment admin
    """

    list_display = (
        '__str__',
        'status',
    )
    list_filter = (
        'enrollment_period__year', 'enrollment_period__period', 'status',
    )
    search_fields = (
        'student__user__first_name',
        'student__user__last_name',
        'student__cui',
    )
    inlines = (GroupEnrollmentInline, )


@admin.register(GroupEnrollment)
class GroupEnrollmentAdmin(admin.ModelAdmin):
    """
    Group Enrollment admin
    """
    list_display = ('enrollment', 'group')


@admin.register(EnrollmentPeriod)
class EnrollmentPeriodAdmin(admin.ModelAdmin):
    """
    Enrollment Period admin
    """

    list_display = ('__str__', 'program')
    list_filter = ('process__year', 'period',)
    search_fields = ('program', )


@admin.register(StudentCourseAttendance)
class StudentCourseAttendanceAdmin(admin.ModelAdmin):
   
    pass


class StudentCourseAttendanceInline(admin.TabularInline):

    model = StudentCourseAttendance
    extra = 0


@admin.register(AdmissionProcess)
class AdmissionProcessAdmin(admin.ModelAdmin):

    list_display = ('__str__', 'date', 'excel_file', )
    list_filter = ('date', )


@admin.register(StudyProgramAdmission)
class StudyProgramAcceptanceAdmin(admin.ModelAdmin):

    list_display = ('admission_process', 'study_program', )


@admin.register(EnrollmentRemoval)
class EnrollmentRemovalAdmin(admin.ModelAdmin):
    pass


@admin.register(StudentGrade)
class StudentGradeAdmin(admin.ModelAdmin):

    list_display = ('student_enrollment', 'get_course_group', 'score',)

    @admin.display(description='Course')
    def get_course_group(self, obj):
        return obj.student_enrollment.group.__str__()


@admin.register(AcademicSession)
class AcademicSessionAdmin(admin.ModelAdmin):
    inlines = [StudentCourseAttendanceInline, ]
    list_display = ['date', ]


@admin.register(EnrollmentUpdate)
class EnrollmentUpdateAdmin(admin.ModelAdmin):
    list_display = ('student', 'study_program', 'created',)
