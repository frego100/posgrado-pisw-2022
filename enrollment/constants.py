"""
Enrollment constants
"""

from django.db import models
from django.db.models import IntegerChoices


class EnrollmentStatus(IntegerChoices):
    """
    Enrollment status
    """

    ENROLLED = 1, 'Matriculado'
    WITHDRAWN = 2, 'Retiro'
    DROPPED = 3, 'Abandono'


class AttendanceType(models.IntegerChoices):
    """
    AttendaceType Students
    """
    ON_TIME = 1, 'A tiempo'
    ABSENT = 2, 'Falta'
    LATE = 3, 'Tarde'
    EXCUSED_ABSENCE = 4, 'Falta Justificada'


ATTENDANCE_TYPE_MAP = {
    AttendanceType.ON_TIME: 'on_time',
    AttendanceType.ABSENT: 'absent',
    AttendanceType.LATE: 'late',
    AttendanceType.EXCUSED_ABSENCE: 'excused'
}

STUDENTS_FILE_START_INDEX = 8
GROUP_ENROLLMENTS_FILE_START_INDEX = 10
ACCEPTANCE_RECORD_FILE_START_INDEX = 2


class EnrollmentRemovalTypes(models.IntegerChoices):
    """
    EnrollmentRemoval types
    """
    PARTIAL = 1, "Partial"
    TOTAL = 2, "Total"


class EnrollmentRemovalStatusTypes(models.IntegerChoices):
    """
    EnrollmentRemoval types
    """
    SENT = 1, "Enviado"
    REPORT_REQUIRED = 2, "Informe requerido"
    REPORT_SENT = 3, "Informe enviado"
    APPROVED = 4, "Aprobado"
    REJECTED = 5, "Rechazado"


ALLOWED_DAYS_TO_REGISTER_STUDENT_GRADES = 14