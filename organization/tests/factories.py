import factory
from organization.models import StudyField, GraduateUnit, StudyProgram


class StudyFieldFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StudyField

    name = 'TestStudyField'


class GraduateUnitFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GraduateUnit

    name = 'TestGraduateUnit'
    study_field = factory.SubFactory(StudyFieldFactory)


class StudyProgramFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StudyProgram

    name = 'TestStudyProgram'
    program_type = 1
    graduate_unit = factory.SubFactory(GraduateUnitFactory)
