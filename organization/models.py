"""
Organization models
"""
from django.db import models

from organization.constants import ProgramTypes


class StudyField(models.Model):
    """
    Study field model
    """

    name = models.CharField(max_length=350)
    code = models.CharField(max_length=16)

    def __str__(self):
        return self.name


class GraduateUnit(models.Model):
    """
    Graduate unit model
    """

    name = models.CharField(max_length=350)
    study_field = models.ForeignKey(StudyField, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class StudyProgram(models.Model):
    """
    Study program model
    """

    graduate_unit = models.ForeignKey(GraduateUnit, on_delete=models.CASCADE)
    name = models.CharField(max_length=350)
    program_type = models.PositiveSmallIntegerField(choices=ProgramTypes.choices)
    mention = models.CharField(max_length=350)
    code = models.CharField(max_length=16)

    def __str__(self):
        return self.name
