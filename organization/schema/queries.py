"""
Organization queries
"""
from graphene import (
    ObjectType,
    List,
    Field,
    ID, Int,
)
from graphene_django.filter import DjangoFilterConnectionField

from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required

from academics.models import StudyPlanRequest
from core.utils import get_object_by_id
from enrollment.models import Enrollment
from operating_plan.models import OperatingPlan, ProfessorRegistration
from organization.schema.types import (
    GraduateUnitType,
    StudyProgramType, SchoolDashboardType, UnitDashboardType,

)
from organization.models import GraduateUnit, StudyProgram


class Query(ObjectType):
    """
    Account Query Object
    """

    get_graduate_units = DjangoFilterConnectionField(
        GraduateUnitType, description=_('Get all graduate units')
    )
    get_graduate_unit = Field(
        GraduateUnitType,
        graduate_unit_id=ID(description=_('Id'), required=True),
        description=_('Get graduate unit by id')
    )
    get_study_programs = DjangoFilterConnectionField(
        StudyProgramType, description=_('Get all study programs')
    )
    get_study_program = Field(
        StudyProgramType,
        study_program_id=ID(description=_('Id'), required=True),
        description=_('Get study program by id')
    )
    get_school_dashboard = Field(
        SchoolDashboardType,
        process=ID(required=True),
        description=_('Get school dashboard')
    )
    get_unit_dashboard = Field(
        UnitDashboardType,
        graduate_unit_id=ID(description=_('Id'), required=True),
        description=_('Get unit dashboard')
    )

    @login_required
    def resolve_get_graduate_units(self, info, **kwargs):
        return GraduateUnit.objects.all()

    @login_required
    def resolve_get_graduate_unit(self, info, graduate_unit_id):
        return get_object_by_id(GraduateUnit, graduate_unit_id)

    @login_required
    def resolve_get_study_programs(self, info, **kwargs):
        return StudyProgram.objects.all().order_by('code')

    @login_required
    def resolve_get_study_program(self, info, study_program_id):
        return get_object_by_id(StudyProgram, study_program_id)

    @login_required
    def resolve_get_school_dashboard(self, info, process):
        num_of_op_plan_approved = OperatingPlan.objects.filter(
            op_plan_process=process,
            status=3
        ).count()
        total = OperatingPlan.objects.filter(
            op_plan_process=process,
        ).count()
        percentage = int((num_of_op_plan_approved / total) * 100)
        study_plans = StudyPlanRequest.objects.exclude(
            status=6
        ).order_by('-request_date')
        professors = ProfessorRegistration.objects.filter(
            operating_plan_program__operating_plan__op_plan_process=process
        ).distinct('professor_id').count()
        students = Enrollment.objects.filter(
            enrollment_period__process=process
        ).count()

        return {
            "graduate_units": GraduateUnit.objects.all(),
            "operating_plans_percentage": percentage,
            "study_plans": study_plans,
            "number_of_professors": professors,
            "number_of_students": students
        }

    @login_required
    def resolve_get_unit_dashboard(self, info, graduate_unit_id):
        return {
            "study_programs": StudyProgram.objects.filter(
                graduate_unit=graduate_unit_id
            )
        }
