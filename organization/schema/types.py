"""
Organization types
"""
from graphene import ID, Int, Field, ObjectType, List, DateTime
from graphene.relay import Node
from graphene_django.types import DjangoObjectType

from academics.models import CourseGroup, StudyPlan
from academics.schema.types import StudyPlanRequestType
from enrollment.constants import EnrollmentStatus
from enrollment.models import Enrollment
from operating_plan.models import ProfessorRegistration
from organization.models import (
    StudyProgram,
    GraduateUnit,
    StudyField,
)


class StudyProgramType(DjangoObjectType):
    """
    Study program type
    """

    id = ID(source='pk', required=True)
    num_of_course_groups = Field(Int, process=ID(required=True))
    num_of_professors = Field(Int, process=ID(required=True))
    num_of_students = Field(Int, process=ID(required=True))
    num_of_semesters = Field(Int)
    last_payment_update = DateTime()

    class Meta:
        model = StudyProgram
        filter_fields = {
            'graduate_unit__id': ['exact'],
            'name': ['icontains'],
            'program_type': ['exact'],
        }
        interfaces = (Node,)

    def resolve_num_of_course_groups(self: StudyProgram, info, process):
        return CourseGroup.objects.filter(
            course__operating_plan_program__study_program=self,
            course__operating_plan_program__operating_plan__op_plan_process=process,
            is_deleted=False
        ).count()

    def resolve_num_of_professors(self: StudyProgram, info, process):
        return ProfessorRegistration.objects.filter(
            operating_plan_program__study_program=self,
            operating_plan_program__operating_plan__op_plan_process=process,
            is_deleted=False
        ).distinct().count()

    def resolve_num_of_students(self: StudyProgram, info, process):
        return Enrollment.objects.filter(
            enrollment_period__program=self,
            enrollment_period__process=process,
            status=1
        ).distinct().count()

    def resolve_num_of_semesters(self: StudyProgram, info):
        if self.studyplan_set.exists():
            return self.studyplan_set.first().num_of_semesters

        return 0

    def resolve_last_payment_update(self: StudyProgram, info):
        payment_schedules = self.paymentschedule_set.filter(
            last_update__isnull=False
        ).order_by('-last_update')

        if payment_schedules.exists():
            return payment_schedules.first().last_update

class GraduateUnitType(DjangoObjectType):
    """
    Graduate unit type
    """

    id = ID(source='pk', required=True)
    num_of_course_groups = Field(Int, process=ID(required=True))
    num_of_professors = Field(Int, process=ID(required=True))
    num_of_students = Field(Int, process=ID(required=True))

    class Meta:
        model = GraduateUnit
        filter_fields = {
            'study_field__id': ['exact'],
            'name': ['icontains'],
        }
        interfaces = (Node,)

    def resolve_num_of_course_groups(self: GraduateUnit, info, process):
        return CourseGroup.objects.filter(
            course__operating_plan_program__study_program__graduate_unit=self,
            course__operating_plan_program__operating_plan__op_plan_process=process,
            is_deleted=False
        ).distinct().count()

    def resolve_num_of_professors(self: GraduateUnit, info, process):
        return ProfessorRegistration.objects.filter(
            operating_plan_program__study_program__graduate_unit=self,
            operating_plan_program__operating_plan__op_plan_process=process,
            is_deleted=False
        ).distinct('professor_id').count()

    def resolve_num_of_students(self: GraduateUnit, info, process):
        return Enrollment.objects.filter(
            enrollment_period__program__graduate_unit=self,
            enrollment_period__process=process,
            status=EnrollmentStatus.ENROLLED
        ).distinct().count()


class StudyFieldType(DjangoObjectType):
    """
    Study field type
    """

    class Meta:
        model = StudyField


class SchoolDashboardType(ObjectType):
    graduate_units = List(GraduateUnitType, default_value=None)
    operating_plans_percentage = Int()
    study_plans = List(StudyPlanRequestType, default_value=None)
    number_of_professors = Int()
    number_of_students = Int()


class UnitDashboardType(ObjectType):
    study_programs = List(StudyProgramType, default_value=None)
