"""
Organization constants
"""
from django.db import models


class ProgramTypes(models.IntegerChoices):
    """
    Program types choices
    """

    MASTER = 1, 'Maestria'
    DOCTORATE = 2, 'Doctorado'
    DIPLOMAT = 3, 'Diplomado'


PROGRAM_TYPE_PERIODS = {
    ProgramTypes.MASTER: 4,
    ProgramTypes.DOCTORATE: 6,
    ProgramTypes.DIPLOMAT: 4
}
