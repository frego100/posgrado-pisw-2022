"""
Organization admin
"""
from django.contrib import admin

from organization.models import (
    GraduateUnit,
    StudyField,
    StudyProgram,
)


@admin.register(GraduateUnit)
class GraduateUnitAdmin(admin.ModelAdmin):
    """
    Graduate admin
    """

    list_display = ('__str__', 'study_field')
    list_filter = ('study_field', )
    search_fields = ('name', )


@admin.register(StudyProgram)
class StudyProgramAdmin(admin.ModelAdmin):
    """
    Study program admin
    """

    list_display = ('__str__', 'graduate_unit', 'program_type')
    list_filter = ('program_type', 'graduate_unit', )
    search_fields = ('name', )


@admin.register(StudyField)
class StudyFieldAdmin(admin.ModelAdmin):
    """
    Study field admin
    """

    pass
